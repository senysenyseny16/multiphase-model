//: MUSCL.cpp

#include "MUSCL.hpp"

#include <cmath>

namespace MUSCL {

double minmod(const double a, const double b)
{
  const double modA = std::abs(a);
  const double modB = std::abs(b);
  const double mulAb = a * b;

  if (modA < modB and mulAb > 0.) {
    return a;
  } else if (modB < modA and mulAb > 0.) {
    return b;
  } else {
    return 0.;
  }
}

double VanLeer(const double dw)
{
  return std::max(0., (dw + std::abs(dw)) / (1. + std::abs(dw) + 1e-100));
}

void MUSCL2( const double  valB
           , const double  valC
           , const double  valF
           , double& recB
           , double& recF)
{
  const double sigma = minmod(valC - valB, valF - valC);

  recB = valC - sigma / 2.;
  recF = valC + sigma / 2.;

  /* const double dwB = (valC - valB) / (valF - valC + 1e-100); */
  /* const double dwF = (valF - valC) / (valC - valB + 1e-100); */

  /* recB = valC - 0.5 * VanLeer(dwB) * (valF - valC); */
  /* recF = valC + 0.5 * VanLeer(dwF) * (valC - valB); */
}

}
