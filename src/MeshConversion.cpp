//: MeshConversion.cpp

#include "MeshConversion.hpp"
#include "Mesh.hpp"
#include "MeshEdge.hpp"
#include "VariablesConversion.hpp"

namespace MeshConversion {

void toConservative( const Mesh& meshPrimitive
                   ,       Mesh& meshConservative)
{
  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const double alpha1 = meshPrimitive(0,  i, j);
      const double rho2   = meshPrimitive(1,  i, j);
      const double rho1   = meshPrimitive(2,  i, j);
      const double u11    = meshPrimitive(3,  i, j);
      const double u12    = meshPrimitive(4,  i, j);
      const double u21    = meshPrimitive(5,  i, j);
      const double u22    = meshPrimitive(6,  i, j);
      const double F11    = meshPrimitive(7,  i, j);
      const double F12    = meshPrimitive(8,  i, j);
      const double F21    = meshPrimitive(9,  i, j);
      const double F22    = meshPrimitive(10, i, j);

      meshConservative(0,  i, j) = VariablesConversion::U0(alpha1);
      meshConservative(1,  i, j) = VariablesConversion::U1(alpha1, rho2);
      meshConservative(2,  i, j) = VariablesConversion::U2(alpha1, rho1);

      meshConservative(3,  i, j) = VariablesConversion::U3(u11, u12);
      meshConservative(4,  i, j) = VariablesConversion::U4(u21, u22);

      meshConservative(5,  i, j) = VariablesConversion::U5(alpha1, rho1, rho2, u11, u12);
      meshConservative(6,  i, j) = VariablesConversion::U6(alpha1, rho1, rho2, u21, u22);

      meshConservative(7,  i, j) = VariablesConversion::U7 (alpha1, rho1, rho2, F11);
      meshConservative(8,  i, j) = VariablesConversion::U8 (alpha1, rho1, rho2, F12);
      meshConservative(9,  i, j) = VariablesConversion::U9 (alpha1, rho1, rho2, F21);
      meshConservative(10, i, j) = VariablesConversion::U10(alpha1, rho1, rho2, F22);

    }
  }
}

void toPrimitive( const Mesh& meshConservative
                ,       Mesh& meshPrimitive)
{
  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const double U0  = meshConservative(0,  i, j);
      const double U1  = meshConservative(1,  i, j);
      const double U2  = meshConservative(2,  i, j);
      const double U3  = meshConservative(3,  i, j);
      const double U4  = meshConservative(4,  i, j);
      const double U5  = meshConservative(5,  i, j);
      const double U6  = meshConservative(6,  i, j);
      const double U7  = meshConservative(7,  i, j);
      const double U8  = meshConservative(8,  i, j);
      const double U9  = meshConservative(9,  i, j);
      const double U10 = meshConservative(10, i, j);

      meshPrimitive(0,  i, j) = VariablesConversion::alpha1(U0);
      meshPrimitive(1,  i, j) = VariablesConversion::rho2(U0, U1);
      meshPrimitive(2,  i, j) = VariablesConversion::rho1(U0, U2);

      meshPrimitive(3,  i, j) = VariablesConversion::u11(U1, U2, U3, U5);
      meshPrimitive(4,  i, j) = VariablesConversion::u12(U1, U2, U3, U5);
      meshPrimitive(5,  i, j) = VariablesConversion::u21(U1, U2, U4, U6);
      meshPrimitive(6,  i, j) = VariablesConversion::u22(U1, U2, U4, U6);

      meshPrimitive(7,  i, j) = VariablesConversion::F11(U1, U2, U7);
      meshPrimitive(8,  i, j) = VariablesConversion::F12(U1, U2, U8);
      meshPrimitive(9,  i, j) = VariablesConversion::F21(U1, U2, U9);
      meshPrimitive(10, i, j) = VariablesConversion::F22(U1, U2, U10);

    }
  }
}

void toConservative( const Mesh& p
                   , double (&c)[Physics::varCount]
                   , const size_t i
                   , const size_t j)

{
  // p - Mesh with primitive vars.
  // c - vector with conservative vars.

  c[0] = VariablesConversion::U0(p(0, i, j));
  c[1] = VariablesConversion::U1(p(0, i, j), p(1, i, j));
  c[2] = VariablesConversion::U2(p(0, i, j), p(2, i, j));

  c[3] = VariablesConversion::U3(p(3, i, j), p(4, i, j));
  c[4] = VariablesConversion::U4(p(5, i, j), p(6, i, j));

  c[5] = VariablesConversion::U5( p(0, i, j)
                                , p(2, i, j) // rho1
                                , p(1, i, j) // rho2
                                , p(3, i, j)
                                , p(4, i, j));

  c[6] = VariablesConversion::U6( p(0, i, j)
                                , p(2, i, j) // rho1
                                , p(1, i, j) // rho2
                                , p(5, i, j)
                                , p(6, i, j));

  //                                               rho1        rho2
  c[7]  = VariablesConversion::U7 (p(0, i, j), p(2, i, j), p(1, i, j), p(7,  i, j));
  c[8]  = VariablesConversion::U8 (p(0, i, j), p(2, i, j), p(1, i, j), p(8,  i, j));
  c[9]  = VariablesConversion::U9 (p(0, i, j), p(2, i, j), p(1, i, j), p(9,  i, j));
  c[10] = VariablesConversion::U10(p(0, i, j), p(2, i, j), p(1, i, j), p(10, i, j));
}

void toPrimitive( const double (&c)[Physics::varCount]
                , Mesh& p
                , const size_t i
                , const size_t j)
{
  p(0,  i, j) = VariablesConversion::alpha1(c[0]);

  // Filter.
  /* static const double eps = 1e-8; */
  /* const double alpha1 = p(0, i, j); */
  /* const double alpha2 = 1. - alpha1; */
  /* p(1,  i, j) = (c[1] * alpha2 + eps * Physics::rho20) / (alpha2 * alpha2 + eps); */
  /* p(2,  i, j) = (c[2] * alpha1 + eps * Physics::rho10) / (alpha1 * alpha1 + eps); */
  p(1,  i, j) = VariablesConversion::rho2  (c[0], c[1]);
  p(2,  i, j) = VariablesConversion::rho1  (c[0], c[2]);

  p(3,  i, j) = VariablesConversion::u11   (c[1], c[2], c[3], c[5]);
  p(4,  i, j) = VariablesConversion::u12   (c[1], c[2], c[3], c[5]);
  p(5,  i, j) = VariablesConversion::u21   (c[1], c[2], c[4], c[6]);
  p(6,  i, j) = VariablesConversion::u22   (c[1], c[2], c[4], c[6]);
  p(7,  i, j) = VariablesConversion::F11   (c[1], c[2], c[7]);
  p(8,  i, j) = VariablesConversion::F12   (c[1], c[2], c[8]);
  p(9,  i, j) = VariablesConversion::F21   (c[1], c[2], c[9]);
  p(10, i, j) = VariablesConversion::F22   (c[1], c[2], c[10]);
}

void toConservative( const MeshEdge& meshPrimitive
                   ,       MeshEdge& meshConservative)
{
  toConservative(meshPrimitive.dataXB(), meshConservative.dataXB());
  toConservative(meshPrimitive.dataXF(), meshConservative.dataXF());
  toConservative(meshPrimitive.dataYB(), meshConservative.dataYB());
  toConservative(meshPrimitive.dataYF(), meshConservative.dataYF());
}

} // namespace MeshConversion

