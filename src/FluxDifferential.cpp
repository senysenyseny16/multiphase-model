//: FluxDifferential.cpp

#include "FluxDifferential.hpp"
#include "Mesh.hpp"

#include "VariablesAdditional.hpp"
#include "Energy.hpp"
#include "ShearStress.hpp"

namespace Flux {

void calcFluxDXLocal( const Mesh& meshPhysicalRec
                    , const size_t i
                    , const size_t j
                    , Mesh& meshFlux)
{
  // Unpack variables.
  const double alpha1 = meshPhysicalRec(0,  i, j);
  const double rho2   = meshPhysicalRec(1,  i, j);
  const double rho1   = meshPhysicalRec(2,  i, j);
  const double u11    = meshPhysicalRec(3,  i, j);
  const double u12    = meshPhysicalRec(4,  i, j);
  const double u21    = meshPhysicalRec(5,  i, j);
  const double u22    = meshPhysicalRec(6,  i, j);
  const double F11    = meshPhysicalRec(7,  i, j);
  const double F12    = meshPhysicalRec(8,  i, j);
  const double F21    = meshPhysicalRec(9,  i, j);
  const double F22    = meshPhysicalRec(10, i, j);

  // Calc additional variables.
  const double alpha2 = VariablesAdditional::alpha2(alpha1);

  const double rho    = VariablesAdditional::rho(alpha1, rho1, rho2);

  const double c1 = VariablesAdditional::c1(alpha1, rho1, rho2);

  const double c2 = VariablesAdditional::c2(alpha1, rho1, rho2);

  const double u1 = VariablesAdditional::u1(c1, c2, u11, u12);

  /* const double u2 = VariablesAdditional::u2(c1, c2, u21, u22); */

  // Calc energy and pressure.
  const double e1 = Energy::e1(rho1, F11, F12, F21, F22);

  const double e2 = Energy::e2(rho2);

  const double p1 = Energy::p1(rho1);

  const double p2 = Energy::p2(rho2);

  // Calc shear stresses.
  const double sigma11 = ShearStress::sigma11(rho1, F11, F12, F21, F22);

  const double sigma21 = ShearStress::sigma21(rho1, F11, F12, F21, F22);

  meshFlux(0,  i, j) = Flux::xF0(alpha1, u1);
  meshFlux(1,  i, j) = Flux::xF1(alpha1, rho2, u12);
  meshFlux(2,  i, j) = Flux::xF2(alpha1, rho1, u11);
  meshFlux(3,  i, j) = Flux::xF3(rho1, rho2, u11, u12, u21, u22, e1, e2, p1, p2);
  meshFlux(4,  i, j) = 0.;
  meshFlux(5,  i, j) = Flux::xF5(alpha1, alpha2, rho1, rho2, u11, u12, p1,  p2,  sigma11);
  meshFlux(6,  i, j) = Flux::xF6(alpha1, alpha2, rho1, rho2, u11, u12, u21, u22, sigma21);

  meshFlux(7,  i, j) = Flux::xF7 (rho, F11, u1);
  meshFlux(8,  i, j) = Flux::xF8 (rho, F12, u1);
  meshFlux(9,  i, j) = Flux::xF9 (rho, F21, u1);
  meshFlux(10, i, j) = Flux::xF10(rho, F22, u1);
}

void calcFluxDYLocal( const Mesh& meshPhysicalRec
                    , const size_t i
                    , const size_t j
                    , Mesh& meshFlux)
{
  // Unpack variables.
  const double alpha1 = meshPhysicalRec(0,  i, j);
  const double rho2   = meshPhysicalRec(1,  i, j);
  const double rho1   = meshPhysicalRec(2,  i, j);
  const double u11    = meshPhysicalRec(3,  i, j);
  const double u12    = meshPhysicalRec(4,  i, j);
  const double u21    = meshPhysicalRec(5,  i, j);
  const double u22    = meshPhysicalRec(6,  i, j);
  const double F11    = meshPhysicalRec(7,  i, j);
  const double F12    = meshPhysicalRec(8,  i, j);
  const double F21    = meshPhysicalRec(9,  i, j);
  const double F22    = meshPhysicalRec(10, i, j);

  // Calc additional variables.
  const double alpha2 = VariablesAdditional::alpha2(alpha1);

  const double rho    = VariablesAdditional::rho(alpha1, rho1, rho2);

  const double c1 = VariablesAdditional::c1(alpha1, rho1, rho2);

  const double c2 = VariablesAdditional::c2(alpha1, rho1, rho2);

  /* const double u1 = VariablesAdditional::u1(c1, c2, u11, u12); */

  const double u2 = VariablesAdditional::u2(c1, c2, u21, u22);

  // Calc energy and pressure.
  const double e1 = Energy::e1(rho1, F11, F12, F21, F22);

  const double e2 = Energy::e2(rho2);

  const double p1 = Energy::p1(rho1);

  const double p2 = Energy::p2(rho2);

  // Calc shear stresses.

  const double sigma12 = ShearStress::sigma12(rho1, F11, F12, F21, F22);

  const double sigma22 = ShearStress::sigma22(rho1, F11, F12, F21, F22);

  meshFlux(0,  i, j) = Flux::yF0(alpha1, u2);
  meshFlux(1,  i, j) = Flux::yF1(alpha1, rho2, u22);
  meshFlux(2,  i, j) = Flux::yF2(alpha1, rho1, u21);
  meshFlux(3,  i, j) = 0.;
  meshFlux(4,  i, j) = Flux::xF3(rho1, rho2, u11, u12, u21, u22, e1, e2, p1, p2);
  meshFlux(5,  i, j) = Flux::yF5(alpha1, alpha2, rho1, rho2, u11, u12, u21, u22, sigma12);
  meshFlux(6,  i, j) = Flux::yF6(alpha1, alpha2, rho1, rho2, u21, u22, p1,  p2,  sigma22);

  meshFlux(7,  i, j) = Flux::yF7 (rho, F11, u2);
  meshFlux(8,  i, j) = Flux::yF8 (rho, F12, u2);
  meshFlux(9,  i, j) = Flux::yF9 (rho, F21, u2);
  meshFlux(10, i, j) = Flux::yF10(rho, F22, u2);
}

} // namespace Flux

