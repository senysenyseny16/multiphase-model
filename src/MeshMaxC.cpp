//: MeshMaxC.cpp

#include "MeshMaxC.hpp"
#include "MeshEdge.hpp"
#include "VariablesAdditional.hpp"
#include "SoundSpeed.hpp"


#include <iostream>

MeshMaxC::MeshMaxC()
  : MeshEdgeMutual1D()
  , maxCGlobalX(0.)
  , maxCGlobalY(0.)
{ }

void MeshMaxC::calcMaxC(const MeshEdge& rP)
{

  maxCGlobalX = 0.;
  maxCGlobalY = 0.;
  minCGlobal  = std::numeric_limits<double>::max();

  // !TODO: check boundaries.
  for (size_t j = Geometry::firstCell + 1; j <= Geometry::lastCellY - 1; ++j) {
    for (size_t i = Geometry::firstCell + 1; i <= Geometry::lastCellX - 1; ++i) {

      const double rho1B  = rP.dataXF()(2, i - 1, j);
      const double rho2B  = rP.dataXF()(1, i - 1, j);

      const double c1B = VariablesAdditional::c1( rP.dataXF()(0, i - 1, j)
                                                , rP.dataXF()(2, i - 1, j)
                                                , rP.dataXF()(1, i - 1, j));
      const double c2B = 1. - c1B;

      const double u1B = VariablesAdditional::u1( c1B, c2B
                                                , rP.dataXF()(3, i - 1, j)
                                                , rP.dataXF()(4, i - 1, j));

      const double maxCB = SoundSpeed::maxC(rho1B, rho2B, c1B, c2B, u1B);

      const double rho1F  = rP.dataXB()(2, i, j);
      const double rho2F  = rP.dataXB()(1, i, j);

      const double c1F = VariablesAdditional::c1( rP.dataXB()(0, i, j)
                                                , rP.dataXB()(2, i, j)
                                                , rP.dataXB()(1, i, j));
      const double c2F = 1. - c1F;

      const double u1F = VariablesAdditional::u1( c1F, c2F
                                                , rP.dataXB()(3, i, j)
                                                , rP.dataXB()(4, i, j));

      const double maxCF = SoundSpeed::maxC(rho1F, rho2F, c1F, c2F, u1F);

      edgeX(i, j) = std::max(maxCB, maxCF);

      // Find global maximum in x-direction.
      if (edgeX(i, j) > maxCGlobalX) maxCGlobalX = edgeX(i, j);
      if (edgeX(i, j) < minCGlobal)  minCGlobal  = edgeX(i, j);
    }
  }


  for (size_t j = Geometry::firstCell + 1; j <= Geometry::lastCellY - 1; ++j) {
    for (size_t i = Geometry::firstCell + 1; i <= Geometry::lastCellX - 1; ++i) {

      const double rho1B  = rP.dataYF()(2, i, j - 1);
      const double rho2B  = rP.dataYF()(1, i, j - 1);

      const double c1B = VariablesAdditional::c1( rP.dataYF()(0, i, j - 1)
                                                , rP.dataYF()(2, i, j - 1)
                                                , rP.dataYF()(1, i, j - 1));
      const double c2B = 1. - c1B;

      const double u2B = VariablesAdditional::u2( c1B, c2B
                                                , rP.dataYF()(5, i, j - 1)
                                                , rP.dataYF()(6, i, j - 1));

      const double maxCB = SoundSpeed::maxC(rho1B, rho2B, c1B, c2B, u2B);

      const double rho1F  = rP.dataYB()(2, i, j);
      const double rho2F  = rP.dataYB()(1, i, j);

      const double c1F = VariablesAdditional::c1( rP.dataYB()(0, i, j)
                                                , rP.dataYB()(2, i, j)
                                                , rP.dataYB()(1, i, j));
      const double c2F = 1. - c1F;

      const double u2F = VariablesAdditional::u2( c1F, c2F
                                                , rP.dataYB()(5, i, j)
                                                , rP.dataYB()(6, i, j));


      const double maxCF = SoundSpeed::maxC(rho1F, rho2F, c1F, c2F, u2F);

      edgeY(i, j) = std::max(maxCB, maxCF);

      // Find global maximum in y-direction.
      if (edgeY(i, j) > maxCGlobalY) maxCGlobalY = edgeY(i, j);
      if (edgeY(i, j) < minCGlobal)  minCGlobal  = edgeY(i, j);
    }
  }

  /* // Check boundaries. */
  /* for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) { */

  /*   const double rho1 = rP.xB(2, Geometry::firstCell, j); */
  /*   const double rho2 = rP.xB(1, Geometry::firstCell, j); */

  /*   const double c1 = VariablesAdditional::c1( rP.xB(0, Geometry::firstCell, j) */
  /*                                            , rP.xB(2, Geometry::firstCell, j) */
  /*                                            , rP.xB(1, Geometry::firstCell, j)); */
  /*   const double c2 = 1. - c1; */

  /*   const double u1 = VariablesAdditional::u1( c1, c2 */
  /*                                            , rP.xB(3, Geometry::firstCell, j) */
  /*                                            , rP.xB(4, Geometry::firstCell, j)); */

  /*   const double maxC = SoundSpeed::maxC(rho1, rho2, c1, c2, u1); */

  /*   edgeX(Geometry::firstCell, j) = maxC; */

  /*   // Find global maximum in x-direction. */
  /*   if (maxC > maxCGlobalX) maxCGlobalX = maxC; */
  /* } */

  /* for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) { */

  /*   const double rho1 = rP.xF(2, Geometry::lastCellX, j); */
  /*   const double rho2 = rP.xF(1, Geometry::lastCellX, j); */

  /*   const double c1 = VariablesAdditional::c1( rP.xF(0, Geometry::lastCellX, j) */
  /*                                            , rP.xF(2, Geometry::lastCellX, j) */
  /*                                            , rP.xF(1, Geometry::lastCellX, j)); */
  /*   const double c2 = 1. - c1; */

  /*   const double u1 = VariablesAdditional::u1( c1, c2 */
  /*                                            , rP.xF(3, Geometry::lastCellX, j) */
  /*                                            , rP.xF(4, Geometry::lastCellX, j)); */

  /*   const double maxC = SoundSpeed::maxC(rho1, rho2, c1, c2, u1); */

  /*   edgeX(Geometry::lastCellX + 1, j) = maxC; */

  /*   // Find global maximum in x-direction. */
  /*   if (maxC > maxCGlobalX) maxCGlobalX = maxC; */
  /* } */


  /* for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) { */

  /*   const double rho1 = rP.yB(2, i, Geometry::firstCell); */
  /*   const double rho2 = rP.yB(1, i, Geometry::firstCell); */

  /*   const double c1 = VariablesAdditional::c1( rP.yB(0, i, Geometry::firstCell) */
  /*                                            , rP.yB(2, i, Geometry::firstCell) */
  /*                                            , rP.yB(1, i, Geometry::firstCell)); */
  /*   const double c2 = 1. - c1; */

  /*   const double u2 = VariablesAdditional::u2( c1, c2 */
  /*                                            , rP.yB(5, i, Geometry::firstCell) */
  /*                                            , rP.yB(6, i, Geometry::firstCell)); */

  /*   const double maxC = SoundSpeed::maxC(rho1, rho2, c1, c2, u2); */

  /*   edgeY(i, Geometry::firstCell) = maxC; */

  /*   // Find global maximum in x-direction. */
  /*   if (maxC > maxCGlobalY) maxCGlobalY = maxC; */
  /* } */

  /* for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) { */

  /*   const double rho1 = rP.yF(2, i, Geometry::lastCellY); */
  /*   const double rho2 = rP.yF(1, i, Geometry::lastCellY); */

  /*   const double c1 = VariablesAdditional::c1( rP.yF(0, i, Geometry::lastCellY) */
  /*                                            , rP.yF(2, i, Geometry::lastCellY) */
  /*                                            , rP.yF(1, i, Geometry::lastCellY)); */
  /*   const double c2 = 1. - c1; */

  /*   const double u2 = VariablesAdditional::u2( c1, c2 */
  /*                                            , rP.yF(5, i, Geometry::lastCellY) */
  /*                                            , rP.yF(6, i, Geometry::lastCellY)); */

  /*   const double maxC = SoundSpeed::maxC(rho1, rho2, c1, c2, u2); */

  /*   edgeY(i, Geometry::lastCellY + 1) = maxC; */

  /*   // Find global maximum in x-direction. */
  /*   if (maxC > maxCGlobalY) maxCGlobalY = maxC; */
  /* } */
}

double MeshMaxC::maxCX() const
{
  return maxCGlobalX;
}

double MeshMaxC::maxCY() const
{
  return maxCGlobalY;
}

double MeshMaxC::minC() const
{
  return minCGlobal;
}

