//: SchemeO2.cpp

#include "SchemeO2.hpp"

#include "MeshRec.hpp"
#include "MeshFluxDifferential.hpp"
#include "MeshFluxNumerical.hpp"
#include "MeshRightSide.hpp"
#include "MeshMaxC.hpp"

#include "MeshConversion.hpp"
#include "PressureRelaxation.hpp"
#include "THINC.hpp"
#include "BoundaryConditions.hpp"
#include "VariablesAdditional.hpp"
#include "VariablesConversion.hpp"

#include "Utility.hpp"
#include "Visualization.hpp"

#include <memory>
#include <iostream>

SchemeO2::SchemeO2(const Mesh& initialData)
  : Scheme(initialData)
{}

void SchemeO2::run()
{
  using namespace SchemePars;

  Mesh                   p1;
  MeshRec                rec;  // Reconstruction.
  MeshEdge               recC; // Conservative vars on edges.
  MeshFluxDifferential   fD;   // Differential flux.

  // Numerical flux.
  std::unique_ptr<FluxNumerical::MeshFluxNumerical> pfN(FluxNumerical::createMeshFluxNumerical());

  MeshRightSide          rs;   // Right side.
  MeshMaxC               maxC;

  // Temporary vectors for conservative cells.
  double cVector0  [Physics::varCount];
  double cVector1  [Physics::varCount];
  double cVectorNew[Physics::varCount];

  // Plot initial data.
  //Visualization::plotMeshPhysical(p0);
  //Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration));
  //Utility::saveMeshToFile(p0, std::to_string(iteration) + ".dat");
  //Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2);
  //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2);

  // Time-loop.
  for (timeCur = 0.; timeCur < Physics::timeEnd; timeCur += tau)
  {
    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) {
      Utility::glueMeshBoundariesX(p0);
    } else {
      Utility::fillMeshBoundariesX(p0);
    }
    if (BC::isBoundaryCondYPeriodic) {
      Utility::glueMeshBoundariesY(p0);
    } else {
      Utility::fillMeshBoundariesY(p0);
    }

    // Calculate reconstruction.
    rec.reconstruct(p0, MeshRec::RecType::Muscl2);
    if (THINC::enableTHINC) THINC::bvd(p0, rec, THINC::RecType::Muscl2);

    // Apply boundary conditions.
    BC::applyBoundaryConditions1(p0, rec);

    /* Utility::boundAlpha1(rec); */

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(rec);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(rec);

    // Calculate differential flux.
    fD.calcFlux(rec);

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(fD);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(fD);

    // Calculate conservative variables.
    MeshConversion::toConservative(rec, recC);

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(recC);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(recC);

    // Calc local and global maximum of characteristics.
    maxC.calcMaxC(rec);

    // Recalc timestep.
    calcTau(maxC.maxCX(), maxC.maxCY());

    // Calculate numerical flux.
    pfN->calcFlux(fD, recC, rec, p0);

    // Apply boundary conditions.
    BC::applyBoundaryConditions2(p0, rec, *pfN);

    // Calculate right side.
    rs.calcRightSide(p0);

    // Change tau for last iteration.
    if (Physics::timeEnd - timeCur < tau) {
      tau = Physics::timeEnd - timeCur;
    }

    const double kx = tau / hx;
    const double ky = tau / hy;

    // First step of RK-2 method.
    #pragma omp parallel for private(cVector0, cVectorNew)
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

        // Convert primitive cell to conservative cell cVector0.
        MeshConversion::toConservative(p0, cVector0, i, j);

        // Evolve alpha1.
        const double alpha1FluxX  = ((pfN->c.xF(0, i, j) - pfN->c.xB(0, i, j)) + (pfN->nC.xF(0, i, j) - pfN->nC.xB(0, i, j))) * kx;
        const double alpha1FluxY  = ((pfN->c.yF(0, i, j) - pfN->c.yB(0, i, j)) + (pfN->nC.yF(0, i, j) - pfN->nC.yB(0, i, j))) * ky;

        cVectorNew[0] = cVector0[0] - (alpha1FluxX + alpha1FluxY);
        /* Utility::boundAlpha1(cVectorNew[0]); */

        // Evaluate U0, U1, U2.
        for (size_t v = 1; v < 3; ++v) {

          const double fluxX = (pfN->c.xF(v, i, j) - pfN->c.xB(v, i, j)) * kx;
          const double fluxY = (pfN->c.yF(v, i, j) - pfN->c.yB(v, i, j)) * ky;

          cVectorNew[v] = cVector0[v] - (fluxX + fluxY);
        }

        // Implement implicit scheme for relative velocity equations (U3, U4).
        /* const double c1 = VariablesAdditional::c1( p0(0, i, j) */
        /*                                          , p0(2, i, j) */
        /*                                          , p0(1, i, j)); */
        /* const double c2 = 1. - c1; */

        /* for (size_t v = 3; v <= 4; ++v) { */

        /*   const double flux =   (pfN->c.xF(v, i, j) - pfN->c.xB(v, i, j)) * kx */
        /*                       + (pfN->c.yF(v, i, j) - pfN->c.yB(v, i, j)) * ky; */

        /*   cVectorNew[v] = (   cVector0[v] */
        /*                     - flux */
        /*                     + rs(v, i, j) * tau */
        /*                   ) / (1. + tau * c1 * c2 * Physics::chi); */
        /* } */
        cVectorNew[3] = 0.;
        cVectorNew[4] = 0.;

        // Evaluate U5 - U10.
        for (size_t v = 5; v < 7; ++v) {

          const double fluxX = (pfN->c.xF(v, i, j) - pfN->c.xB(v, i, j)) * kx;
          const double fluxY = (pfN->c.yF(v, i, j) - pfN->c.yB(v, i, j)) * ky;

          cVectorNew[v] = cVector0[v] - (fluxX + fluxY);
        }

        for (size_t v = 7; v < Physics::varCount; ++v) {

          const double fluxX = ((pfN->c.xF(v, i, j) - pfN->c.xB(v, i, j)) + (pfN->nC.xF(v, i, j) - pfN->nC.xB(v, i, j))) * kx;
          const double fluxY = ((pfN->c.yF(v, i, j) - pfN->c.yB(v, i, j)) + (pfN->nC.yF(v, i, j) - pfN->nC.yB(v, i, j))) * ky;

          cVectorNew[v] = cVector0[v] - (fluxX + fluxY);
        }

        // Save new cell as primitive to p1.
        MeshConversion::toPrimitive(cVectorNew, p1, i, j);
      }
    }

    if (PressureRelaxation::enablePressureRelaxation) PressureRelaxation::relaxPressureMesh(p1);

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) {
      Utility::glueMeshBoundariesX(p1);
    } else {
      Utility::fillMeshBoundariesX(p1);
    }
    if (BC::isBoundaryCondYPeriodic) {
      Utility::glueMeshBoundariesY(p1);
    } else {
      Utility::fillMeshBoundariesY(p1);
    }

    // Calculate reconstruction.
    rec.reconstruct(p1, MeshRec::RecType::Muscl2);
    if (THINC::enableTHINC) THINC::bvd(p1, rec, THINC::RecType::Muscl2);

    // Apply boundary conditions.
    BC::applyBoundaryConditions1(p1, rec);

    /* Utility::boundAlpha1(rec); */

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(rec);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(rec);

    // Calculate differential flux.
    fD.calcFlux(rec);

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(fD);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(fD);

    // Calculate conservative variables.
    MeshConversion::toConservative(rec, recC);

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(recC);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(recC);

    // Calculate numerical flux.
    pfN->calcFlux(fD, recC, rec, p1);

    // Apply boundary conditions.
    BC::applyBoundaryConditions2(p1, rec, *pfN);

    // Calculate right side.
    rs.calcRightSide(p1);

    // Second step of RK-2 method.
    #pragma omp parallel for private(cVector0, cVector1, cVectorNew)
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

        // Convert primitive cells p0, p1 to conservative cells cVector0, cVector1.
        MeshConversion::toConservative(p0, cVector0, i, j);
        MeshConversion::toConservative(p1, cVector1, i, j);

        // Evolve alpha1.
        const double alpha1FluxX  = ((pfN->c.xF(0, i, j) - pfN->c.xB(0, i, j)) + (pfN->nC.xF(0, i, j) - pfN->nC.xB(0, i, j))) * kx;
        const double alpha1FluxY  = ((pfN->c.yF(0, i, j) - pfN->c.yB(0, i, j)) + (pfN->nC.yF(0, i, j) - pfN->nC.yB(0, i, j))) * ky;

        cVectorNew[0] = (cVector0[0] + (cVector1[0] - (alpha1FluxX + alpha1FluxY))) * 0.5;
        /* Utility::boundAlpha1(cVectorNew[0]); */

        // Scheme step.
        // Evalulate U0, U1, U2.
        for (size_t v = 1; v < 3; ++v) {

          const double fluxX = (pfN->c.xF(v, i, j) - pfN->c.xB(v, i, j)) * kx;
          const double fluxY = (pfN->c.yF(v, i, j) - pfN->c.yB(v, i, j)) * ky;

          cVectorNew[v] = (cVector0[v] + (cVector1[v] - (fluxX + fluxY))) * 0.5;
        }

        // Implement implicit scheme for relative velocity equations (U3, U4).
        /* const double c1 = VariablesAdditional::c1( p1(0, i, j) */
        /*                                          , p1(2, i, j) */
        /*                                          , p1(1, i, j)); */
        /* const double c2 = 1. - c1; */

        /* for (size_t v = 3; v <= 4; ++v) { */

        /*   const double flux =   (pfN->c.xF(v, i, j) - pfN->c.xB(v, i, j)) * kx */
        /*                       + (pfN->c.yF(v, i, j) - pfN->c.yB(v, i, j)) * ky; */

        /*   cVectorNew[v] = (   cVector0[v] */
        /*                     + (   cVector1[v] */
        /*                         - flux */
        /*                         + rs(v, i, j) * tau */
        /*                       ) / (1. + tau * c1 * c2 * Physics::chi) */
        /*                   ) * 0.5; */
        /* } */
        cVectorNew[3] = 0.;
        cVectorNew[4] = 0.;

        // Evalulate U5 - U10.
        for (size_t v = 5; v < 7; ++v) {

          const double fluxX = (pfN->c.xF(v, i, j) - pfN->c.xB(v, i, j)) * kx;
          const double fluxY = (pfN->c.yF(v, i, j) - pfN->c.yB(v, i, j)) * ky;

          cVectorNew[v] = (cVector0[v] + (cVector1[v] - (fluxX + fluxY))) * 0.5;
        }

        for (size_t v = 7; v < Physics::varCount; ++v) {

          const double fluxX = ((pfN->c.xF(v, i, j) - pfN->c.xB(v, i, j)) + (pfN->nC.xF(v, i, j) - pfN->nC.xB(v, i, j))) * kx;
          const double fluxY = ((pfN->c.yF(v, i, j) - pfN->c.yB(v, i, j)) + (pfN->nC.yF(v, i, j) - pfN->nC.yB(v, i, j))) * ky;

          cVectorNew[v] = (cVector0[v] + (cVector1[v] - (fluxX + fluxY))) * 0.5;
        }

        // Save new cell as primitive to p0.
        // p0 is solution.
        MeshConversion::toPrimitive(cVectorNew, p0, i, j);
      }
    }

    if (PressureRelaxation::enablePressureRelaxation) PressureRelaxation::relaxPressureMesh(p0);

    // Update iteration.
    ++iteration;

    // Update status and graps.
    if (iteration % statusInterval == 0) {
      status();

      //Utility::printMinMaxAlpha1(p0);

      //Visualization::plotMeshPhysical(p0);
      Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration));
      //Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2);
      //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2);
    }
  }

  // Show status and solution.
  status();

  //Visualization::plotMeshPhysical(p0);
  Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration));
  //Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2);
  //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2);

  // Save solution to file.
  std::string filename(   "mesh-"
                        + std::to_string(Geometry::cellCountX)
                        + "x"
                        + std::to_string(Geometry::cellCountY)
                        + "-i"
                        + std::to_string(iteration)
                        + "-t"
                        + std::to_string(timeCur)
                        + ".dat");
  Utility::saveMeshToFile(p0, filename);
}

void SchemeO2::info() const
{
  Scheme::info();

  std::cout << "Scheme: RK-2, WENO-3" << std::endl;
}

