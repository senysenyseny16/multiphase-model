//: MeshFluxNumerical.cpp

#include "MeshFluxNumerical.hpp"
#include "MeshFluxDifferential.hpp"

#include "FluxNumerical.hpp"
#include "FluxDifferential.hpp"

#include "VariablesConversion.hpp"
#include "VariablesAdditional.hpp"
#include "Energy.hpp"
#include "ShearStress.hpp"

#include "SoundSpeed.hpp"

#include <cmath>
#include <iostream>

namespace FluxNumerical {

MeshFluxNumerical::MeshFluxNumerical()
{ }

MeshFluxNumerical::~MeshFluxNumerical()
{ }

HLLC::HLLC()
  : MeshFluxNumerical()
{ }


void HLLC::calcFlux( const MeshFluxDifferential& fD
                   , const MeshEdge&             rC
                   , const MeshEdge&             rP
                   , const Mesh&                 p)
{
  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell + 1; i <= Geometry::lastCellX; ++i) {

      HLLCX( fD.dataXF()
           , fD.dataXB()
           , rC.dataXF()
           , rC.dataXB()
           , rP.dataXF()
           , rP.dataXB()
           , p
           , i - 1
           , i
           , j
           , c. dataXF()
           , c. dataXB()
           , nC.dataXF()
           , nC.dataXB());
    }
  }

  #pragma omp parallel for
  for (size_t j = Geometry::firstCell + 1; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      HLLCY( fD.dataYF()
           , fD.dataYB()
           , rC.dataYF()
           , rC.dataYB()
           , rP.dataYF()
           , rP.dataYB()
           , p
           , i
           , j - 1
           , j
           , c. dataYF()
           , c. dataYB()
           , nC.dataYF()
           , nC.dataYB());
    }
  }
}

void HLLC::HLLCX( const Mesh& fDB
                , const Mesh& fDF
                , const Mesh& rCB
                , const Mesh& rCF
                , const Mesh& rPB
                , const Mesh& rPF
                , const Mesh& p
                , const size_t iB
                , const size_t iF
                , const size_t j
                , Mesh& cB
                , Mesh& cF
                , Mesh& nCB
                , Mesh& nCF)
{
  // Unpack variables.
  const double alpha1B = rPB(0,  iB, j);
  const double rho2B   = rPB(1,  iB, j);
  const double rho1B   = rPB(2,  iB, j);
  const double u11B    = rPB(3,  iB, j);
  const double u12B    = rPB(4,  iB, j);
  const double u21B    = rPB(5,  iB, j);
  const double u22B    = rPB(6,  iB, j);
  const double F11B    = rPB(7,  iB, j);
  const double F12B    = rPB(8,  iB, j);
  const double F21B    = rPB(9,  iB, j);
  const double F22B    = rPB(10, iB, j);

  const double alpha1F = rPF(0,  iF, j);
  const double rho2F   = rPF(1,  iF, j);
  const double rho1F   = rPF(2,  iF, j);
  const double u11F    = rPF(3,  iF, j);
  const double u12F    = rPF(4,  iF, j);
  const double u21F    = rPF(5,  iF, j);
  const double u22F    = rPF(6,  iF, j);
  const double F11F    = rPF(7,  iF, j);
  const double F12F    = rPF(8,  iF, j);
  const double F21F    = rPF(9,  iF, j);
  const double F22F    = rPF(10, iF, j);

  // 1. Find SB, SF.

  // 1. 1. Find longitudinal velocity of mixture.
  const double c1B = VariablesAdditional::c1(alpha1B, rho1B, rho2B);
  const double c2B = 1. - c1B;

  const double c1F = VariablesAdditional::c1(alpha1F, rho1F, rho2F);
  const double c2F = 1. - c1F;

  const double longVelocityMixtureB = SoundSpeed::longitudinalVelocityMixture(rho1B, rho2B, c1B);
  const double longVelocityMixtureF = SoundSpeed::longitudinalVelocityMixture(rho1F, rho2F, c1F);

  // 1. 2. Calc mixture velocity.
  const double u1B = VariablesAdditional::u1(c1B, c2B, u11B, u12B);
  const double u1F = VariablesAdditional::u1(c1F, c2F, u11F, u12F);

  // 1. 3. Find SB, SF.
  const double SB = std::min(u1B - longVelocityMixtureB, u1F - longVelocityMixtureF);
  const double SF = std::max(u1B + longVelocityMixtureB, u1F + longVelocityMixtureF);

  // 2. Find SM.
  const double totalStress11B = -(alpha1B * Energy::p1(rho1B) + (1. - alpha1B) * Energy::p2(rho2B)) + alpha1B * ShearStress::sigma11(rho1B, F11B, F12B, F21B, F22B);
  const double totalStress11F = -(alpha1F * Energy::p1(rho1F) + (1. - alpha1F) * Energy::p2(rho2F)) + alpha1F * ShearStress::sigma11(rho1F, F11F, F12F, F21F, F22F);

  const double rhoB = VariablesAdditional::rho(alpha1B, rho1B, rho2B);
  const double rhoF = VariablesAdditional::rho(alpha1F, rho1F, rho2F);

  const double SM = (rhoB * (u1B * (u1B - SB)) - rhoF * (u1F * (u1F - SF)) + (totalStress11F - totalStress11B)) / (rhoB * (u1B - SB) - rhoF * (u1F - SF));


  // 3. Find TotalStress21M.

  const double u2B = VariablesAdditional::u2(c1B, c2B, u21B, u22B);
  const double u2F = VariablesAdditional::u2(c1F, c2F, u21F, u22F);

  const double totalStress21B = alpha1B * ShearStress::sigma21(rho1B, F11B, F12B, F21B, F22B);
  const double totalStress21F = alpha1F * ShearStress::sigma21(rho1F, F11F, F12F, F21F, F22F);

  // Sliding treatment.
  double totalStress21M;
  if (alpha1B > SlidingTreatment::eps and alpha1F > SlidingTreatment::eps) {

    totalStress21M = ((u1F - SF) * rhoF * totalStress21B - (u1B - SB) * rhoB * totalStress21F + (u1B - SB) * (u1F - SF) * (u2F - u2B) * (rhoB * rhoF)) / ((u1F - SF) * rhoF - (u1B - SB) * rhoB);

  } else {

    // Sliding.
    totalStress21M = 0.;

  }


  // 3. Calc HLLC B-flux.

  // 3. 1. SB <= 0 and SM >= 0.

  if (SB <= 0. and SM > 0.) {

    // Calc UM.
    const double alpha1 = alpha1B;

    const double mulDensity = (SB - u1B) / (SB - SM);
    const double rho    = rhoB  * mulDensity;
    const double rho1   = rho1B * mulDensity;
    const double rho2   = rho2B * mulDensity;

    assert(rho > 0.);
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    const double u2 = u2B + (totalStress21M - totalStress21B) / ((u1B - SB) * rhoB);

    const double rhoF11 = rhoB * F11B;
    const double rhoF12 = rhoB * F12B;
    const double rhoF21 = rhoB * (F21B * (SB - u1B) - F11B * (u2 - u2B)) / (SB - SM);
    const double rhoF22 = rhoB * (F22B * (SB - u1B) - F12B * (u2 - u2B)) / (SB - SM);

    // Form conservative vectors.
    double U[Physics::varCount];

    U[0]  = alpha1;
    U[1]  = (1. - alpha1) * rho2;
    U[2]  = alpha1 * rho1;
    U[3]  = 0.; // w1
    U[4]  = 0.; // w2
    U[5]  = rho * SM;
    U[6]  = rho * u2;
    U[7]  = rhoF11;
    U[8]  = rhoF12;
    U[9]  = rhoF21;
    U[10] = rhoF22;

    // Calc HLLC flux.
    // Calc conservative part.
    cB(0, iB, j) = SM * alpha1;
    cF(0, iF, j) = cB(0, iB, j); // copy

    for (size_t v = 1; v < 7; ++v) {
      cB(v, iB, j) = fDB(v, iB, j) + SB * (U[v] - rCB(v, iB, j));
      cF(v, iF, j) = cB(v, iB, j); // copy
    }
    cB(7,  iB, j) = rhoF11 * SM;
    cB(8,  iB, j) = rhoF12 * SM;
    cB(9,  iB, j) = rhoF21 * SM;
    cB(10, iB, j) = rhoF22 * SM;
    for (size_t v = 7; v < Physics::varCount; ++v) {
      cF(v, iF, j) = cB(v, iB, j); // copy
    }

    // Calc non-conservative part.
    nCB(0,  iB, j) = -p(0, iB, j) * SM;
    const double rhoAveragedB = VariablesAdditional::rho(p(0, iB, j), p(2, iB, j), p(1, iB, j));
    nCB(7,  iB, j) = -(rhoAveragedB * p(7, iB, j)) * SM;
    nCB(8,  iB, j) = -(rhoAveragedB * p(8, iB, j)) * SM;
    nCB(9,  iB, j) = -(rhoAveragedB * p(7, iB, j)) * u2;
    nCB(10, iB, j) = -(rhoAveragedB * p(8, iB, j)) * u2;

    nCF(0,  iF, j) = -p(0, iF, j) * SM;
    const double rhoAveragedF = VariablesAdditional::rho(p(0, iF, j), p(2, iF, j), p(1, iF, j));
    nCF(7,  iF, j) = -(rhoAveragedF * p(7, iF, j)) * SM;
    nCF(8,  iF, j) = -(rhoAveragedF * p(8, iF, j)) * SM;
    nCF(9,  iF, j) = -(rhoAveragedF * p(7, iF, j)) * u2;
    nCF(10, iF, j) = -(rhoAveragedF * p(8, iF, j)) * u2;

  } else if (SM <= 0. and SF >= 0.) { // 3. 2. SM <= 0 and SF >= 0.

    // Calc UM.
    const double alpha1 = alpha1F;

    const double mulDensity = (SF - u1F) / (SF - SM);
    const double rho    = rhoF  * mulDensity;
    const double rho1   = rho1F * mulDensity;
    const double rho2   = rho2F * mulDensity;

    assert(rho > 0.);
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    const double u2 = u2F + (totalStress21M - totalStress21F) / ((u1F - SF) * rhoF);

    const double rhoF11 = rhoF * F11F;
    const double rhoF12 = rhoF * F12F;
    const double rhoF21 = rhoF * (F21F * (SF - u1F) - F11F * (u2 - u2F)) / (SF - SM);
    const double rhoF22 = rhoF * (F22F * (SF - u1F) - F12F * (u2 - u2F)) / (SF - SM);

    // Form conservative vectors.
    double U[Physics::varCount];

    U[0]  = alpha1;
    U[1]  = (1. - alpha1) * rho2;
    U[2]  = alpha1 * rho1;
    U[3]  = 0.; // w1
    U[4]  = 0.; // w2
    U[5]  = rho * SM;
    U[6]  = rho * u2;
    U[7]  = rhoF11;
    U[8]  = rhoF12;
    U[9]  = rhoF21;
    U[10] = rhoF22;

    // Calc HLLC flux.
    // Calc conservative part.
    cB(0, iB, j) = SM * alpha1;
    cF(0, iF, j) = cB(0, iB, j); // copy

    for (size_t v = 1; v < 7; ++v) {
      cB(v, iB, j) = fDF(v, iF, j) + SF * (U[v] - rCF(v, iF, j));
      cF(v, iF, j) = cB(v, iB, j); // copy
    }
    cB(7,  iB, j) = rhoF11 * SM;
    cB(8,  iB, j) = rhoF12 * SM;
    cB(9,  iB, j) = rhoF21 * SM;
    cB(10, iB, j) = rhoF22 * SM;
    for (size_t v = 7; v < Physics::varCount; ++v) {
      cF(v, iF, j) = cB(v, iB, j); // copy
    }

    nCB(0,  iB, j) = -p(0, iB, j) * SM;
    const double rhoAveragedB = VariablesAdditional::rho(p(0, iB, j), p(2, iB, j), p(1, iB, j));
    nCB(7,  iB, j) = -(rhoAveragedB * p(7, iB, j)) * SM;
    nCB(8,  iB, j) = -(rhoAveragedB * p(8, iB, j)) * SM;
    nCB(9,  iB, j) = -(rhoAveragedB * p(7, iB, j)) * u2;
    nCB(10, iB, j) = -(rhoAveragedB * p(8, iB, j)) * u2;

    nCF(0,  iF, j) = -p(0, iF, j) * SM;
    const double rhoAveragedF = VariablesAdditional::rho(p(0, iF, j), p(2, iF, j), p(1, iF, j));
    nCF(7,  iF, j) = -(rhoAveragedF * p(7, iF, j)) * SM;
    nCF(8,  iF, j) = -(rhoAveragedF * p(8, iF, j)) * SM;
    nCF(9,  iF, j) = -(rhoAveragedF * p(7, iF, j)) * u2;
    nCF(10, iF, j) = -(rhoAveragedF * p(8, iF, j)) * u2;

  } else if (SB >= 0.) { // 3. 3. SB >= 0.

    std::cout << "Supersonic flow. Abort." << std::endl;
    std::abort();

    for (size_t v = 0; v < Physics::varCount; ++v) {
      cB(v, iB, j) = fDB(v, iB, j);
      cF(v, iF, j) = cB(v, iB, j); // copy
    }

  } else { // 3. 4. SF <= 0.

    std::cout << "Supersonic flow. Abort." << std::endl;
    std::abort();

    for (size_t v = 0; v < Physics::varCount; ++v) {
      cB(v, iB, j) = fDF(v, iF, j);
      cF(v, iF, j) = cB(v, iB, j); // copy
    }

  }
}

void HLLC::HLLCY( const Mesh& fDB
                , const Mesh& fDF
                , const Mesh& rCB
                , const Mesh& rCF
                , const Mesh& rPB
                , const Mesh& rPF
                , const Mesh& p
                , const size_t i
                , const size_t jB
                , const size_t jF
                , Mesh& cB
                , Mesh& cF
                , Mesh& nCB
                , Mesh& nCF)
{
  // Unpack variables.
  const double alpha1B = rPB(0,  i, jB);
  const double rho2B   = rPB(1,  i, jB);
  const double rho1B   = rPB(2,  i, jB);
  const double u11B    = rPB(3,  i, jB);
  const double u12B    = rPB(4,  i, jB);
  const double u21B    = rPB(5,  i, jB);
  const double u22B    = rPB(6,  i, jB);
  const double F11B    = rPB(7,  i, jB);
  const double F12B    = rPB(8,  i, jB);
  const double F21B    = rPB(9,  i, jB);
  const double F22B    = rPB(10, i, jB);

  const double alpha1F = rPF(0,  i, jF);
  const double rho2F   = rPF(1,  i, jF);
  const double rho1F   = rPF(2,  i, jF);
  const double u11F    = rPF(3,  i, jF);
  const double u12F    = rPF(4,  i, jF);
  const double u21F    = rPF(5,  i, jF);
  const double u22F    = rPF(6,  i, jF);
  const double F11F    = rPF(7,  i, jF);
  const double F12F    = rPF(8,  i, jF);
  const double F21F    = rPF(9,  i, jF);
  const double F22F    = rPF(10, i, jF);

  // 1. Find SB, SF.

  // 1. 1. Find longitudinal velocity of mixture.
  const double c1B = VariablesAdditional::c1(alpha1B, rho1B, rho2B);
  const double c2B = 1. - c1B;

  const double c1F = VariablesAdditional::c1(alpha1F, rho1F, rho2F);
  const double c2F = 1. - c1F;

  const double longVelocityMixtureB = SoundSpeed::longitudinalVelocityMixture(rho1B, rho2B, c1B);
  const double longVelocityMixtureF = SoundSpeed::longitudinalVelocityMixture(rho1F, rho2F, c1F);

  // 1. 2. Calc mixture velocity.
  const double u2B = VariablesAdditional::u2(c1B, c2B, u21B, u22B);
  const double u2F = VariablesAdditional::u2(c1F, c2F, u21F, u22F);

  // 1. 3. Find SB, SF.
  const double SB = std::min(u2B - longVelocityMixtureB, u2F - longVelocityMixtureF);
  const double SF = std::max(u2B + longVelocityMixtureB, u2F + longVelocityMixtureF);


  // 2. Find SM.
  const double totalStress22B = -(alpha1B * Energy::p1(rho1B) + (1. - alpha1B) * Energy::p2(rho2B)) + alpha1B * ShearStress::sigma22(rho1B, F11B, F12B, F21B, F22B);
  const double totalStress22F = -(alpha1F * Energy::p1(rho1F) + (1. - alpha1F) * Energy::p2(rho2F)) + alpha1F * ShearStress::sigma22(rho1F, F11F, F12F, F21F, F22F);

  const double rhoB = VariablesAdditional::rho(alpha1B, rho1B, rho2B);
  const double rhoF = VariablesAdditional::rho(alpha1F, rho1F, rho2F);

  const double SM = (rhoB * (u2B * (u2B - SB)) - rhoF * (u2F * (u2F - SF)) + (totalStress22F - totalStress22B)) / (rhoB * (u2B - SB) - rhoF * (u2F - SF));


  // 3. Find TotalStress12M.

  const double u1B = VariablesAdditional::u1(c1B, c2B, u11B, u12B);
  const double u1F = VariablesAdditional::u1(c1F, c2F, u11F, u12F);

  const double totalStress12B = alpha1B * ShearStress::sigma12(rho1B, F11B, F12B, F21B, F22B);
  const double totalStress12F = alpha1F * ShearStress::sigma12(rho1F, F11F, F12F, F21F, F22F);

  // Sliding treatment.
  double totalStress12M;
  if (alpha1B > SlidingTreatment::eps and alpha1F > SlidingTreatment::eps) {

    totalStress12M = ((u2F - SF) * rhoF * totalStress12B - (u2B - SB) * rhoB * totalStress12F + (u2B - SB) * (u2F - SF) * (u1F - u1B) * (rhoB * rhoF)) / ((u2F - SF) * rhoF - (u2B - SB) * rhoB);

  } else {

    // Sliding.
    totalStress12M = 0.;

  }


  // 3. Calc HLLC B-flux.

  // 3. 1. SB <= 0 and SM >= 0.

  if (SB <= 0. and SM > 0.) {

    // Calc UM.
    const double alpha1 = alpha1B;

    const double mulDensity = (SB - u2B) / (SB - SM);
    const double rho    = rhoB  * mulDensity;
    const double rho1   = rho1B * mulDensity;
    const double rho2   = rho2B * mulDensity;

    assert(rho > 0.);
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    const double u1 = u1B + (totalStress12M - totalStress12B) / ((u2B - SB) * rhoB);

    const double rhoF21 = rhoB * F21B;
    const double rhoF22 = rhoB * F22B;
    const double rhoF11 = rhoB * (F11B * (SB - u2B) - F21B * (u1 - u1B)) / (SB - SM);
    const double rhoF12 = rhoB * (F12B * (SB - u2B) - F22B * (u1 - u1B)) / (SB - SM);

    // Form conservative vectors.
    double U[Physics::varCount];

    U[0]  = alpha1;
    U[1]  = (1. - alpha1) * rho2;
    U[2]  = alpha1 * rho1;
    U[3]  = 0.; // w1
    U[4]  = 0.; // w2
    U[5]  = rho * u1;
    U[6]  = rho * SM;
    U[7]  = rhoF11;
    U[8]  = rhoF12;
    U[9]  = rhoF21;
    U[10] = rhoF22;

    // Calc HLLC flux.
    // Calc conservative part.
    cB(0, i, jB) = SM * alpha1;
    cF(0, i, jF) = cB(0, i, jB); // copy

    for (size_t v = 1; v < 7; ++v) {
      cB(v, i, jB) = fDB(v, i, jB) + SB * (U[v] - rCB(v, i, jB));
      cF(v, i, jF) = cB(v, i, jB); // copy
    }
    cB(7,  i, jB) = rhoF11 * SM;
    cB(8,  i, jB) = rhoF12 * SM;
    cB(9,  i, jB) = rhoF21 * SM;
    cB(10, i, jB) = rhoF22 * SM;
    for (size_t v = 7; v < Physics::varCount; ++v) {
      cF(v, i, jF) = cB(v, i, jB); // copy
    }

    // Calc non-conservative part.
    nCB(0,  i, jB) = -p(0, i, jB) * SM;
    const double rhoAveragedB = VariablesAdditional::rho(p(0, i, jB), p(2, i, jB), p(1, i, jB));
    nCB(7,  i, jB) = -(rhoAveragedB * p(9,  i, jB)) * u1;
    nCB(8,  i, jB) = -(rhoAveragedB * p(10, i, jB)) * u1;
    nCB(9,  i, jB) = -(rhoAveragedB * p(9,  i, jB)) * SM;
    nCB(10, i, jB) = -(rhoAveragedB * p(10, i, jB)) * SM;

    nCF(0,  i, jF) = -p(0, i, jF) * SM;
    const double rhoAveragedF = VariablesAdditional::rho(p(0, i, jF), p(2, i, jF), p(1, i, jF));
    nCF(7,  i, jF) = -(rhoAveragedF * p(9,  i, jF)) * u1;
    nCF(8,  i, jF) = -(rhoAveragedF * p(10, i, jF)) * u1;
    nCF(9,  i, jF) = -(rhoAveragedF * p(9,  i, jF)) * SM;
    nCF(10, i, jF) = -(rhoAveragedF * p(10, i, jF)) * SM;

  } else if (SM <= 0. and SF >= 0.) { // 3. 2. SM <= 0 and SF >= 0.

    // Calc UM.
    const double alpha1 = alpha1F;

    const double mulDensity = (SF - u2F) / (SF - SM);
    const double rho    = rhoF  * mulDensity;
    const double rho1   = rho1F * mulDensity;
    const double rho2   = rho2F * mulDensity;

    assert(rho > 0.);
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    const double u1 = u1F + (totalStress12M - totalStress12F) / ((u2F - SF) * rhoF);

    const double rhoF21 = rhoF * F21F;
    const double rhoF22 = rhoF * F22F;
    const double rhoF11 = rhoF * (F11F * (SF - u2F) - F21F * (u1 - u1F)) / (SF - SM);
    const double rhoF12 = rhoF * (F12F * (SF - u2F) - F22F * (u1 - u1F)) / (SF - SM);

    // Form conservative vectors.
    double U[Physics::varCount];

    U[0]  = alpha1;
    U[1]  = (1. - alpha1) * rho2;
    U[2]  = alpha1 * rho1;
    U[3]  = 0.; // w1
    U[4]  = 0.; // w2
    U[5]  = rho * u1;
    U[6]  = rho * SM;
    U[7]  = rhoF11;
    U[8]  = rhoF12;
    U[9]  = rhoF21;
    U[10] = rhoF22;

    // Calc HLLC flux.
    // Calc conservative part.
    cB(0, i, jB) = SM * alpha1;
    cF(0, i, jF) = cB(0, i, jB); // copy

    for (size_t v = 1; v < 7; ++v) {
      cB(v, i, jB) = fDF(v, i, jF) + SF * (U[v] - rCF(v, i, jF));
      cF(v, i, jF) = cB(v, i, jB); // copy
    }
    cB(7,  i, jB) = rhoF11 * SM;
    cB(8,  i, jB) = rhoF12 * SM;
    cB(9,  i, jB) = rhoF21 * SM;
    cB(10, i, jB) = rhoF22 * SM;
    for (size_t v = 7; v < Physics::varCount; ++v) {
      cF(v, i, jF) = cB(v, i, jB); // copy
    }

    // Calc non-conservative part.
    nCB(0,  i, jB) = -p(0, i, jB) * SM;
    const double rhoAveragedB = VariablesAdditional::rho(p(0, i, jB), p(2, i, jB), p(1, i, jB));
    nCB(7,  i, jB) = -(rhoAveragedB * p(9,  i, jB)) * u1;
    nCB(8,  i, jB) = -(rhoAveragedB * p(10, i, jB)) * u1;
    nCB(9,  i, jB) = -(rhoAveragedB * p(9,  i, jB)) * SM;
    nCB(10, i, jB) = -(rhoAveragedB * p(10, i, jB)) * SM;

    nCF(0,  i, jF) = -p(0, i, jF) * SM;
    const double rhoAveragedF = VariablesAdditional::rho(p(0, i, jF), p(2, i, jF), p(1, i, jF));
    nCF(7,  i, jF) = -(rhoAveragedF * p(9,  i, jF)) * u1;
    nCF(8,  i, jF) = -(rhoAveragedF * p(10, i, jF)) * u1;
    nCF(9,  i, jF) = -(rhoAveragedF * p(9,  i, jF)) * SM;
    nCF(10, i, jF) = -(rhoAveragedF * p(10, i, jF)) * SM;

  } else if (SB >= 0.) { // 3. 3. SB >= 0.

    std::cout << "Supersonic flow. Abort." << std::endl;
    std::abort();

    for (size_t v = 0; v < Physics::varCount; ++v) {
      cB(v, i, jB) = fDB(v, i, jB);
      cF(v, i, jF) = cB(v, i, jB); // copy
    }

  } else { // 3. 4. SF <= 0.

    std::cout << "Supersonic flow. Abort." << std::endl;
    std::abort();

    for (size_t v = 0; v < Physics::varCount; ++v) {
      cB(v, i, jB) = fDF(v, i, jF);
      cF(v, i, jF) = cB(v, i, jB); // copy
    }
  }
}

MeshFluxNumerical* createMeshFluxNumerical()
{
  switch (SchemePars::fluxType) {

  case (SchemePars::FluxType::HLLC):

    return (new HLLC);

  break;

  default:

    return nullptr;

  break;
  }
}

} // namespace FluxNumerical

