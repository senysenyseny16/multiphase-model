//: Scheme.cpp

#include "Scheme.hpp"
#include "Definitions.hpp"
#include "Utility.hpp"
#include "SoundSpeed.hpp"
#include "VariablesAdditional.hpp"

#include <iostream>
#include <ctime>
#include <iomanip>

Scheme::Scheme(const Mesh& initialData)
  : tau(0.)
  , timeCur(0.)
  , iteration(0)
  , saveInterval  (SchemePars::defaultSaveInterval)
  , statusInterval(SchemePars::defaultStatusInterval)
  , p0(initialData)
{}

Scheme::~Scheme()
{}

void Scheme::status() const
{
  using namespace std;

  cout << endl;
  cout.setf(ios::left, ios::adjustfield);

  time_t realTime = time(0); // Get current time.
  struct tm* now = localtime(&realTime);
  cout << asctime(now);

  cout << setw(15) << "iteration"
       << setw(15) << "time"
       << setw(15) << "tau"
       << endl;
  cout << setfill('-') << setw(45) << '-'
       << setfill(' ')
       << endl;
  cout << setw(15) << iteration
       << setw(15) << timeCur
       << setw(15) << tau
       << endl;
}

void Scheme::info() const
{
  std::cout << std::endl;

  std::cout << "Mesh: ("
            << Geometry::cellCountX
            << ", "
            << Geometry::cellCountY
            << ")"
            << std::endl;

  std::cout << "hx: " << SchemePars::hx << ", hy: " << SchemePars::hy << std::endl;

  Utility::printFluxType();
}

void Scheme::setSaveInterval(const size_t saveInterval)
{
  this->saveInterval = saveInterval;
}

void Scheme::setStatusInterval(const size_t statusInterval)
{
  this->statusInterval = statusInterval;
}

void Scheme::calcTau(const double maxCX, const double maxCY)
{
  using namespace SchemePars;

  tau = CFL * (hx * hy) / (2. * maxCX * hy + 2. * maxCY * hx);
}

const Mesh& Scheme::physicalVars() const
{
  return p0;
}

