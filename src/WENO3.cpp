//: WENO3.cpp

#include "WENO.hpp"
#include "Definitions.hpp" // for SchemePars::WenoEps.

namespace WENO {

void weno3( const double vB
          , const double vC
          , const double vF
          , double& recB
          , double& recF)
{
  // 1. Obtain 2 reconstructed values v0_{i+0.5}, v1_{i+0.5},
  //    of 2-th order accuracy, based on the stencils
  //    {x_i, x_{i+1}}, {x_{i-1}, x_i}.
  const double v0F =  0.5 * (vC + vF);
  const double v1F = -0.5 * vB + 1.5 * vC;

  // 2. Also obtain 2 reconstructed values v0_{i-0.5}, v1_{i-0.5},
  //    of 2-th order accuracy, again based on the stencils
  //    {x_i, x_{i+1}}, {x_{i-1}, x_i}.
  const double v0B = 1.5 * vC - 0.5 * vF;
  const double v1B = 0.5 * (vB + vC);

  // 3. Find constants d_F and d_B.
  const double d0F = 2. / 3.;
  const double d1F = 1. / 3.;

  const double d0B = d1F;
  const double d1B = d0F;

  // 4. Find smooth indicators.
  const double B0 = (vF - vC) * (vF - vC);
  const double B1 = (vC - vB) * (vC - vB);

  // 5. Form the weights w.
  const double denominator1 = (SchemePars::wenoEps + B0) * (SchemePars::wenoEps + B0);
  const double denominator2 = (SchemePars::wenoEps + B1) * (SchemePars::wenoEps + B1);

  const double a0F = d0F / denominator1;
  const double a1F = d1F / denominator2;

  const double w0F = a0F / (a0F + a1F);
  const double w1F = a1F / (a0F + a1F);

  const double a0B = d0B / denominator1;
  const double a1B = d1B / denominator2;

  const double w0B = a0B / (a0B + a1B);
  const double w1B = a1B / (a0B + a1B);

  // 6. Find reconstruction.
  recB = w0B * v0B + w1B * v1B;
  recF = w0F * v0F + w1F * v1F;
}

} // namespace WENO

