//: UtilityEigenvectors.cpp

#include "UtilityEigenvectors.hpp"
#include "Definitions.hpp"

#include <cassert>

namespace Utility {

Eigen::Matrix11 eigenVectorsDecomp( const Eigen::Matrix11& A
                                  , const Eigen::VectorRow11& eigenValues
                                  , const Eigen::Matrix11&    eigenVectors)
{
  // Collect all 6 non-zero eigenvectors.
  Eigen::Matrix<double, 6, 11> nonZeroEigenVectors;

  size_t rowCounter = 0;

  for (size_t evalueNum = 0; evalueNum < Physics::varCount; ++evalueNum) {
    if (std::abs(eigenValues(evalueNum)) > BC::zeroEpsSlipHybrid) {

      nonZeroEigenVectors.row(rowCounter++) = eigenVectors.row(evalueNum);

    }
  }

  assert(rowCounter == 6);

  const Eigen::Matrix<double, 11, 6> nonZeroEigenVectorsT = nonZeroEigenVectors.transpose();

  const Eigen::Matrix11 AT = A.transpose();

  // Form A* vi, i = 1,2,...,6.
  Eigen::Matrix<double, 6, 11> B;
  B.row(0) = AT * nonZeroEigenVectorsT.col(0);
  B.row(1) = AT * nonZeroEigenVectorsT.col(1);
  B.row(2) = AT * nonZeroEigenVectorsT.col(2);
  B.row(3) = AT * nonZeroEigenVectorsT.col(3);
  B.row(4) = AT * nonZeroEigenVectorsT.col(4);
  B.row(5) = AT * nonZeroEigenVectorsT.col(5);

  // Find kernel A* vi.
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(B, Eigen::ComputeFullV);

  Eigen::Matrix11 eigenVectorsFixed;

  // Form fixed system of eigenvectors.
  eigenVectorsFixed.row(0) = nonZeroEigenVectors.row(0);
  eigenVectorsFixed.row(1) = nonZeroEigenVectors.row(1);
  eigenVectorsFixed.row(2) = nonZeroEigenVectors.row(2);
  eigenVectorsFixed.row(3) = nonZeroEigenVectors.row(3);
  eigenVectorsFixed.row(4) = nonZeroEigenVectors.row(4);
  eigenVectorsFixed.row(5) = nonZeroEigenVectors.row(5);

  eigenVectorsFixed.row(6)  = svd.matrixV().col(6) .transpose();
  eigenVectorsFixed.row(7)  = svd.matrixV().col(7) .transpose();
  eigenVectorsFixed.row(8)  = svd.matrixV().col(8) .transpose();
  eigenVectorsFixed.row(9)  = svd.matrixV().col(9) .transpose();
  eigenVectorsFixed.row(10) = svd.matrixV().col(10).transpose();

  #if !defined(NDEBUG) // Check rank.
    Eigen::FullPivLU<Eigen::Matrix11> lu_decomp(eigenVectorsFixed);
    auto rank = lu_decomp.rank();
    assert(rank == Physics::varCount);
  #endif

  return eigenVectorsFixed;
}

} // namespace Utility

