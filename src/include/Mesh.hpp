//: Mesh.hpp

#ifndef MESH_HPP
#define MESH_HPP

#include <vector>
#include <iosfwd>

/** @class Mesh
 *  @brief 3D array for keeping variables in calculation area.
 *  All geometry parameters are defined in Definitions.hpp.
 *
 *  Mesh structure (1D sketch):
 *
 *   Geometry::firstCell       Geometry::lastCellX / Geometry::lastCellY
 *           |                                     |
 *           v                                     v
 *  x .. x | o o o ........................... o o o | x .. x,
 *  ^                                                       ^
 *  |                                                       |
 *  Zero index                          Geometry::MeshSizeX / Geometry::MeshSizeY
 *
 *  where x - ghost cell (Geometry::cellGhostCount),
 *        o - normal cell.
 *
 *  The first normal cell have index Geometry::firstCell,
 *      last in x-direction - Geometry::lastCellX,
 *           in y-direction - Geometry::lastCellY.
 *
 *  Use row-major loop style:
 *  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
 *    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
 *      // do something with mesh(v, i, j).
 *    }
 *  }
 *
 *  ! Mesh dosn't allow you to use negative indexes.
 */
class Mesh {
 public:

  /** Ctor **/
  Mesh();

  /** Mesh copy. **/
  Mesh(const Mesh&);
  Mesh& operator=(const Mesh&);

  virtual ~Mesh();

  /** Operator to access the elements of array (const).
   *  syntax: mesh(var, i, j).
   *
   *  @param var - variable number.
   *  @param i   - x-coordinate.
   *  @param j   - y-coordinate.
   */
  double  operator()(size_t var, size_t i, size_t j) const;

  /** Operator to access the elements of array (by reference).
   *  syntax: mesh(var, i, j).
   *
   *  @param var - variable number.
   *  @param i   - x-coordinate.
   *  @param j   - y-coordinate.
   */
  double& operator()(size_t var, size_t i, size_t j);

  /** Overload ostream operator. */
  friend std::ostream& operator<<(std::ostream& os, const Mesh& mesh);

 protected:

  /// Emulate 3d array.
  std::vector<double> data;
};

#include "Mesh.inl" // Inlined bodies for both operators ().

#endif // MESH_HPP

