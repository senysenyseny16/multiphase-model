//: MeshEdge.hpp

#ifndef MESH_EDGE_HPP
#define MESH_EDGE_HPP

#include "Mesh.hpp"

/** @class MeshEdge
 *  @brief MeshEdge contains four variables on the cells edges.
 */
class MeshEdge {
 public:

  /** Allocate memory for variables. */
  MeshEdge();

  virtual ~MeshEdge();

  /** Get value in backward x-direction. */
  double xB(size_t var, size_t i, size_t j) const;

  /** Get value in forward  x-direction. */
  double xF(size_t var, size_t i, size_t j) const;

  /** Get value in backward y-direction. */
  double yB(size_t var, size_t i, size_t j) const;

  /** Get value in forward  y-direction. */
  double yF(size_t var, size_t i, size_t j) const;

  /** Get ref in backward x-direction. */
  double& xB(size_t var, size_t i, size_t j);

  /** Get ref in forward  x-direction. */
  double& xF(size_t var, size_t i, size_t j);

  /** Get ref in backward y-direction. */
  double& yB(size_t var, size_t i, size_t j);

  /** Get ref in forward  y-direction. */
  double& yF(size_t var, size_t i, size_t j);

  /** Get const ref to meshes. */
  const Mesh& dataXB() const;
  const Mesh& dataXF() const;
  const Mesh& dataYB() const;
  const Mesh& dataYF() const;

  /** Get ref to meshes. */
  Mesh& dataXB();
  Mesh& dataXF();
  Mesh& dataYB();
  Mesh& dataYF();

 protected:

  /// Storage for values in backward (B) x-direction: x - dx / 2.
  Mesh meshXB;
  /// Storage for values in forward  (F) x-direction: x + dx / 2.
  Mesh meshXF;
  /// Storage for values in backward (B) y-direction: y - dy / 2.
  Mesh meshYB;
  /// Storage for values in forward  (F) y-direction: y + dy / 2.
  Mesh meshYF;
};

#include "MeshEdge.inl" // Inlined bodies for xB, xF, yB, yF and data ref.

#endif // MESH_EDGE_HPP

