//: MeshNCFluxAlpha1.hpp

#ifndef MESH_NC_FLUX_ALPHA1_HPP
#define MESH_NC_FLUX_ALPHA1_HPP

#include "MeshEdge1D.hpp"

class Mesh;
class MeshRec;

namespace FluxNumerical {

/** @class MeshNCFluxAlpha1
 *  @brief MeshEdge1D for alpha1 (nonconservative) numerical flux.
 *
 *  Edit Definitions.hpp: SchemePars::fluxType to change type of used flux.
 */
class MeshNCFluxAlpha1: public MeshEdge1D {
 public:

  MeshNCFluxAlpha1();

  virtual ~MeshNCFluxAlpha1() = 0;

  /** Calculate alpha1 numerical flux on the edges of cells.
   *
   *  @param rec is reconstructed primitive variables.
   *  @param primitive is mesh with primitive variables.
   */
  virtual void calcFlux( const MeshRec&          rec
                       , const Mesh&             primitive
                       ) = 0;
};


class MeshNCFluxAlpha1HLLC: public MeshNCFluxAlpha1 {
 public:

  MeshNCFluxAlpha1HLLC();

  ~MeshNCFluxAlpha1HLLC();

  void calcFlux( const MeshRec&  rec
               , const Mesh&     primitive);
};

MeshNCFluxAlpha1* createMeshNCFluxAlpha1();

}

#endif // MESH_NC_FLUX_ALPHA1_HPP

