//: UtilityEigenvectors.hpp

#ifndef UTILITY_EIGENVECTORS_HPP
#define UTILITY_EIGENVECTORS_HPP

#include "DefinitionsEigen.hpp"

namespace Utility {

/* Special eigenvectors decomposition for system with 5 zero-eigenvalues.
 *
 * @param A - matrix with 5 zero-eigenvalues.
 * @param eigenValues of matrix A
 * @param eigenVectors (bad) of matrix A
 * 
 * @return fixed system of eigenvectors with proper rank.
 */
Eigen::Matrix11 eigenVectorsDecomp( const Eigen::Matrix11& A
                                  , const Eigen::VectorRow11& eigenValues
                                  , const Eigen::Matrix11&    eigenVectors);

} // namespace Utility

#endif // UTILITY_EIGENVECTORS_HPP

