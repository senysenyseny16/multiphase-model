//: VariablesConversion.hpp

#ifndef VARIABLES_CONVERSION_HPP
#define VARIABLES_CONVERSION_HPP

/** VariablesConversion provide functions for convert
 *  primitive (physical) variables to conservative
 *  and vice versa.
 */
namespace VariablesConversion {

/** Main 11 primitive variables:
 *  alpha1 - volume fraction of elastic porous medium,
 *  rho2   - mass density of liquid,
 *  rho1   - mass density of elastic porous medium,
 *  u11    - velocity component along x axis of porous medium,
 *  u12    - velocity component along x axis of liquid,
 *  u21    - velocity component along y axis of porous medium,
 *  u22    - velocity component along y axis of liquid,
 *  and
 *  F11, F12, F21, F22 - components of elastic deformation gradient tensor.
 */

double alpha1(double U0);

double rho2(double U0, double U1);

double rho1(double U0, double U2);

double u11(double U1, double U2, double U3, double U5);

double u12(double U1, double U2, double U3, double U5);

double u21(double U1, double U2, double U4, double U6);

double u22(double U1, double U2, double U4, double U6);

double F11(double U1, double U2, double U7);

double F12(double U1, double U2, double U8);

double F21(double U1, double U2, double U9);

double F22(double U1, double U2, double U10);

/** Additional primitive variables:
 *  alpha2 - volume fraction of liquid phase,
 *  rho    - mass density of mixture,
 *  w1     - relative velocity (x) of porous medium relative of liquid,
 *  w2     - relative velocity (y) of porous medium relative of liquid,
 *  c1     - mass fraction of porous medium,
 *  c2     - mass fraction of liquid,
 *  u1     - mixture velocity (x),
 *  u2     - mixture velocity (y).
 */

double alpha2(double U0);

double rho(double U1, double U2);

double w1(double U3);

double w2(double U4);

double c1(double U1, double U2);

double c2(double U1, double U2);

double u1(double U1, double U2, double U5);

double u2(double U1, double U2, double U6);

/** Conservative variables U0,..., U10. **/

double U0(double alpha1);

double U1(double alpha1, double rho2);

double U2(double alpha1, double rho1);

double U3(double u11, double u12);

double U4(double u21, double u22);

double U5(double alpha1, double rho1, double rho2, double u11, double u12);

double U6(double alpha1, double rho1, double rho2, double u21, double u22);

double U7(double alpha1, double rho1, double rho2, double F11);

double U8(double alpha1, double rho1, double rho2, double F12);

double U9(double alpha1, double rho1, double rho2, double F21);

double U10(double alpha1, double rho1, double rho2, double F22);

} // namespace VariablesConversion

#endif // VARIABLES_CONVERSION_HPP

