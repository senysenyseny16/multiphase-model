//: Scheme.hpp

#ifndef SCHEME_HPP
#define SCHEME_HPP

#include "Mesh.hpp"

#include <cstddef>

/** @class Scheme
 *  @brief Interface of numerical scheme.
 */
class Scheme {
 public:

  /** Create abstract scheme. */
  Scheme(const Mesh& initialData);

  virtual ~Scheme() = 0;

  /** Run numerical method. */
  virtual void run() = 0;

  /** Show current solver status. */
  virtual void status() const;

  /** Show scheme info. */
  virtual void info() const;

  /** Set number of solver steps for data saving. */
  void setSaveInterval(size_t saveInterval);

  /** Set status output interval. */
  void setStatusInterval(size_t statusInterval);

  /** Calc new time step. */
  void calcTau(double maxCX, double maxCY);

  const Mesh& physicalVars() const;

 protected:

  /// Current timestep.
  double tau;

  /// Current physical time.
  double timeCur;

  /// Current iteration.
  size_t iteration;

  /// Every "saveInterval" iterations solver saves data.
  size_t saveInterval;

  /// Every "statusInterval" iteration solver prints status.
  size_t statusInterval;

  /// Solution.
  Mesh p0;
};

#endif // SCHEME_HPP

