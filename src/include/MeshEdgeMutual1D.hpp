//: MeshEdgeMutual1D.hpp

#ifndef MESH_EDGE_MUTUAL_1D_HPP
#define MESH_EDGE_MUTUAL_1D_HPP

#include "Mesh1D.hpp"

class MeshEdgeMutual1D {
 public:

  MeshEdgeMutual1D();

  virtual ~MeshEdgeMutual1D();

  /** Operator to access the elements of array (const).
   *
   *  @param i   - x-coordinate.
   *  @param j   - y-coordinate.
   */
  double  edgeX(size_t i, size_t j) const;
  double  edgeY(size_t i, size_t j) const;

  /** Operator to access the elements of array (by reference).
   *
   *  @param i   - x-coordinate.
   *  @param j   - y-coordinate.
   */
  double& edgeX(size_t i, size_t j);
  double& edgeY(size_t i, size_t j);

  /** Get value in backward x-direction. */
  double xB(size_t i, size_t j) const;

  /** Get value in forward  x-direction. */
  double xF(size_t i, size_t j) const;

  /** Get value in backward y-direction. */
  double yB(size_t i, size_t j) const;

  /** Get value in forward  y-direction. */
  double yF(size_t i, size_t j) const;

 protected:

  Mesh1D edgesX;
  Mesh1D edgesY;

};

#include "MeshEdgeMutual1D.inl" // Inlined bodies for xB, xF, yB, yF and data ref.

#endif // MESH_EDGE_MUTUAL_1D_HPP

