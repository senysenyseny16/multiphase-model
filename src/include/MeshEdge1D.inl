//: MeshEdge1D.inl

inline double MeshEdge1D::xB( const size_t i
                            , const size_t j) const
{
  return meshXB(i, j);
}

inline double MeshEdge1D::xF( const size_t i
                            , const size_t j) const
{
  return meshXF(i, j);
}

inline double MeshEdge1D::yB( const size_t i
                            , const size_t j) const
{
  return meshYB(i, j);
}

inline double MeshEdge1D::yF( const size_t i
                            , const size_t j) const
{
  return meshYF(i, j);
}

inline double& MeshEdge1D::xB( const size_t i
                             , const size_t j)
{
  return meshXB(i, j);
}

inline double& MeshEdge1D::xF( const size_t i
                             , const size_t j)
{
  return meshXF(i, j);
}

inline double& MeshEdge1D::yB( const size_t i
                             , const size_t j)
{
  return meshYB(i, j);
}

inline double& MeshEdge1D::yF( const size_t i
                             , const size_t j)
{
  return meshYF(i, j);
}

inline Mesh1D& MeshEdge1D::getMeshXB()
{
  return meshXB;
}

inline Mesh1D& MeshEdge1D::getMeshXF()
{
  return meshXF;
}

inline Mesh1D& MeshEdge1D::getMeshYB()
{
  return meshYB;
}

inline Mesh1D& MeshEdge1D::getMeshYF()
{
  return meshYF;
}

