//: LinearizedFluxMatrices.hpp

#ifndef LINEARIZED_FLUX_MATRICES_HPP
#define LINEARIZED_FLUX_MATRICES_HPP

#include "DefinitionsEigen.hpp"

//! Contains functions for linearization and characteristic decomposition.
namespace LinearizedFluxMatrices {

/* Linearized matrix of coefficients in x-direction. */
Eigen::Matrix11 Ax( double alpha1
                  , double rho
                  , double rho1
                  , double u11
                  , double u12
                  , double u21
                  , double u22
                  , double F11
                  , double F12
                  , double F21
                  , double F22);

/* Linearized matrix of coefficients in y-direction. */
Eigen::Matrix11 Ay( double alpha1
                  , double rho
                  , double rho1
                  , double u11
                  , double u12
                  , double u21
                  , double u22
                  , double F11
                  , double F12
                  , double F21
                  , double F22);

} // LinearizedFluxMatrices

#endif // LINEARIZED_FLUX_MATRICES_HPP

