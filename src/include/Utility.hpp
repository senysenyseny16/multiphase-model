//: Utility.hpp

#ifndef UTILITY_HPP
#define UTILITY_HPP

#include "Definitions.hpp"
#include "DefinitionsEigen.hpp"

#include <string>
#include <sstream>
#include <iomanip>

class Mesh;
class MeshEdge;
class MeshEdgeMutual1D;
class MeshEdge1D;

//! Contains various useful functions :).
namespace Utility {

/** Print used flux. See Definitions.hpp. */
void printFluxType();

/** Save mesh to file. */
void saveMeshToFile( const Mesh& mesh
                   , std::string filename);

/** Save averaged mesh to file.
 *
 *  @param stencil is averaging stencil.
 */
void saveAverageMeshToFile( const Mesh& mesh
                          , std::string filename
                          , size_t stencil);

/** Glue mesh boundaries with symmetry condition in x-direction. */
void glueMeshBoundariesX(Mesh& mesh);

/** Glue mesh boundaries with symmetry condition in y-direction. */
void glueMeshBoundariesY(Mesh& mesh);

/** Fill mesh boundaries by max value in x-direction. */
void fillMeshBoundariesX(Mesh& mesh);

/** Fill mesh boundaries by max value in y-direction. */
void fillMeshBoundariesY(Mesh& mesh);

/** Glue MeshEdge x-boundaries with symmetry condition. */
void glueMeshEdgeBoundariesX(MeshEdge& meshEdge);

/** Glue MeshEdge y-boundaries with symmetry condition. */
void glueMeshEdgeBoundariesY(MeshEdge& meshEdge);

void glueMeshBoundariesX(MeshEdge1D& mesh);

void glueMeshBoundariesY(MeshEdge1D& mesh);

/** Bound alpha1. */
void boundAlpha1(double& alpha1);

/** Bound alpha1. */
void boundAlpha1(Mesh& mesh);

/** Bound alpha1 for MeshEdge. */
void boundAlpha1(MeshEdge& meshEdge);

/** Check compatility of boundary conditions **/
bool checkCompatibilityBC();

std::vector<std::pair<double, Eigen::VectorRow11> >
sortVectorsByValues(std::vector<std::pair<double, Eigen::VectorRow11> > v);

std::pair<double, double> findMinMaxAlpha1(const Mesh& physicalVars);

void printMinMaxAlpha1(const Mesh& physicalVars);

/** Output value with precision. */
template <typename T>
std::string toStringWithPrecision(const T value, const int n = 16)
{
  std::ostringstream out;
  out << std::scientific << std::setprecision(n) << value;
  return out.str();
}

/** Sign function. */
template <typename T> int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

void dumpMesh(const Mesh& mesh);

} // namespace Utility

#endif // UTILITY_HPP

