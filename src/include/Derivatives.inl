//: Derivatives.inl

namespace Derivatives {

inline double dXS( const double vXB
                 , const double vXF)
{
  return ((vXF - vXB) / (2. * SchemePars::hx));
}

inline double dYS( const double vYB
                 , const double vYF)
{
  return ((vYF - vYB) / (2. * SchemePars::hy));
}

inline double dXB( const double vXB
                 , const double vXC)
{
  return ((vXC - vXB) / SchemePars::hx);
}

inline double dXF( const double vXC
                 , const double vXF)
{
  return ((vXF - vXC) / SchemePars::hx);
}

inline double dYB( const double vYB
                 , const double vYC)
{
  return ((vYC - vYB) / SchemePars::hy);
}

inline double dYF( const double vYC
                 , const double vYF)
{
  return ((vYF - vYC) / SchemePars::hy);
}

} // namespace Derivatives

