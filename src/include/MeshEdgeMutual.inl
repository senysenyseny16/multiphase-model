//: MeshEdgeMutual.inl

inline double MeshEdgeMutual::xB( const size_t var
                                , const size_t i
                                , const size_t j) const
{
  return edgesX(var, i, j);
}

inline double MeshEdgeMutual::xF( const size_t var
                                , const size_t i
                                , const size_t j) const
{
  return edgesX(var, i + 1, j);
}

inline double MeshEdgeMutual::yB( const size_t var
                                , const size_t i
                                , const size_t j) const
{
  return edgesY(var, i, j);
}

inline double MeshEdgeMutual::yF( const size_t var
                                , const size_t i
                                , const size_t j) const
{
  return edgesY(var, i, j + 1);
}

inline double MeshEdgeMutual::edgeX( const size_t var
                                   , const size_t i
                                   , const size_t j) const
{
  return edgesX(var, i, j);
}

inline double MeshEdgeMutual::edgeY( const size_t var
                                   , const size_t i
                                   , const size_t j) const
{
  return edgesY(var, i, j);
}

inline double& MeshEdgeMutual::edgeX( const size_t var
                                    , const size_t i
                                    , const size_t j)
{
  return edgesX(var, i, j);
}

inline double& MeshEdgeMutual::edgeY( const size_t var
                                    , const size_t i
                                    , const size_t j)
{
  return edgesY(var, i, j);
}

