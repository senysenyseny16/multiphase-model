//: MeshEdgeMutual1D.inl

inline double MeshEdgeMutual1D::xB( const size_t i
                                  , const size_t j) const
{
  return edgesX(i, j);
}

inline double MeshEdgeMutual1D::xF( const size_t i
                                  , const size_t j) const
{
  return edgesX(i + 1, j);
}

inline double MeshEdgeMutual1D::yB( const size_t i
                                  , const size_t j) const
{
  return edgesY(i, j);
}

inline double MeshEdgeMutual1D::yF( const size_t i
                                  , const size_t j) const
{
  return edgesY(i, j + 1);
}

inline double MeshEdgeMutual1D::edgeX( const size_t i
                                     , const size_t j) const
{
  return edgesX(i, j);
}

inline double MeshEdgeMutual1D::edgeY( const size_t i
                                     , const size_t j) const
{
  return edgesY(i, j);
}

inline double& MeshEdgeMutual1D::edgeX( const size_t i
                                      , const size_t j)
{
  return edgesX(i, j);
}

inline double& MeshEdgeMutual1D::edgeY( const size_t i
                                      , const size_t j)
{
  return edgesY(i, j);
}

