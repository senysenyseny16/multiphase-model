//: BoundaryConditionYBack.hpp

#ifndef BC_Y_BACK_HPP
#define BC_Y_BACK_HPP

namespace BC {

/* Calc hybrid (nonslip for skeleton and slip for liquid) boundary condition
 * on the y-back boundary.
 *
 * @param alpha1, rho, rho1, u, F - cell average.
 * @param xBalpha1, ... , xBF - primitive variables
 * on the y-back edge of the y-back cell (boundary condition).
 */
void slipHybridYBack( double alpha1
                    , double rho
                    , double rho1
                    , double u11
                    , double u12
                    , double u21
                    , double u22
                    , double F11
                    , double F12
                    , double F21
                    , double F22

                    , double& yF0
                    , double& yF1
                    , double& yF2
                    , double& yF3
                    , double& yF4
                    , double& yF5
                    , double& yF6
                    , double& yF7
                    , double& yF8
                    , double& yF9
                    , double& yF10);

} // namespace BC

#endif // BC_Y_BACK_HPP

