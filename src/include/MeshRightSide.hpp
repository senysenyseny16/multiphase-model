//: MeshRightSide.hpp

#ifndef MESH_RIGHT_SIDE_HPP
#define MESH_RIGHT_SIDE_HPP

#include "Mesh.hpp"

/** @class MeshRightSide
 *  @brief Right side of equations system.
 */
class MeshRightSide: public Mesh {
 public:

  MeshRightSide();

  void calcRightSide(const Mesh& physicalVars);
};

#endif // MESH_RIGHT_SIDE_HPP

