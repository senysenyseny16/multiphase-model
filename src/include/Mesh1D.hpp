//: Mesh1D.hpp

#ifndef MESH_1D_HPP
#define MESH_1D_HPP

#include <vector>

class Mesh1D {
 public:

  /** Ctor **/
  Mesh1D();

  /** Mesh copy. **/
  Mesh1D(const Mesh1D&);
  Mesh1D& operator=(const Mesh1D&);

  virtual ~Mesh1D();

  /** Operator to access the elements of array (const).
   *  @param i   - x-coordinate.
   *  @param j   - y-coordinate.
   */
  double  operator()(size_t i, size_t j) const;

  /** Operator to access the elements of array (by reference).
   *  @param i   - x-coordinate.
   *  @param j   - y-coordinate.
   */
  double& operator()(size_t i, size_t j);

 protected:

  /// Emulate 2d array.
  std::vector<double> data;
};

#include "Mesh1D.inl" // Inlined bodies for both operators ().

#endif // MESH_1D_HPP

