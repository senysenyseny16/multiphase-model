//: Derivatives.hpp

#ifndef DERIVATIVES_HPP
#define DERIVATIVES_HPP

#include "Definitions.hpp"

namespace Derivatives {

double dXS(double vXB, double vXF);

double dYS(double vYB, double vYF);

double dXB(double vXB, double vXC);

double dXF(double vXC, double vXF);

double dYB(double vYB, double vYC);

double dYF(double vYC, double vYF);

} // namespace Derivatives

#include "Derivatives.inl" // Inlined bodies.

#endif // DERIVATIVES_HPP
