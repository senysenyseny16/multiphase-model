//: MeshEdge1D.hpp

#ifndef MESH_EDGE_1D_HPP
#define MESH_EDGE_1D_HPP

#include "Mesh1D.hpp"

/** @class MeshEdge1D
 *  @brief MeshEdge1D contains four variables on the cells edges.
 */
class MeshEdge1D {
 public:

  /** Allocate memory for variables. */
  MeshEdge1D();

  virtual ~MeshEdge1D();

  /** Get value in backward x-direction. */
  double  xB(size_t i, size_t j) const;

  /** Get value in forward  x-direction. */
  double  xF(size_t i, size_t j) const;

  /** Get value in backward y-direction. */
  double  yB(size_t i, size_t j) const;

  /** Get value in forward  y-direction. */
  double  yF(size_t i, size_t j) const;


  /** Get ref in backward x-direction. */
  double& xB(size_t i, size_t j);

  /** Get ref in forward  x-direction. */
  double& xF(size_t i, size_t j);

  /** Get ref in backward y-direction. */
  double& yB(size_t i, size_t j);

  /** Get ref in forward  y-direction. */
  double& yF(size_t i, size_t j);

  Mesh1D& getMeshXB();
  Mesh1D& getMeshXF();
  Mesh1D& getMeshYB();
  Mesh1D& getMeshYF();

 protected:

  /// Storage for values in backward (B) x-direction: x - dx / 2.
  Mesh1D meshXB;
  /// Storage for values in forward  (F) x-direction: x + dx / 2.
  Mesh1D meshXF;
  /// Storage for values in backward (B) y-direction: y - dy / 2.
  Mesh1D meshYB;
  /// Storage for values in forward  (F) y-direction: y + dy / 2.
  Mesh1D meshYF;
};

#include "MeshEdge1D.inl" // Inlined bodies for xB, xF, yB, yF and data ref.

#endif // MESH_EDGE_1D_HPP

