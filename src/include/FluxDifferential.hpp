//: FluxDifferential.hpp

#ifndef FLUX_DIFFERENTIAL_HPP
#define FLUX_DIFFERENTIAL_HPP

#include <cstddef>
#include <cassert>

class Mesh;

/** Differential flux.
 *
 *  Additional variables:
 *  u1, u2,
 *  e1, e2,
 *  p1, p2,
 *  alpha2,
 *  sigma{ij}.
 */
namespace Flux {

/** Calculate differential flux in point (local) in x-direction.
 *
 *  @param meshPhysicalRec is mesh with reconstructed physical variables.
 *  @param i, j are coordiantes of point.
 *  @param meshFlux is destination of calculated flux.
 */
void calcFluxDXLocal( const Mesh& meshPhysicalRec
                    , size_t i
                    , size_t j
                    , Mesh& meshFlux);

/** Calculate differential flux in point (local) in y-direction.
 *
 *  @param meshPhysicalRec is mesh with reconstructed physical variables.
 *  @param i, j are coordiantes of point.
 *  @param meshFlux is destination of calculated flux.
 */
void calcFluxDYLocal( const Mesh& meshPhysicalRec
                    , size_t i
                    , size_t j
                    , Mesh& meshFlux);

/// Flux for conservative form of volume fraction eq.
double xF0( double alpha1
          , double u1);

double xF1( double alpha1
          , double rho2
          , double u12);

double xF2( double alpha1
          , double rho1
          , double u11);

double xF3( double rho1
          , double rho2
          , double u11
          , double u12
          , double u21
          , double u22
          , double e1
          , double e2
          , double p1
          , double p2);

double xF5( double alpha1
          , double alpha2
          , double rho1
          , double rho2
          , double u11
          , double u12
          , double p1
          , double p2
          , double sigma11);

double xF6( double alpha1
          , double alpha2
          , double rho1
          , double rho2
          , double u11
          , double u12
          , double u21
          , double u22
          , double sigma21);

double xF7( double rho
          , double F11
          , double u1);

double xF8( double rho
          , double F12
          , double u1);

double xF9( double rho
          , double F21
          , double u1);

double xF10( double rho
           , double F22
           , double u1);

/// Flux for conservative form of volume fraction eq.
double yF0( double alpha1
          , double u2);

double yF1( double alpha1
          , double rho2
          , double u22);

double yF2( double alpha1
          , double rho1
          , double u21);

double yF5( double alpha1
          , double alpha2
          , double rho1
          , double rho2
          , double u11
          , double u12
          , double u21
          , double u22
          , double sigma12);

double yF6( double alpha1
          , double alpha2
          , double rho1
          , double rho2
          , double u21
          , double u22
          , double p1
          , double p2
          , double sigma22);

double yF7( double rho
          , double F11
          , double u2);

double yF8( double rho
          , double F12
          , double u2);

double yF9( double rho
          , double F21
          , double u2);

double yF10( double rho
           , double F22
           , double u2);

} // namespace Flux

#include "FluxDifferential.inl" // Inlined bodies for flux.

#endif // FLUX_DIFFERENTIAL_HPP

