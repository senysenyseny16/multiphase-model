//: MeshFluxNumerical.hpp

#ifndef MESH_FLUX_NUM_HPP
#define MESH_FLUX_NUM_HPP

#include "MeshEdge.hpp"

class MeshFluxDifferential;
class MeshMaxC;

namespace FluxNumerical {

/** @class MeshFluxNumerical
 *  @brief Mesh for numerical flux.
 *
 *  Edit Definitions.hpp: SchemePars::fluxType to change type of used flux.
 */
class MeshFluxNumerical {
 public:

  MeshFluxNumerical();

  virtual ~MeshFluxNumerical() = 0;

  /** Calculate numerical flux on the edges of cells.
   *
   *  @param fluxDifferential is differential flux.
   *  @param recConservative is reconstructed conservative variables.
   *  @param recPrimitive is reconstructed primitive variables.
   *  @param primitive is mesh with primitive variables.
   */
  virtual void calcFlux( const MeshFluxDifferential& fluxDifferential
                       , const MeshEdge&             recConservative
                       , const MeshEdge&             recPrimitive
                       , const Mesh&                 primitive
                       ) = 0;

  /// Conservative part of numerical flux.
  MeshEdge c;

  /// Non-conservative part of numerical flux.
  MeshEdge nC;
};


class HLLC: public MeshFluxNumerical {
 public:

  HLLC();

  void calcFlux( const MeshFluxDifferential& fluxDifferential
               , const MeshEdge&             recConservative
               , const MeshEdge&             recPrimitive
               , const Mesh&                 primitive
               );

 protected:

  void HLLCX( const Mesh& fluxDifferentialB
            , const Mesh& fluxDifferentialF
            , const Mesh& recConservativeB
            , const Mesh& recConservativeF
            , const Mesh& recPrimitiveB
            , const Mesh& recPrimitiveF
            , const Mesh& primitive
            , size_t iB
            , size_t iF
            , size_t j
            , Mesh& fluxNumericalConservativeB
            , Mesh& fluxNumericalConservativeF
            , Mesh& fluxNumericalNonConservativeB
            , Mesh& fluxNumericalNonConservativeF);

  void HLLCY( const Mesh& fluxDifferentialB
            , const Mesh& fluxDifferentialF
            , const Mesh& recConservativeB
            , const Mesh& recConservativeF
            , const Mesh& recPrimitiveB
            , const Mesh& recPrimitiveF
            , const Mesh& primitive
            , size_t i
            , size_t jB
            , size_t jF
            , Mesh& fluxNumericalConservativeB
            , Mesh& fluxNumericalConservativeBF
            , Mesh& fluxNumericalNonConservativeB
            , Mesh& fluxNumericalNonConservativeF);
};

/** createMeshFluxNumerical is factory for concrete numerical flux.
 *
 *  Edit Definitions.hpp: SchemePars::fluxType to change numerical flux.
 */
MeshFluxNumerical* createMeshFluxNumerical();

} // namespace FluxNumerical

#endif // MESH_FLUX_NUM_HPP

