//: InitialData.hpp

#ifndef INITIAL_DATA_HPP
#define INITIAL_DATA_HPP

#include "Mesh.hpp"

//! Initial data for computation.
namespace InitialData {

/// Initial data with equlibrium state.
Mesh eqState();

/// Initial data with two different state.
Mesh squareTwoStates();

/// Initial data with two different equlibrium states.
Mesh twoEqStatesX();

/// Initial data with two different equlibrium states.
Mesh twoEqStatesY();

// Symmetric (1D in x-coord) initial data with discontinuous in alpha1.
Mesh twoAlpha1X();

/// Symmetric (1D in y-coord) initial data with discontinuous in alpha1.
Mesh twoAlpha1Y();

/// Smooth periodical by x initial data.
Mesh periodicalSmoothX();

/// Smooth periodical initial data.
Mesh periodicalSmoothXY();

/// Rhombus initial data with discontinuous in alpha1.
Mesh rhombusAlpha1();

/// Circle-symmetry data with discontinuous in alpha1.
Mesh circleAlpha1();

/// Circle-symmetry smooth data with discontinuous in alpha1.
Mesh circleSmoothAlpha1();

/// Circle-symmetry data with discontinuous in pressure.
Mesh circlePressure();

/// Smooth elliptical data with discontinuous in alpha1.
Mesh ellipseSmoothAlpha1();

/// Model of vessel.
Mesh vessel0();

} // namespace InitialData

#endif // INITIAL_DATA_HPP

