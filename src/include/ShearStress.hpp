//: ShearStress.hpp

#ifndef SHEAR_STRESS_HPP
#define SHEAR_STRESS_HPP

/** ShearStress provide functions to calc
 *  shear stresses sigma_{ij}.
 *
 *  Note! From symmetry follows sigma12 = sigma21.
 */
namespace ShearStress {

/// Shear stress in x-direction.
double sigma11( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

/// Shear stress in xy-direction.
double sigma12( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

/// From symmetry (sigma12 = sigma21).
double sigma21( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

/// Shear stress in y-direction.
double sigma22( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

} // namespace ShearStress

#endif // SHEAR_STRESS_HPP

