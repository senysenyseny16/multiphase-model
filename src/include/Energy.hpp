//: Energy.hpp

#ifndef ENERGY_HPP
#define ENERGY_HPP

/** Energy (namespace) provide functions to calculate
 *  energy characteristics of mixture:
 *  - internal energy of porous medium (e1)
 *  - internal energy of liquid (e2)
 *  - porous medium pressure (p1)
 *  - liquid pressure (p2)
 *  and so on.
 */
namespace Energy {

/// Internal energy of abstract liquid part.
double liquid( double C
             , double gamma
             , double rho
             , double rho0
             , double p0);

/// Internal energy of abstract solid part.
double solid( double mu, double rho0
            , double F11, double F12, double F21, double F22);

/// Internal energy of abstract porous medium.
double porous( double C
             , double gamma
             , double rho
             , double rho0
             , double p0
             , double mu
             , double F11, double F12, double F21, double F22);

/// Pressure (for both phases).
double pressure( double C
               , double gamma
               , double rho
               , double rho0
               , double p0);

/// Internal energy of porous medium.
double e1( double rho1
         , double F11, double F12, double F21, double F22);

/// Internal energy of liquid.
double e2(double rho2);

/// Pressure in porous medium.
double p1(double rho1);

/// Pressure in liquid.
double p2(double rho2);

/// Derivative of pressure (by density).
double dp( double C
         , double gamma
         , double rho
         , double rho0);

/// Pressure derivative for porous medium.
double dp1(double rho1);

/// Pressure derivative for liquid.
double dp2(double rho2);

/// Calc density by known pressure.
double rho( double C
          , double gamma
          , double rho0
          , double p0
          , double p);

/// Density of porous medium.
double rho1(double p1);

/// Density of liquid.
double rho2(double p2);

} // namespace Energy

#endif // ENERGY_HPP

