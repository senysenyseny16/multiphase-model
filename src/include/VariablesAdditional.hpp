//: VariablesAdditional.hpp

#ifndef VARIABLES_ADDITIONAL_HPP
#define VARIABLES_ADDITIONAL_HPP

#include <cassert>

//! Functions for calculation additional variables.
namespace VariablesAdditional {

double alpha2(double alpha1);

double rho( double alpha1
          , double rho1
          , double rho2);

double c1( double alpha1
         , double rho1
         , double rho2);

double c2( double alpha1
         , double rho1
         , double rho2);

double u1( double c1
         , double c2
         , double u11
         , double u12);

double u2( double c1
         , double c2
         , double u21
         , double u22);

double w1( double u11
         , double u12);

double w2( double u21
         , double u22);

} // namespace VariablesAdditional

#include "VariablesAdditional.inl" // Inlined bodies.

#endif // VARIABLES_ADDITIONAL_HPP

