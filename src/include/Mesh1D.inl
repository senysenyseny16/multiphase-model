//: Mesh1D.inl

#include "Definitions.hpp"

#include <cassert>

inline double Mesh1D::operator()(const size_t i, const size_t j) const
{
  assert(i < Geometry::meshSizeX);
  assert(j < Geometry::meshSizeY);

  // Row-major order.
  return data[   j *  Geometry::meshSizeX // shift by rows.
               + i                        // shift by cols.
             ];
}

inline double& Mesh1D::operator()(const size_t i, const size_t j)
{
  assert(i < Geometry::meshSizeX);
  assert(j < Geometry::meshSizeY);

  // Row-major order.
  return data[   j * Geometry::meshSizeX // shift by rows.
               + i                       // shift by cols.
             ];
}
