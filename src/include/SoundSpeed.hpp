//: SoundSpeed.hpp

#ifndef SOUND_SPEED_HPP
#define SOUND_SPEED_HPP

namespace SoundSpeed {

double C1(double rho1);

double C2(double rho2);

double Cv(double rho1);

double maxC( double rho1
           , double rho2
           , double c1
           , double c2
           , double u);

double longitudinalVelocity2Elastic(double rho1);

double longitudinalVelocity2Liquid(double rho2);

double longitudinalVelocityMixture( double rho1
                                  , double rho2
                                  , double c1);

} // namespace SoundSpeed

#endif // SOUND_SPEED_HPP

