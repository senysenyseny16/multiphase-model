//: MeshMaxC.hpp

#ifndef MESH_MAX_C_HPP
#define MESH_MAX_C_HPP

#include "MeshEdgeMutual1D.hpp"

class MeshEdge;

/** @class MeshMaxC
 *  @brief Storage for calculated local and global max characteristics speed.
 */
class MeshMaxC: public MeshEdgeMutual1D {
 public:

  MeshMaxC();

  /** Calc max characteristics speed. */
  void calcMaxC(const MeshEdge& recPrimitive);

  /** Global max charactersitic speed in x-direction. */
  double maxCX() const;

  /** Global max charactersitic speed in y-direction. */
  double maxCY() const;

  double minC() const;

 protected:

  /// Global max charactersitic speed in x-direction.
  double maxCGlobalX;

  /// Global max charactersitic speed in x-direction.
  double maxCGlobalY;

  /// Global min characteristic speed.
  double minCGlobal;
};

#endif // MESH_MAX_C_HPP

