//: FluxDifferential.inl

namespace Flux {

inline double xF0( const double alpha1
                 , const double u1)
{
  assert(alpha1 > 0. and alpha1 < 1.);

  return (alpha1 * u1);
}

inline double xF1( const double alpha1
                 , const double rho2
                 , const double u12)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho2 > 0.);

  return ((rho2 * u12) * (1. - alpha1));
}

inline double xF2( const double alpha1
                 , const double rho1
                 , const double u11)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);

  return ((rho1 * u11) * alpha1);
}

inline double xF3( const double rho1
                 , const double rho2
                 , const double u11
                 , const double u12
                 , const double u21
                 , const double u22
                 , const double e1
                 , const double e2
                 , const double p1
                 , const double p2)
{
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  return ( ( u11 * u11
           + u21 * u21
           - u12 * u12
           - u22 * u22
           ) / 2.
         + (e1 + p1 / rho1)
         - (e2 + p2 / rho2)
         );
}

inline double xF5( const double alpha1
                 , const double alpha2
                 , const double rho1
                 , const double rho2
                 , const double u11
                 , const double u12
                 , const double p1
                 , const double p2
                 , const double sigma11)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(alpha2 > 0. and alpha2 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  return (   (alpha1 * rho1) * (u11 * u11)
           + (alpha2 * rho2) * (u12 * u12)
           + (alpha1 * p1 + alpha2 * p2)
           - (alpha1 * sigma11)
         );
}

inline double xF6( const double alpha1
                 , const double alpha2
                 , const double rho1
                 , const double rho2
                 , const double u11
                 , const double u12
                 , const double u21
                 , const double u22
                 , const double sigma21)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(alpha2 > 0. and alpha2 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  return (   (alpha1 * rho1) * (u21 * u11)
           + (alpha2 * rho2) * (u22 * u12)
           - (alpha1 * sigma21)
         );
}

inline double xF7( const double rho
                 , const double F11
                 , const double u1)
{
  assert(rho > 0.);

  return (rho * F11) * u1;
}

inline double xF8( const double rho
                 , const double F12
                 , const double u1)
{
  assert(rho > 0.);

  return (rho * F12) * u1;
}

inline double xF9( const double rho
                 , const double F21
                 , const double u1)
{
  assert(rho > 0.);

  return (rho * F21) * u1;
}

inline double xF10( const double rho
                  , const double F22
                  , const double u1)
{
  assert(rho > 0.);

  return (rho * F22) * u1;
}

inline double yF0( const double alpha1
                 , const double u2)
{
  assert(alpha1 > 0. and alpha1 < 1.);

  return (alpha1 * u2);
}

inline double yF1( const double alpha1
                 , const double rho2
                 , const double u22)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho2 > 0.);

  return ((rho2  * u22) * (1. - alpha1));
}

inline double yF2( const double alpha1
                 , const double rho1
                 , const double u21)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);

  return ((rho1 * u21) * alpha1);
}

inline double yF5( const double alpha1
                 , const double alpha2
                 , const double rho1
                 , const double rho2
                 , const double u11
                 , const double u12
                 , const double u21
                 , const double u22
                 , const double sigma12)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(alpha2 > 0. and alpha2 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  return (   (alpha1 * rho1) * (u11 * u21)
           + (alpha2 * rho2) * (u12 * u22)
           - (alpha1 * sigma12)
         );
}

inline double yF6( const double alpha1
                 , const double alpha2
                 , const double rho1
                 , const double rho2
                 , const double u21
                 , const double u22
                 , const double p1
                 , const double p2
                 , const double sigma22)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(alpha2 > 0. and alpha2 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  return (   (alpha1 * rho1) * (u21 * u21)
           + (alpha2 * rho2) * (u22 * u22)
           + (alpha1 * p1 + alpha2 * p2)
           - (alpha1 * sigma22)
         );
}

inline double yF7( const double rho
                 , const double F11
                 , const double u2)
{
  assert(rho > 0.);

  return (rho * F11) * u2;
}

inline double yF8( const double rho
                 , const double F12
                 , const double u2)
{
  assert(rho > 0.);

  return (rho * F12) * u2;
}

inline double yF9( const double rho
                 , const double F21
                 , const double u2)
{
  assert(rho > 0.);

  return (rho * F21) * u2;
}

inline double yF10( const double rho
                  , const double F22
                  , const double u2)
{
  assert(rho > 0.);

  return (rho * F22) * u2;
}

} // namespace Flux

