//: DefinitionsEigen.hpp

#ifndef DEFINITIONS_EIGEN_HPP
#define DEFINITIONS_EIGEN_HPP

#include <Eigen/Dense>

namespace Eigen {

/* Useful definitions for Eigen library. */

typedef Eigen::Matrix<double, 11, 1> VectorCol11;
typedef Eigen::Matrix<double, 1, 11> VectorRow11;

typedef Eigen::Matrix<double, 11, 11> Matrix11;

typedef Eigen::EigenSolver<Matrix11> EigenSolver11;

typedef Eigen::Matrix<double, 3, 1> VectorCol3;
typedef Eigen::Matrix<double, 1, 3> VectorRow3;

typedef Eigen::Matrix<double, 3, 3> Matrix3;

typedef Eigen::EigenSolver<Matrix3> EigenSolver3;

} // namespace Eigen

#endif // DEFINITIONS_EIGEN_HPP

