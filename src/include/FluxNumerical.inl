//: FluxNumerical.inl

namespace Flux {

inline double LF( const double cB
                , const double cF
                , const double fB
                , const double fF
                , const double k)
{
  return ((fB + fF) - (k / 2.) * (cF - cB)) / 2.;
}

inline double LFAlpha1( double alpha1B
                      , double alpha1F
                      , double u
                      , double k)
{
  return (u * (alpha1B + alpha1F) - (k / 2.) * (alpha1F - alpha1B)) / 2.;
}

inline double LLF( const double cB
                 , const double cF
                 , const double fB
                 , const double fF
                 , const double maxC)
{
  return ((fB + fF) - (maxC) * (cF - cB)) / 2.;
}

inline double LLFAlpha1( const double alpha1B
                       , const double alpha1F
                       , const double u
                       , const double maxC)
{
  return (u * (alpha1B + alpha1F) - maxC * (alpha1F - alpha1B)) / 2.;
}

inline double ULW( const double cB
                 , const double cF
                 , const double fB
                 , const double fF
                 , const double k)
{
  return ((cB + cF) / 2. - k * (fF - fB));
}

inline double LWAlpha1( const double alpha1B
                      , const double alpha1F
                      , const double u
                      , const double k)
{
  return (u * ((alpha1F + alpha1B) / 2. - (u * k) * (alpha1F - alpha1B)));
}

}

