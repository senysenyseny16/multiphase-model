//: VariablesAdditional.inl

namespace VariablesAdditional {

inline double alpha2(const double alpha1)
{
  const double alpha2 = 1. - alpha1;

  assert(alpha2 > 0. and alpha2 < 1.);

  return alpha2;
}

inline double rho( const double alpha1
                 , const double rho1
                 , const double rho2)
{
  const double rho = alpha1 * rho1 + (1. - alpha1) * rho2;

  assert(rho > 0.);

  return rho;
}

inline double c1( const double alpha1
                , const double rho1
                , const double rho2)
{
  const double c1 = (alpha1 * rho1) / rho(alpha1, rho1, rho2);

  assert(c1 > 0. and c1 < 1.);

  return c1;
}

inline double c2( const double alpha1
                , const double rho1
                , const double rho2)
{
  const double c2 = ((1. - alpha1) * rho2) / rho(alpha1, rho1, rho2);

  assert(c2 > 0. and c2 < 1.);

  return c2;
}

inline double u1( const double c1
                , const double c2
                , const double u11
                , const double u12)
{
  return (c1 * u11 + c2 * u12);
}

inline double u2( const double c1
                , const double c2
                , const double u21
                , const double u22)
{
  return (c1 * u21 + c2 * u22);
}

inline double w1( const double u11
                , const double u12)
{
  return (u11 - u12);
}

inline double w2( const double u21
                , const double u22)
{
  return (u21 - u22);
}

} // namespace VariablesAdditional

