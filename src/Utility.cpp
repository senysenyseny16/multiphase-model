//: Utility.cpp

#include "Utility.hpp"
#include "Mesh.hpp"
#include "MeshEdge.hpp"
#include "MeshEdgeMutual1D.hpp"
#include "Mesh1D.hpp"
#include "MeshEdge1D.hpp"
#include "VariablesConversion.hpp"
#include "VariablesAdditional.hpp"
#include "SoundSpeed.hpp"

#include <iostream>
#include <fstream>

namespace Utility {

void printFluxType()
{
  std::cout << "Flux type: ";

  switch(SchemePars::fluxType) {

  case SchemePars::FluxType::HLLC:

    std::cout << "HLLC";

  break;
  }

  std::cout << std::endl;
}

void saveMeshToFile( const Mesh& mesh
                   , const std::string filename)
{
  using namespace SchemePars;

  std::ofstream meshFile(filename);

  const std::string separator(" ");
  std::string dataRow;
  dataRow.reserve(1000);

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
      // First two columns are x and y coordinates.
      dataRow.clear();
      dataRow.append(   toStringWithPrecision(i * hx + hx / 2.)
                      + separator
                      + toStringWithPrecision(j * hy + hy / 2.)
                      + separator
                    );

      // Add physical variables.
      for (size_t var = 0; var < Physics::varCount; ++var) {
        dataRow.append(toStringWithPrecision(mesh(var, i, j)) + separator);
      }

      // Push row to file.
      meshFile << dataRow << std::endl;
    }
  }
  meshFile.close();
}

void saveAverageMeshToFile( const Mesh& mesh
                          , const std::string filename
                          , const size_t stencil)
{
  assert(Geometry::cellCountX % stencil == 0);
  assert(Geometry::cellCountY % stencil == 0);

  using namespace SchemePars;

  std::ofstream meshFile(filename);

  const std::string separator(" ");

  const size_t averageMultiplier = stencil * stencil;

  for (size_t var = 0; var < Physics::varCount; ++var) {
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; j += stencil) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; i += stencil) {

        double sum = 0.;

        // Averaging.
        for (size_t jj = j; jj < j + stencil; ++jj) {
          for (size_t ii = i; ii < i + stencil; ++ii) {
            sum += mesh(var, ii, jj);
          }
        }
        sum = sum / averageMultiplier;

        // Write.
        meshFile << toStringWithPrecision(sum) << separator;
      }

      meshFile << std::endl;
    }
    meshFile << std::endl;
  }
  meshFile.close();
}

void glueMeshBoundariesX(Mesh& mesh)
{
  using namespace Geometry;

  // Glue boundaries in x-direction.
  for (size_t j = 0; j < meshSizeY; ++j) {
    for (size_t i = 0; i < cellCountGhost; ++i) {
      for (size_t v = 0; v < Physics::varCount; ++v) {

        mesh(v, i, j) = mesh(v, cellCountX + i, j);
        mesh(v, cellCountX + cellCountGhost + i, j) = mesh(v, cellCountGhost + i, j);

      }
    }
  }
}

void glueMeshBoundariesY(Mesh& mesh)
{
  using namespace Geometry;

  // Glue boundaries in y-direction.
  for (size_t j = 0; j < cellCountGhost; ++j) {
    for (size_t i = 0; i < meshSizeX; ++i) {
      for (size_t v = 0; v < Physics::varCount; ++v) {

        mesh(v, i, j) = mesh(v, i, cellCountY + j);
        mesh(v, i, cellCountY + cellCountGhost + j) = mesh(v, i, cellCountGhost + j);

      }
    }
  }
}

void glueMeshBoundariesX(Mesh1D& mesh)
{
  using namespace Geometry;

  // Glue boundaries in x-direction.
  for (size_t j = 0; j < meshSizeY; ++j) {
    for (size_t i = 0; i < cellCountGhost; ++i) {

        mesh(i, j) = mesh(cellCountX + i, j);
        mesh(cellCountX + cellCountGhost + i, j) = mesh(cellCountGhost + i, j);

    }
  }
}

void glueMeshBoundariesY(Mesh1D& mesh)
{

  using namespace Geometry;

  // Glue boundaries in y-direction.
  for (size_t j = 0; j < cellCountGhost; ++j) {
    for (size_t i = 0; i < meshSizeX; ++i) {

        mesh(i, j) = mesh(i, cellCountY + j);
        mesh(i, cellCountY + cellCountGhost + j) = mesh(i, cellCountGhost + j);

    }
  }
}

void glueMeshBoundariesX(MeshEdge1D& mesh)
{
  glueMeshBoundariesX(mesh.getMeshXB());
  glueMeshBoundariesX(mesh.getMeshXF());
}

void glueMeshBoundariesY(MeshEdge1D& mesh)
{
  glueMeshBoundariesY(mesh.getMeshYB());
  glueMeshBoundariesY(mesh.getMeshYF());
}

void fillMeshBoundariesX(Mesh& mesh)
{
  using namespace Geometry;

  // Fill boundaries in x-direction.
  for (size_t j = 0; j < meshSizeY; ++j) {
    for (size_t i = 0; i < cellCountGhost; ++i) {
      for (size_t v = 0; v < Physics::varCount; ++v) {

        mesh(v, i, j) = SchemePars::ghostCellsFillValue;
        mesh(v, cellCountX + cellCountGhost + i, j) = SchemePars::ghostCellsFillValue;

      }
    }
  }
}

void fillMeshBoundariesY(Mesh& mesh)
{
  using namespace Geometry;

  // Fill boundaries in y-direction.
  for (size_t j = 0; j < cellCountGhost; ++j) {
    for (size_t i = 0; i < meshSizeX; ++i) {
      for (size_t v = 0; v < Physics::varCount; ++v) {

        mesh(v, i, j) = SchemePars::ghostCellsFillValue;
        mesh(v, i, cellCountY + cellCountGhost + j) = SchemePars::ghostCellsFillValue;

      }
    }
  }
}

void glueMeshEdgeBoundariesX(MeshEdge& meshEdge)
{
  glueMeshBoundariesX(meshEdge.dataXB());
  glueMeshBoundariesX(meshEdge.dataXF());
  glueMeshBoundariesX(meshEdge.dataYB());
  glueMeshBoundariesX(meshEdge.dataYF());
}

void glueMeshEdgeBoundariesY(MeshEdge& meshEdge)
{
  glueMeshBoundariesY(meshEdge.dataXB());
  glueMeshBoundariesY(meshEdge.dataXF());
  glueMeshBoundariesY(meshEdge.dataYB());
  glueMeshBoundariesY(meshEdge.dataYF());
}

void boundAlpha1(double& alpha1)
{
  using namespace SchemePars;

  if (alpha1 < alpha1Eps     ) alpha1 = alpha1Eps;
  if (alpha1 > 1. - alpha1Eps) alpha1 = 1. - alpha1Eps;
}

void boundAlpha1(Mesh& mesh)
{
  using namespace SchemePars;

  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      if (mesh(0, i, j) < alpha1Eps     ) mesh(0, i, j) = alpha1Eps;
      if (mesh(0, i, j) > 1. - alpha1Eps) mesh(0, i, j) = 1. - alpha1Eps;

    }
  }
}

void boundAlpha1(MeshEdge& meshEdge)
{
  boundAlpha1(meshEdge.dataXB());
  boundAlpha1(meshEdge.dataXF());
  boundAlpha1(meshEdge.dataYB());
  boundAlpha1(meshEdge.dataYF());
}

bool checkCompatibilityBC()
{
  bool isCompatible = true;

  if (     BC::boundaryCondYBack  == BC::BoundaryCondYBack ::Periodic
       and BC::boundaryCondYFront != BC::BoundaryCondYFront::Periodic) {

    isCompatible = false;

  }

  if (     BC::boundaryCondYBack  != BC::BoundaryCondYBack ::Periodic
       and BC::boundaryCondYFront == BC::BoundaryCondYFront::Periodic) {

    isCompatible = false;

  }

  if (not isCompatible) {

    std::cout << "y-boundary conditions are not compatible" << std::endl;

  }

  return isCompatible;
}

bool compVectorsByValues( const std::pair<double, Eigen::VectorRow11>& l
                        , const std::pair<double, Eigen::VectorRow11>& r)
{
  return (l.first < r.first);
}

std::vector<std::pair<double, Eigen::VectorRow11> >
sortVectorsByValues(std::vector<std::pair<double, Eigen::VectorRow11> > v)
{
  std::sort(v.begin(), v.end(), compVectorsByValues);

  return v;
}

std::pair<double, double> findMinMaxAlpha1(const Mesh& physicalVars)
{
  double minAlpha1 = 1.;
  double maxAlpha1 = 0.;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      if (physicalVars(0, i, j) < minAlpha1) minAlpha1 = physicalVars(0, i, j);
      if (physicalVars(0, i, j) > maxAlpha1) maxAlpha1 = physicalVars(0, i, j);

    }
  }

  return std::make_pair(minAlpha1, maxAlpha1);
}

void printMinMaxAlpha1(const Mesh& physicalVars)
{
  auto minMaxAlpha1 = findMinMaxAlpha1(physicalVars);
  std::cout << "alpha1 min: "
            << minMaxAlpha1.first
            << ", "
            << "max: "
            << minMaxAlpha1.second
            << std::endl;
}

void dumpMesh(const Mesh& mesh)
{
  using namespace SchemePars;

  std::ofstream meshFile("meshDump.dat");

  const std::string separator(" ");

  for (size_t var = 0; var < Physics::varCount; ++var) {
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

        meshFile << toStringWithPrecision(mesh(var, i, j)) << separator;

      }

      meshFile << std::endl;
    }
    meshFile << std::endl;
  }
  meshFile.close();
}

} // namespace Utility

