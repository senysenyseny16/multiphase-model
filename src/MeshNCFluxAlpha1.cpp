//: MeshNCFluxAlpha1.cpp

#include "MeshNCFluxAlpha1.hpp"
#include "MeshRec.hpp"
#include "MeshMaxC.hpp"
#include "FluxNumerical.hpp"
#include "VariablesAdditional.hpp"

#include "Energy.hpp"
#include "ShearStress.hpp"
#include <cmath>

#include <iostream>

namespace FluxNumerical {

MeshNCFluxAlpha1::MeshNCFluxAlpha1()
  : MeshEdge1D()
{ }

MeshNCFluxAlpha1::~MeshNCFluxAlpha1()
{ }

MeshNCFluxAlpha1HLLC::MeshNCFluxAlpha1HLLC()
  : MeshNCFluxAlpha1()
{ }

MeshNCFluxAlpha1HLLC::~MeshNCFluxAlpha1HLLC()
{ }

void MeshNCFluxAlpha1HLLC::calcFlux( const MeshRec&  rP
                                   , const Mesh&     p)
{
  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell + 1; i <= Geometry::lastCellX; ++i) {

        // Unpack variables.
        const double alpha1B = rP.xF(0,  i - 1, j);
        const double rho2B   = rP.xF(1,  i - 1, j);
        const double rho1B   = rP.xF(2,  i - 1, j);
        const double u11B    = rP.xF(3,  i - 1, j);
        const double u12B    = rP.xF(4,  i - 1, j);
        const double F11B    = rP.xF(7,  i - 1, j);
        const double F12B    = rP.xF(8,  i - 1, j);
        const double F21B    = rP.xF(9,  i - 1, j);
        const double F22B    = rP.xF(10, i - 1, j);

        const double alpha1F = rP.xB(0,  i, j);
        const double rho2F   = rP.xB(1,  i, j);
        const double rho1F   = rP.xB(2,  i, j);
        const double u11F    = rP.xB(3,  i, j);
        const double u12F    = rP.xB(4,  i, j);
        const double F11F    = rP.xB(7,  i, j);
        const double F12F    = rP.xB(8,  i, j);
        const double F21F    = rP.xB(9,  i, j);
        const double F22F    = rP.xB(10, i, j);

        // HLLC in x-direction.

        // 1. Find S_F, S_B.

        // 1. 1. Find C_1, C_2
        const double C1SquaredLiquidB = Energy::dp1(rho1B);
        const double C1SquaredLiquidF = Energy::dp1(rho1F);

        const double C1SquaredPorousB = Physics::mu / (rho1B);
        const double C1SquaredPorousF = Physics::mu / (rho1F);

        const double C1SquaredB = C1SquaredLiquidB + (4. / 3.) * C1SquaredPorousB;
        const double C1SquaredF = C1SquaredLiquidF + (4. / 3.) * C1SquaredPorousF;

        const double C2SquaredB = Energy::dp2(rho2B);
        const double C2SquaredF = Energy::dp2(rho2F);

        const double c1B = VariablesAdditional::c1(alpha1B, rho1B, rho2B);
        const double c1F = VariablesAdditional::c1(alpha1F, rho1F, rho2F);
        const double c2B = 1. - c1B;
        const double c2F = 1. - c1F;

        const double CMixtureB = std::sqrt(c1B * C1SquaredB + c2B * C2SquaredB);
        const double CMixtureF = std::sqrt(c1F * C1SquaredF + c2F * C2SquaredF);

        const double u1B = u11B;
        const double u1F = u11F;

        const double SF = std::max(u1B + CMixtureB, u1F + CMixtureF);
        const double SB = std::min(u1B - CMixtureB, u1F - CMixtureF);

        if (SB >= 0) {

          xB(i, j) = - p(0, i, j) * u1B;

        } else if (SF <= 0) {

          xB(i, j) = - p(0, i, j) * u1F;

        } else {

          // 2. Find S_* (S_M).

          const double fSigma11B = - (alpha1B * Energy::p1(rho1B) + (1. - alpha1B) * Energy::p2(rho2B)) + alpha1B * ShearStress::sigma11(rho1B, F11B, F12B, F21B, F22B);
          const double fSigma11F = - (alpha1F * Energy::p1(rho1F) + (1. - alpha1F) * Energy::p2(rho2F)) + alpha1F * ShearStress::sigma11(rho1F, F11F, F12F, F21F, F22F);
          const double rhoB = VariablesAdditional::rho(alpha1B, rho1B, rho2B);
          const double rhoF = VariablesAdditional::rho(alpha1F, rho1F, rho2F);

          const double SM = ((rhoB * u1B * u1B - fSigma11B) - (rhoF * u1F * u1F - fSigma11F) - SB * rhoB * u1B + SF * rhoF * u1F) / (rhoB * u1B - rhoF * u1F - SB * rhoB + SF * rhoF);

          xB(i, j) = - p(0, i, j) * SM;
        }
    }
  }

  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX - 1; ++i) {

        // Unpack variables.
        const double alpha1B = rP.xF(0,  i, j);
        const double rho2B   = rP.xF(1,  i, j);
        const double rho1B   = rP.xF(2,  i, j);
        const double u11B    = rP.xF(3,  i, j);
        const double u12B    = rP.xF(4,  i, j);
        const double F11B    = rP.xF(7,  i, j);
        const double F12B    = rP.xF(8,  i, j);
        const double F21B    = rP.xF(9,  i, j);
        const double F22B    = rP.xF(10, i, j);

        const double alpha1F = rP.xB(0,  i + 1, j);
        const double rho2F   = rP.xB(1,  i + 1, j);
        const double rho1F   = rP.xB(2,  i + 1, j);
        const double u11F    = rP.xB(3,  i + 1, j);
        const double u12F    = rP.xB(4,  i + 1, j);
        const double F11F    = rP.xB(7,  i + 1, j);
        const double F12F    = rP.xB(8,  i + 1, j);
        const double F21F    = rP.xB(9,  i + 1, j);
        const double F22F    = rP.xB(10, i + 1, j);

        // HLLC in x-direction.

        // 1. Find S_F, S_B.

        // 1. 1. Find C_1, C_2
        const double C1SquaredLiquidB = Energy::dp1(rho1B);
        const double C1SquaredLiquidF = Energy::dp1(rho1F);

        const double C1SquaredPorousB = Physics::mu / (rho1B);
        const double C1SquaredPorousF = Physics::mu / (rho1F);

        const double C1SquaredB = C1SquaredLiquidB + (4. / 3.) * C1SquaredPorousB;
        const double C1SquaredF = C1SquaredLiquidF + (4. / 3.) * C1SquaredPorousF;

        const double C2SquaredB = Energy::dp2(rho2B);
        const double C2SquaredF = Energy::dp2(rho2F);

        const double c1B = VariablesAdditional::c1(alpha1B, rho1B, rho2B);
        const double c1F = VariablesAdditional::c1(alpha1F, rho1F, rho2F);
        const double c2B = 1. - c1B;
        const double c2F = 1. - c1F;

        const double CMixtureB = std::sqrt(c1B * C1SquaredB + c2B * C2SquaredB);
        const double CMixtureF = std::sqrt(c1F * C1SquaredF + c2F * C2SquaredF);

        const double u1B = u11B;
        const double u1F = u11F;

        const double SF = std::max(u1B + CMixtureB, u1F + CMixtureF);
        const double SB = std::min(u1B - CMixtureB, u1F - CMixtureF);

        if (SB >= 0) {

          xF(i, j) = - p(0, i, j) * u1B;

        } else if (SF <= 0) {

          xF(i, j) = - p(0, i, j) * u1F;

        } else {

          // 2. Find S_* (S_M).

          const double fSigma11B = - (alpha1B * Energy::p1(rho1B) + (1. - alpha1B) * Energy::p2(rho2B)) + alpha1B * ShearStress::sigma11(rho1B, F11B, F12B, F21B, F22B);
          const double fSigma11F = - (alpha1F * Energy::p1(rho1F) + (1. - alpha1F) * Energy::p2(rho2F)) + alpha1F * ShearStress::sigma11(rho1F, F11F, F12F, F21F, F22F);
          const double rhoB = VariablesAdditional::rho(alpha1B, rho1B, rho2B);
          const double rhoF = VariablesAdditional::rho(alpha1F, rho1F, rho2F);

          const double SM = ((rhoB * u1B * u1B - fSigma11B) - (rhoF * u1F * u1F - fSigma11F) - SB * rhoB * u1B + SF * rhoF * u1F) / (rhoB * u1B - rhoF * u1F - SB * rhoB + SF * rhoF);

          xF(i, j) = - p(0, i, j) * SM;
        }
    }
  }

  #pragma omp parallel for
  for (size_t j = Geometry::firstCell + 1; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

        // Unpack variables.
        const double alpha1B = rP.yF(0,  i, j - 1);
        const double rho2B   = rP.yF(1,  i, j - 1);
        const double rho1B   = rP.yF(2,  i, j - 1);
        const double u21B    = rP.yF(5,  i, j - 1);
        const double u22B    = rP.yF(6,  i, j - 1);
        const double F11B    = rP.yF(7,  i, j - 1);
        const double F12B    = rP.yF(8,  i, j - 1);
        const double F21B    = rP.yF(9,  i, j - 1);
        const double F22B    = rP.yF(10, i, j - 1);

        const double alpha1F = rP.yB(0,  i, j);
        const double rho2F   = rP.yB(1,  i, j);
        const double rho1F   = rP.yB(2,  i, j);
        const double u21F    = rP.yB(5,  i, j);
        const double u22F    = rP.yB(6,  i, j);
        const double F11F    = rP.yB(7,  i, j);
        const double F12F    = rP.yB(8,  i, j);
        const double F21F    = rP.yB(9,  i, j);
        const double F22F    = rP.yB(10, i, j);

        // HLLC in y-direction.

        // 1. Find S_F, S_B.

        // 1. 1. Find C_1, C_2
        const double C1SquaredLiquidB = Energy::dp1(rho1B);
        const double C1SquaredLiquidF = Energy::dp1(rho1F);

        const double C1SquaredPorousB = Physics::mu / rho1B;
        const double C1SquaredPorousF = Physics::mu / rho1F;

        const double C1SquaredB = C1SquaredLiquidB + (4. / 3.) * C1SquaredPorousB;
        const double C1SquaredF = C1SquaredLiquidF + (4. / 3.) * C1SquaredPorousF;

        const double C2SquaredB = Energy::dp2(rho2B);
        const double C2SquaredF = Energy::dp2(rho2F);

        const double c1B = VariablesAdditional::c1(alpha1B, rho1B, rho2B);
        const double c1F = VariablesAdditional::c1(alpha1F, rho1F, rho2F);
        const double c2B = 1. - c1B;
        const double c2F = 1. - c1F;

        const double CMixtureB = std::sqrt(c1B * C1SquaredB + c2B * C2SquaredB);
        const double CMixtureF = std::sqrt(c1F * C1SquaredF + c2F * C2SquaredF);

        const double u2B = VariablesAdditional::u2(c1B, c2B, u21B, u22B);
        const double u2F = VariablesAdditional::u2(c1F, c2F, u21F, u22F);

        const double SF = std::max(u2B + CMixtureB, u2F + CMixtureF);
        const double SB = std::min(u2B - CMixtureB, u2F - CMixtureF);

        if (SB >= 0) {

          yB(i, j) = -p(0, i, j) * u2B;

        } else if (SF <= 0) {

          yB(i, j) = -p(0, i, j) * u2F;

        } else {

          // 2. Find S_* (S_M).

          const double fSigma22B = - (alpha1B * Energy::p1(rho1B) + (1. - alpha1B) * Energy::p2(rho2B)) + alpha1B * ShearStress::sigma22(rho1B, F11B, F12B, F21B, F22B);
          const double fSigma22F = - (alpha1F * Energy::p1(rho1F) + (1. - alpha1F) * Energy::p2(rho2F)) + alpha1F * ShearStress::sigma22(rho1F, F11F, F12F, F21F, F22F);
          const double rhoB = VariablesAdditional::rho(alpha1B, rho1B, rho2B);
          const double rhoF = VariablesAdditional::rho(alpha1F, rho1F, rho2F);

          const double SM = ((rhoB * u2B * u2B - fSigma22B) - (rhoF * u2F * u2F - fSigma22F) - SB * rhoB * u2B + SF * rhoF * u2F) / (rhoB * u2B - rhoF * u2F - SB * rhoB + SF * rhoF);

          yB(i, j) = -p(0, i, j) * SM;

        }

    }
  }

  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY - 1; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

        // Unpack variables.
        const double alpha1B = rP.yF(0,  i, j);
        const double rho2B   = rP.yF(1,  i, j);
        const double rho1B   = rP.yF(2,  i, j);
        const double u21B    = rP.yF(5,  i, j);
        const double u22B    = rP.yF(6,  i, j);
        const double F11B    = rP.yF(7,  i, j);
        const double F12B    = rP.yF(8,  i, j);
        const double F21B    = rP.yF(9,  i, j);
        const double F22B    = rP.yF(10, i, j);

        const double alpha1F = rP.yB(0,  i, j + 1);
        const double rho2F   = rP.yB(1,  i, j + 1);
        const double rho1F   = rP.yB(2,  i, j + 1);
        const double u21F    = rP.yB(5,  i, j + 1);
        const double u22F    = rP.yB(6,  i, j + 1);
        const double F11F    = rP.yB(7,  i, j + 1);
        const double F12F    = rP.yB(8,  i, j + 1);
        const double F21F    = rP.yB(9,  i, j + 1);
        const double F22F    = rP.yB(10, i, j + 1);

        // HLLC in x-direction.

        // 1. Find S_F, S_B.

        // 1. 1. Find C_1, C_2
        const double C1SquaredLiquidB = Energy::dp1(rho1B);
        const double C1SquaredLiquidF = Energy::dp1(rho1F);

        const double C1SquaredPorousB = Physics::mu / rho1B;
        const double C1SquaredPorousF = Physics::mu / rho1F;

        const double C1SquaredB = C1SquaredLiquidB + (4. / 3.) * C1SquaredPorousB * std::pow(alpha1B, 0);
        const double C1SquaredF = C1SquaredLiquidF + (4. / 3.) * C1SquaredPorousF * std::pow(alpha1F, 0);

        const double C2SquaredB = Energy::dp2(rho2B);
        const double C2SquaredF = Energy::dp2(rho2F);

        const double c1B = VariablesAdditional::c1(alpha1B, rho1B, rho2B);
        const double c1F = VariablesAdditional::c1(alpha1F, rho1F, rho2F);
        const double c2B = 1. - c1B;
        const double c2F = 1. - c1F;

        const double CMixtureB = std::sqrt(c1B * C1SquaredB + c2B * C2SquaredB);
        const double CMixtureF = std::sqrt(c1F * C1SquaredF + c2F * C2SquaredF);

        const double u2B = VariablesAdditional::u2(c1B, c2B, u21B, u22B);
        const double u2F = VariablesAdditional::u2(c1F, c2F, u21F, u22F);

        const double SF = std::max(u2B + CMixtureB, u2F + CMixtureF);
        const double SB = std::min(u2B - CMixtureB, u2F - CMixtureF);

        if (SB >= 0) {

          yF(i, j) = -p(0, i, j) * u2B;

        } else if (SF <= 0) {

          yF(i, j) = -p(0, i, j) * u2F;

        } else {

          // 2. Find S_* (S_M).

          const double fSigma22B = - (alpha1B * Energy::p1(rho1B) + (1. - alpha1B) * Energy::p2(rho2B)) + alpha1B * ShearStress::sigma22(rho1B, F11B, F12B, F21B, F22B);
          const double fSigma22F = - (alpha1F * Energy::p1(rho1F) + (1. - alpha1F) * Energy::p2(rho2F)) + alpha1F * ShearStress::sigma22(rho1F, F11F, F12F, F21F, F22F);
          const double rhoB = VariablesAdditional::rho(alpha1B, rho1B, rho2B);
          const double rhoF = VariablesAdditional::rho(alpha1F, rho1F, rho2F);

          const double SM = ((rhoB * u2B * u2B - fSigma22B) - (rhoF * u2F * u2F - fSigma22F) - SB * rhoB * u2B + SF * rhoF * u2F) / (rhoB * u2B - rhoF * u2F - SB * rhoB + SF * rhoF);

          yF(i, j) = -p(0, i, j) * SM;

        }

    }
  }
}

MeshNCFluxAlpha1* createMeshNCFluxAlpha1()
{
  switch (SchemePars::fluxType) {

  case (SchemePars::FluxType::HLLC):

    return (new MeshNCFluxAlpha1HLLC);

  break;

  default:

    return nullptr;

  break;
  }
}

} // namespace FluxNumerical

