//: Mesh1D.cpp

#include "Mesh1D.hpp"

Mesh1D::Mesh1D()
  : data(Geometry::meshSizeVar, 0.)
{}

Mesh1D::Mesh1D(const Mesh1D& other)
  : data(other.data)
{}

Mesh1D& Mesh1D::operator=(const Mesh1D& right)
{
  if (this == &right) {
    return *this;
  }

  data = right.data;

  return *this;
}

Mesh1D::~Mesh1D()
{}

