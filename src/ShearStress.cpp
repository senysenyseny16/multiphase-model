//: ShearStress.cpp

#include "ShearStress.hpp"
#include "Definitions.hpp"

#include <cmath>

using namespace std;
using namespace Physics;

namespace ShearStress {

double sigma11( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  /* return -((4*F11*F12*F21*F22+pow(F11,2)*(pow(F21,2)-pow(F22,2))+pow(F12,2)*(-pow(F21,2)+pow(F22,2))+pow(pow(F21,2)+pow(F22,2),2))*mu*rho1)/(2.*pow(F12*F21-F11*F22,4)*rho10); */
  /* return ((-2*pow(pow(F21,2)+pow(F22,2),2)-2*F11*F12*F21*F22*(3+pow(F21,2)+pow(F22,2))+pow(F11,2)*(pow(F22,2)+pow(F22,4)+pow(F21,2)*(-2+pow(F22,2)))+pow(F12,2)*(pow(F21,4)-2*pow(F22,2)+pow(F21,2)*(1+pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,4)*rho10); */
  return ((pow(F12,4)*(1.+pow(F21,4))-2.*F11*F12*F21*(1.+2.*pow(F12,2)*pow(F21,2))*F22-pow(F12,2)*pow(F22,2)-4.*pow(F11,3)*F12*F21*pow(F22,3)-2.*pow(pow(F21,2)+pow(F22,2),2)+pow(F11,4)*(1.+pow(F22,4))+pow(F11,2)*(-pow(F21,2)+pow(F12,2)*(2.+6.*pow(F21,2)*pow(F22,2))))*mu*rho1)/(6.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,4)*rho10);
}

double sigma12( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return sigma21(rho1, F11, F12, F21, F22);
}

double sigma21( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  /* return ((F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu*rho1)/(2.*pow(F12*F21-F11*F22,4)*rho10); */
  /* return -((pow(F12,3)*(-2+pow(F21,2))*F22+pow(F11,3)*F21*(-2+pow(F22,2))+pow(F11,2)*F12*F22*(-2-2*pow(F21,2)+pow(F22,2))-2*F11*F21*(pow(F21,2)+pow(F22,2))-2*F12*F22*(pow(F21,2)+pow(F22,2))+F11*pow(F12,2)*F21*(pow(F21,2)-2*(1+pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,4)*rho10); */
  return (pow(pow(F12*F21-F11*F22,-2),1.3333333333333333)*(F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu*rho1)/(2.*rho10);
}

double sigma22( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  /* return -((pow(F11,4)+4*F11*F12*F21*F22+pow(F11,2)*(2*pow(F12,2)+pow(F21,2)-pow(F22,2))+pow(F12,2)*(pow(F12,2)-pow(F21,2)+pow(F22,2)))*mu*rho1)/(2.*pow(F12*F21-F11*F22,4)*rho10); */
  /* return ((-2*pow(F11,3)*F12*F21*F22-2*F11*F12*(3+pow(F12,2))*F21*F22+pow(F12,2)*(pow(F21,2)+pow(F12,2)*(-2+pow(F21,2))-2*pow(F22,2))+pow(F11,4)*(-2+pow(F22,2))+pow(F11,2)*(-2*pow(F21,2)+pow(F22,2)+pow(F12,2)*(-4+pow(F21,2)+pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,4)*rho10); */
  return ((pow(F12,4)*(-2.+pow(F21,4))-2.*F11*F12*F21*(1.+2.*pow(F12,2)*pow(F21,2))*F22-pow(F12,2)*pow(F22,2)-4.*pow(F11,3)*F12*F21*pow(F22,3)+pow(pow(F21,2)+pow(F22,2),2)+pow(F11,4)*(-2.+pow(F22,4))+pow(F11,2)*(-pow(F21,2)+pow(F12,2)*(-4.+6.*pow(F21,2)*pow(F22,2))))*mu*rho1)/(6.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,4)*rho10);
}

} // namespace ShearStress

