//: LinearizedFluxMatrices.cpp

#include "LinearizedFluxMatrices.hpp"
#include "Definitions.hpp"

namespace LinearizedFluxMatrices {

Eigen::Matrix11 Ax( const double alpha1
                  , const double rho1
                  , const double rho2
                  , const double u11
                  , const double u12
                  , const double u21
                  , const double u22
                  , const double F11
                  , const double F12
                  , const double F21
                  , const double F22)
{
  using namespace Physics;

  Eigen::Matrix11 ax;

  // 1 row.
  ax(0,  0) =  (alpha1*rho1*u11+(1.-alpha1)*rho2*u12)/(alpha1*(rho1-rho2)+rho2);
  ax(0,  1) =  0.0;
  ax(0,  2) =  0.0;
  ax(0,  3) =  0.0;
  ax(0,  4) =  0.0;
  ax(0,  5) =  0.0;
  ax(0,  6) =  0.0;
  ax(0,  7) =  0.0;
  ax(0,  8) =  0.0;
  ax(0,  9) =  0.0;
  ax(0, 10) =  0.0;

  // 2 row.
  ax(1,  0) =  (alpha1*rho1*rho2*(-u11+u12))/((-1+alpha1)*(alpha1*(rho1-rho2)+rho2));
  ax(1,  1) =  u12;
  ax(1,  2) =  0.0;
  ax(1,  3) =  0.0;
  ax(1,  4) =  rho2;
  ax(1,  5) =  0.0;
  ax(1,  6) =  0.0;
  ax(1,  7) =  0.0;
  ax(1,  8) =  0.0;
  ax(1,  9) =  0.0;
  ax(1, 10) =  0.0;

  // 3 row.
  ax(2,  0) =  ((1.-alpha1)*rho1*rho2*(u11-u12))/(alpha1*(alpha1*(rho1-rho2)+rho2));
  ax(2,  1) =  0.0;
  ax(2,  2) =  u11;
  ax(2,  3) =  rho1;
  ax(2,  4) =  0.0;
  ax(2,  5) =  0.0;
  ax(2,  6) =  0.0;
  ax(2,  7) =  0.0;
  ax(2,  8) =  0.0;
  ax(2,  9) =  0.0;
  ax(2, 10) =  0.0;

  // 4 row.
  ax(3,  0) =  (p10-((-2*pow(pow(F21,2)+pow(F22,2),2)-2*F11*F12*F21*F22*(3+pow(F21,2)+pow(F22,2))+pow(F12,2)*(pow(F21,2)+pow(F21,4)+(-2+pow(F21,2))*pow(F22,2))+pow(F11,2)*(pow(F22,2)+pow(F22,4)+pow(F21,2)*(-2+pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,4)*rho10)+(pow(C1,2)*(-1+pow(rho1/rho10,gamma1))*rho10)/gamma1-(gamma2*p20+pow(C2,2)*(-1+pow(rho2/rho20,gamma2))*rho20)/gamma2)/(alpha1*(rho1-rho2)+rho2);
  ax(3,  1) =  0.0;
  ax(3,  2) =  (alpha1*pow(C1,2)*pow(rho1/rho10,-1+gamma1)+(alpha1*(2*pow(pow(F21,2)+pow(F22,2),2)+2*F11*F12*F21*F22*(3+pow(F21,2)+pow(F22,2))-pow(F12,2)*(pow(F21,2)+pow(F21,4)+(-2+pow(F21,2))*pow(F22,2))-pow(F11,2)*(pow(F22,2)+pow(F22,4)+pow(F21,2)*(-2+pow(F22,2))))*mu)/(3.*pow(F12*F21-F11*F22,4)*rho10)-((-1+alpha1)*pow(C1,2)*pow(rho1/rho10,gamma1)*rho10*rho2)/pow(rho1,2))/(alpha1*(rho1-rho2)+rho2);
  ax(3,  3) =  u11;
  ax(3,  4) =  0.0;
  ax(3,  5) =  (pow(-1+alpha1,2)*pow(rho2,2)*(u21-u22))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(3,  6) = -(((-1+alpha1)*alpha1*rho1*rho2*(u21-u22))/pow(alpha1*(rho1-rho2)+rho2,2));
  ax(3,  7) =  (mu*(2*alpha1*(2*F11*F12*F21*(pow(F21,2)+(4+pow(F21,2))*pow(F22,2)+pow(F22,4))-pow(F11,2)*(pow(F22,3)+pow(F22,5)+pow(F21,2)*F22*(-2+pow(F22,2)))+F22*(4*pow(pow(F21,2)+pow(F22,2),2)+pow(F12,2)*(4*pow(F22,2)-pow(F21,2)*(-1+pow(F21,2)+pow(F22,2)))))*rho1+(-1+alpha1)*(pow(F11,3)*F12*F21*(-2+pow(F22,2))+pow(F11,2)*F22*(pow(F22,2)+pow(F22,4)+pow(F21,2)*(-2+pow(F22,2))+pow(F12,2)*(-2-2*pow(F21,2)+pow(F22,2)))+F22*(pow(F12,4)*(-2+pow(F21,2))-2*pow(pow(F21,2)+pow(F22,2),2)+pow(F12,2)*(pow(F21,4)-4*pow(F22,2)+pow(F21,2)*(-1+pow(F22,2))))+F11*F12*F21*(-2*(pow(F21,2)+(4+pow(F21,2))*pow(F22,2)+pow(F22,4))+pow(F12,2)*(pow(F21,2)-2*(1+pow(F22,2)))))*rho2))/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(3,  8) =  (mu*(2*alpha1*(pow(F11,2)*F21*(-pow(F22,2)+pow(F22,4)+pow(F21,2)*(-4+pow(F22,2)))-2*F11*F12*F22*(pow(F21,4)+pow(F22,2)+pow(F21,2)*(4+pow(F22,2)))+F21*(-4*pow(pow(F21,2)+pow(F22,2),2)+pow(F12,2)*(pow(F21,2)+pow(F21,4)+(-2+pow(F21,2))*pow(F22,2))))*rho1-(-1+alpha1)*(pow(F11,4)*F21*(-2+pow(F22,2))+pow(F11,3)*F12*F22*(-2-2*pow(F21,2)+pow(F22,2))+F21*(-2*pow(pow(F21,2)+pow(F22,2),2)+pow(F12,2)*(pow(F21,2)+pow(F21,4)+(-2+pow(F21,2))*pow(F22,2)))+pow(F11,2)*F21*(-pow(F22,2)+pow(F22,4)+pow(F21,2)*(-4+pow(F22,2))+pow(F12,2)*(pow(F21,2)-2*(1+pow(F22,2))))+F11*F12*F22*(pow(F12,2)*(-2+pow(F21,2))-2*(pow(F21,4)+pow(F22,2)+pow(F21,2)*(4+pow(F22,2)))))*rho2))/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(3,  9) =  (mu*(2*alpha1*(pow(F11,3)*F21*F22*(-2+pow(F22,2))-4*F12*pow(F22,2)*(pow(F21,2)+pow(F22,2))+pow(F12,3)*(pow(F21,2)+(-4+pow(F21,2))*pow(F22,2))-pow(F11,2)*F12*(pow(F22,2)-pow(F22,4)+2*pow(F21,2)*(1+pow(F22,2)))+F11*F21*F22*(pow(F12,2)*(-8+pow(F21,2)-2*pow(F22,2))-4*(pow(F21,2)+pow(F22,2))))*rho1-(-1+alpha1)*(F12*(-2*pow(pow(F11,2)+pow(F12,2),2)+(pow(F12,2)+pow(F12,4)+pow(F11,2)*(-2+pow(F12,2)))*pow(F21,2))-F11*F21*(2*(pow(F11,2)+(4+pow(F11,2))*pow(F12,2)+pow(F12,4))-(-2+pow(F12,2))*pow(F21,2))*F22+F12*(pow(F11,4)-2*pow(F21,2)+pow(F11,2)*(-1+pow(F12,2)-2*pow(F21,2))+pow(F12,2)*(-4+pow(F21,2)))*pow(F22,2)+F11*(pow(F11,2)-2*(1+pow(F12,2)))*F21*pow(F22,3)+(-2+pow(F11,2))*F12*pow(F22,4))*rho2))/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(3, 10) =  (mu*(2*alpha1*(F11*pow(F21,2)*(4*pow(F11,2)+pow(F12,2)-(-4+pow(F12,2))*pow(F21,2))+F12*F21*(4*pow(F21,2)-pow(F12,2)*(-2+pow(F21,2))+2*pow(F11,2)*(4+pow(F21,2)))*F22-F11*(pow(F11,2)-2*pow(F12,2)+(pow(F11,2)-2*(2+pow(F12,2)))*pow(F21,2))*pow(F22,2)-(-4+pow(F11,2))*F12*F21*pow(F22,3))*rho1-(-1+alpha1)*(F11*(2*pow(pow(F11,2)+pow(F12,2),2)+(pow(F12,2)-pow(F12,4)-pow(F11,2)*(-4+pow(F12,2)))*pow(F21,2)-(-2+pow(F12,2))*pow(F21,4))+F12*F21*(2*pow(F11,4)+2*pow(F21,2)-pow(F12,2)*(-2+pow(F21,2))+2*pow(F11,2)*(4+pow(F12,2)+pow(F21,2)))*F22-F11*(pow(F11,4)+pow(F11,2)*(1+pow(F12,2)+pow(F21,2))-2*(pow(F21,2)+pow(F12,2)*(1+pow(F21,2))))*pow(F22,2)-(-2+pow(F11,2))*F12*F21*pow(F22,3))*rho2))/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));

  // 5 row.
  ax(4,  0) =  (p10-((-2*pow(pow(F21,2)+pow(F22,2),2)-2*F11*F12*F21*F22*(3+pow(F21,2)+pow(F22,2))+pow(F12,2)*(pow(F21,2)+pow(F21,4)+(-2+pow(F21,2))*pow(F22,2))+pow(F11,2)*(pow(F22,2)+pow(F22,4)+pow(F21,2)*(-2+pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,4)*rho10)+(pow(C1,2)*(-1+pow(rho1/rho10,gamma1))*rho10)/gamma1-(gamma2*p20+pow(C2,2)*(-1+pow(rho2/rho20,gamma2))*rho20)/gamma2)/(alpha1*(rho1-rho2)+rho2);
  ax(4,  1) =  (pow(C2,2)*pow(rho2/rho20,gamma2)*rho20)/pow(rho2,2);
  ax(4,  2) =  (alpha1*(2*pow(pow(F21,2)+pow(F22,2),2)+2*F11*F12*F21*F22*(3+pow(F21,2)+pow(F22,2))-pow(F12,2)*(pow(F21,2)+pow(F21,4)+(-2+pow(F21,2))*pow(F22,2))-pow(F11,2)*(pow(F22,2)+pow(F22,4)+pow(F21,2)*(-2+pow(F22,2))))*mu)/(3.*pow(F12*F21-F11*F22,4)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(4,  3) =  0.0;
  ax(4,  4) =  u12;
  ax(4,  5) =  ((-1+alpha1)*alpha1*rho1*rho2*(u21-u22))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(4,  6) =  (pow(alpha1,2)*pow(rho1,2)*(-u21+u22))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(4,  7) =  (alpha1*(pow(F11,3)*F12*F21*(-2+pow(F22,2))-pow(F11,2)*F22*(pow(F22,2)+pow(F22,4)+pow(F12,2)*(2+2*pow(F21,2)-pow(F22,2))+pow(F21,2)*(-2+pow(F22,2)))+F11*F12*F21*(2*(pow(F21,2)+(4+pow(F21,2))*pow(F22,2)+pow(F22,4))+pow(F12,2)*(pow(F21,2)-2*(1+pow(F22,2))))+F22*(pow(F12,4)*(-2+pow(F21,2))+6*pow(pow(F21,2)+pow(F22,2),2)+pow(F12,2)*(4*pow(F22,2)-pow(F21,2)*(-1+pow(F21,2)+pow(F22,2)))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(4,  8) = -(alpha1*(pow(F11,4)*F21*(-2+pow(F22,2))+pow(F11,3)*F12*F22*(-2-2*pow(F21,2)+pow(F22,2))+F21*(6*pow(pow(F21,2)+pow(F22,2),2)-pow(F12,2)*(pow(F21,2)+pow(F21,4)+(-2+pow(F21,2))*pow(F22,2)))+pow(F11,2)*F21*(pow(F22,2)-pow(F22,4)-pow(F21,2)*(-4+pow(F22,2))+pow(F12,2)*(pow(F21,2)-2*(1+pow(F22,2))))+F11*F12*F22*(pow(F12,2)*(-2+pow(F21,2))+2*(pow(F21,4)+pow(F22,2)+pow(F21,2)*(4+pow(F22,2)))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(4,  9) = -(alpha1*(F12*(-2*pow(pow(F11,2)+pow(F12,2),2)+(-pow(F12,2)+pow(F12,4)+pow(F11,2)*(2+pow(F12,2)))*pow(F21,2))+F11*F21*(-2*pow(F11,2)*(-1+pow(F12,2))+6*pow(F21,2)-pow(F12,2)*(-8+2*pow(F12,2)+pow(F21,2)))*F22+F12*(pow(F11,4)+6*pow(F21,2)-pow(F12,2)*(-4+pow(F21,2))+pow(F11,2)*(1+pow(F12,2)+2*pow(F21,2)))*pow(F22,2)+F11*(6-pow(F11,2)+2*pow(F12,2))*F21*pow(F22,3)-(-6+pow(F11,2))*F12*pow(F22,4))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(4, 10) =  (alpha1*(F11*(-2*pow(pow(F11,2)+pow(F12,2),2)+(pow(F12,2)+pow(F12,4)+pow(F11,2)*(4+pow(F12,2)))*pow(F21,2)-(-6+pow(F12,2))*pow(F21,4))+F12*F21*(2*pow(F12,2)-2*pow(F11,2)*(-4+pow(F11,2)+pow(F12,2))+(6+2*pow(F11,2)-pow(F12,2))*pow(F21,2))*F22+F11*(pow(F11,4)+pow(F11,2)*(-1+pow(F12,2)-pow(F21,2))+2*(pow(F12,2)+(3+pow(F12,2))*pow(F21,2)))*pow(F22,2)-(-6+pow(F11,2))*F12*F21*pow(F22,3))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));

  // 6 row.
  ax(5,  0) =  ((F11*F21+F12*F22)*(pow(F12,2)*(-2+pow(F21,2))-2*F11*F12*F21*F22+pow(F11,2)*(-2+pow(F22,2))-2*(pow(F21,2)+pow(F22,2)))*mu*rho1)/(3.*pow(F12*F21-F11*F22,4)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(5,  1) =  0.0;
  ax(5,  2) =  (alpha1*(F11*F21+F12*F22)*(pow(F12,2)*(-2+pow(F21,2))-2*F11*F12*F21*F22+pow(F11,2)*(-2+pow(F22,2))-2*(pow(F21,2)+pow(F22,2)))*mu)/(3.*pow(F12*F21-F11*F22,4)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(5,  3) =  0.0;
  ax(5,  4) =  0.0;
  ax(5,  5) =  (alpha1*rho1*(alpha1*(rho1-2*rho2)+2*rho2)*u11+pow(-1+alpha1,2)*pow(rho2,2)*u12)/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(5,  6) =  ((-1+alpha1)*alpha1*rho1*rho2*(u11-u12))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(5,  7) =  (alpha1*(pow(F11,3)*F21*F22*(-2+pow(F22,2))-2*F12*(pow(F21,2)+pow(F22,2))*(pow(F21,2)+4*pow(F22,2))-pow(F11,2)*F12*(6*pow(F21,2)+(4+pow(F21,2))*pow(F22,2)-2*pow(F22,4))+pow(F12,3)*(pow(F21,4)-8*pow(F22,2)+2*pow(F21,2)*(-1+pow(F22,2)))-F11*F21*F22*(6*(pow(F21,2)+pow(F22,2))+pow(F12,2)*(10+pow(F21,2)+4*pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(5,  8) =  (alpha1*(pow(F11,2)*F12*F21*F22*(10+4*pow(F21,2)+pow(F22,2))-pow(F11,3)*(-8*pow(F21,2)+2*(-1+pow(F21,2))*pow(F22,2)+pow(F22,4))+F12*F21*F22*(-(pow(F12,2)*(-2+pow(F21,2)))+6*(pow(F21,2)+pow(F22,2)))+F11*(2*(pow(F21,2)+pow(F22,2))*(4*pow(F21,2)+pow(F22,2))+pow(F12,2)*(6*pow(F22,2)+pow(F21,2)*(4-2*pow(F21,2)+pow(F22,2)))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(5,  9) =  (alpha1*(F11*F12*F21*(6*(pow(F11,2)+pow(F12,2))-(-2+pow(F12,2))*pow(F21,2))+(2*(pow(F11,2)+pow(F12,2))*(pow(F11,2)+4*pow(F12,2))+(6*pow(F11,2)+(4+pow(F11,2))*pow(F12,2)-2*pow(F12,4))*pow(F21,2))*F22+F11*F12*(10+pow(F11,2)+4*pow(F12,2))*F21*pow(F22,2)-(pow(F11,4)-8*pow(F12,2)+2*pow(F11,2)*(-1+pow(F12,2)))*pow(F22,3))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(5, 10) =  (alpha1*(-2*(pow(F11,2)+pow(F12,2))*(4*pow(F11,2)+pow(F12,2))*F21+(-8*pow(F11,2)+2*(-1+pow(F11,2))*pow(F12,2)+pow(F12,4))*pow(F21,3)-F11*F12*(6*(pow(F11,2)+pow(F12,2))+(10+4*pow(F11,2)+pow(F12,2))*pow(F21,2))*F22+(2*pow(F11,4)-6*pow(F12,2)-pow(F11,2)*(4+pow(F12,2)))*F21*pow(F22,2)+F11*(-2+pow(F11,2))*F12*pow(F22,3))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));

  // 7 row.
  ax(6,  0) =  ((F11*F21+F12*F22)*(pow(F12,2)*(-2+pow(F21,2))-2*F11*F12*F21*F22+pow(F11,2)*(-2+pow(F22,2))-2*(pow(F21,2)+pow(F22,2)))*mu*rho1)/(3.*pow(F12*F21-F11*F22,4)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(6,  1) =  0.0;
  ax(6,  2) =  (alpha1*(F11*F21+F12*F22)*(pow(F12,2)*(-2+pow(F21,2))-2*F11*F12*F21*F22+pow(F11,2)*(-2+pow(F22,2))-2*(pow(F21,2)+pow(F22,2)))*mu)/(3.*pow(F12*F21-F11*F22,4)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(6,  3) =  0.0;
  ax(6,  4) =  0.0;
  ax(6,  5) =  ((1.-alpha1)*alpha1*rho1*rho2*(u11-u12))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(6,  6) =  (pow(alpha1,2)*pow(rho1,2)*u11+(-1+alpha1)*rho2*(-2*alpha1*rho1+(-1+alpha1)*rho2)*u12)/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(6,  7) =  (alpha1*(pow(F11,3)*F21*F22*(-2+pow(F22,2))-2*F12*(pow(F21,2)+pow(F22,2))*(pow(F21,2)+4*pow(F22,2))-pow(F11,2)*F12*(6*pow(F21,2)+(4+pow(F21,2))*pow(F22,2)-2*pow(F22,4))+pow(F12,3)*(pow(F21,4)-8*pow(F22,2)+2*pow(F21,2)*(-1+pow(F22,2)))-F11*F21*F22*(6*(pow(F21,2)+pow(F22,2))+pow(F12,2)*(10+pow(F21,2)+4*pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(6,  8) =  (alpha1*(pow(F11,2)*F12*F21*F22*(10+4*pow(F21,2)+pow(F22,2))-pow(F11,3)*(-8*pow(F21,2)+2*(-1+pow(F21,2))*pow(F22,2)+pow(F22,4))+F12*F21*F22*(-(pow(F12,2)*(-2+pow(F21,2)))+6*(pow(F21,2)+pow(F22,2)))+F11*(2*(pow(F21,2)+pow(F22,2))*(4*pow(F21,2)+pow(F22,2))+pow(F12,2)*(6*pow(F22,2)+pow(F21,2)*(4-2*pow(F21,2)+pow(F22,2)))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(6,  9) =  (alpha1*(F11*F12*F21*(6*(pow(F11,2)+pow(F12,2))-(-2+pow(F12,2))*pow(F21,2))+(2*(pow(F11,2)+pow(F12,2))*(pow(F11,2)+4*pow(F12,2))+(6*pow(F11,2)+(4+pow(F11,2))*pow(F12,2)-2*pow(F12,4))*pow(F21,2))*F22+F11*F12*(10+pow(F11,2)+4*pow(F12,2))*F21*pow(F22,2)-(pow(F11,4)-8*pow(F12,2)+2*pow(F11,2)*(-1+pow(F12,2)))*pow(F22,3))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));
  ax(6, 10) =  (alpha1*(-2*(pow(F11,2)+pow(F12,2))*(4*pow(F11,2)+pow(F12,2))*F21+(-8*pow(F11,2)+2*(-1+pow(F11,2))*pow(F12,2)+pow(F12,4))*pow(F21,3)-F11*F12*(6*(pow(F11,2)+pow(F12,2))+(10+4*pow(F11,2)+pow(F12,2))*pow(F21,2))*F22+(2*pow(F11,4)-6*pow(F12,2)-pow(F11,2)*(4+pow(F12,2)))*F21*pow(F22,2)+F11*(-2+pow(F11,2))*F12*pow(F22,3))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10*(alpha1*(rho1-rho2)+rho2));

  // 8 row.
  ax(7,  0) =  (F11*rho1*rho2*(-u11+u12))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(7,  1) =  ((1.-alpha1)*alpha1*F11*rho1*(u11-u12))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(7,  2) =  ((-1.+alpha1)*alpha1*F11*rho2*(u11-u12))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(7,  3) = -((alpha1*F11*rho1)/(alpha1*(rho1-rho2)+rho2));
  ax(7,  4) =  ((-1.+alpha1)*F11*rho2)/(alpha1*(rho1-rho2)+rho2);
  ax(7,  5) =  0.0;
  ax(7,  6) =  0.0;
  ax(7,  7) =  (alpha1*rho1*u11-(-1+alpha1)*rho2*u12)/(alpha1*(rho1-rho2)+rho2);
  ax(7,  8) =  0.0;
  ax(7,  9) =  0.0;
  ax(7, 10) =  0.0;

  // 9 row.
  ax(8,  0) =  (F12*rho1*rho2*(-u11+u12))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(8,  1) =  ((1.-alpha1)*alpha1*F12*rho1*(u11-u12))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(8,  2) =  ((-1.+alpha1)*alpha1*F12*rho2*(u11-u12))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(8,  3) = -((alpha1*F12*rho1)/(alpha1*(rho1-rho2)+rho2));
  ax(8,  4) =  ((-1.+alpha1)*F12*rho2)/(alpha1*(rho1-rho2)+rho2);
  ax(8,  5) =  0.0;
  ax(8,  6) =  0.0;
  ax(8,  7) =  0.0;
  ax(8,  8) =  (alpha1*rho1*u11-(-1+alpha1)*rho2*u12)/(alpha1*(rho1-rho2)+rho2);
  ax(8,  9) =  0.0;
  ax(8, 10) =  0.0;

  // 10 row.
  ax(9,  0) =  (F11*rho1*rho2*(-u21+u22))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(9,  1) = -(((-1.+alpha1)*alpha1*F11*rho1*(u21-u22))/pow(alpha1*(rho1-rho2)+rho2,2));
  ax(9,  2) =  ((-1.+alpha1)*alpha1*F11*rho2*(u21-u22))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(9,  3) =  0.0;
  ax(9,  4) =  0.0;
  ax(9,  5) = -((alpha1*F11*rho1)/(alpha1*(rho1-rho2)+rho2));
  ax(9,  6) =  ((-1.+alpha1)*F11*rho2)/(alpha1*(rho1-rho2)+rho2);
  ax(9,  7) =  0.0;
  ax(9,  8) =  0.0;
  ax(9,  9) =  (alpha1*rho1*u11-(-1+alpha1)*rho2*u12)/(alpha1*(rho1-rho2)+rho2);
  ax(9, 10) =  0.0;

  // 11 row.
  ax(10,  0) =  (F12*rho1*rho2*(-u21+u22))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(10,  1) =  ((1.-alpha1)*alpha1*F12*rho1*(u21-u22))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(10,  2) =  ((-1.+alpha1)*alpha1*F12*rho2*(u21-u22))/pow(alpha1*(rho1-rho2)+rho2,2);
  ax(10,  3) =  0.0;
  ax(10,  4) =  0.0;
  ax(10,  5) = -((alpha1*F12*rho1)/(alpha1*(rho1-rho2)+rho2));
  ax(10,  6) =  ((-1.+alpha1)*F12*rho2)/(alpha1*(rho1-rho2)+rho2);
  ax(10,  7) =  0.0;
  ax(10,  8) =  0.0;
  ax(10,  9) =  0.0;
  ax(10, 10) =  (alpha1*rho1*u11-(-1+alpha1)*rho2*u12)/(alpha1*(rho1-rho2)+rho2);

  return ax;
}

Eigen::Matrix11 Ay( const double alpha1
                  , const double rho
                  , const double rho1
                  , const double u11
                  , const double u12
                  , const double u21
                  , const double u22
                  , const double F11
                  , const double F12
                  , const double F21
                  , const double F22)
{
  using namespace Physics;

  Eigen::Matrix11 ay;

  // 1 row.
  ay(0,  0) = (alpha1*rho1*(u21-u22)+rho*u22)/rho;
  ay(0,  1) = 0.0;
  ay(0,  2) = 0.0;
  ay(0,  3) = 0.0;
  ay(0,  4) = 0.0;
  ay(0,  5) = 0.0;
  ay(0,  6) = 0.0;
  ay(0,  7) = 0.0;
  ay(0,  8) = 0.0;
  ay(0,  9) = 0.0;
  ay(0, 10) = 0.0;

  // 2 row.
  ay(1,  0) = rho1*(u21-u22);
  ay(1,  1) = u22;
  ay(1,  2) = alpha1*(u21-u22);
  ay(1,  3) = 0.0;
  ay(1,  4) = 0.0;
  ay(1,  5) = alpha1*rho1;
  ay(1,  6) = rho-alpha1*rho1;
  ay(1,  7) = 0.0;
  ay(1,  8) = 0.0;
  ay(1,  9) = 0.0;
  ay(1, 10) = 0.0;

  // 3 row.
  ay(2,  0) = (rho1*(rho-alpha1*rho1)*(u21-u22))/(alpha1*rho);
  ay(2,  1) = 0.0;
  ay(2,  2) = u21;
  ay(2,  3) = 0.0;
  ay(2,  4) = 0.0;
  ay(2,  5) = rho1;
  ay(2,  6) = 0.0;
  ay(2,  7) = 0.0;
  ay(2,  8) = 0.0;
  ay(2,  9) = 0.0;
  ay(2, 10) = 0.0;

  // 4 row.
  ay(3,  0) = -(pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*(F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu*rho1)/(2.*rho*rho10);
  ay(3,  1) =  0.0;
  ay(3,  2) = -(alpha1*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*(F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu)/(2.*rho*rho10);
  ay(3,  3) =  (alpha1*rho1*(2*rho-alpha1*rho1)*u21+pow(rho-alpha1*rho1,2)*u22)/pow(rho,2);
  ay(3,  4) =  (alpha1*rho1*(-rho+alpha1*rho1)*(u21-u22))/pow(rho,2);
  ay(3,  5) =  0.0;
  ay(3,  6) =  0.0;
  ay(3,  7) = -(alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(-(pow(F11,3)*F21*F22)+pow(F11,2)*F12*(9*pow(F21,2)+2*pow(F22,2))+F12*(pow(F12,2)+pow(F21,2)+pow(F22,2))*(3*pow(F21,2)+8*pow(F22,2))+F11*F21*F22*(11*pow(F12,2)+5*(pow(F21,2)+pow(F22,2))))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(3,  8) =  (alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(2*F11*pow(F21,2)*(4*pow(F11,2)+pow(F12,2)+4*pow(F21,2))+F12*F21*(11*pow(F11,2)-pow(F12,2)+5*pow(F21,2))*F22+F11*(3*pow(F11,2)+9*pow(F12,2)+11*pow(F21,2))*pow(F22,2)+5*F12*F21*pow(F22,3)+3*F11*pow(F22,4))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(3,  9) =  (alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(5*pow(F11,3)*F12*F21+3*pow(F11,4)*F22+pow(F11,2)*F22*(11*pow(F12,2)+9*pow(F21,2)+3*pow(F22,2))+2*pow(F12,2)*F22*(4*pow(F12,2)+pow(F21,2)+4*pow(F22,2))+F11*F12*F21*(5*pow(F12,2)-pow(F21,2)+11*pow(F22,2)))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(3, 10) = -(alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*((8*pow(F11,2)+3*pow(F12,2))*F21*(pow(F11,2)+pow(F12,2)+pow(F21,2))+F11*F12*(5*(pow(F11,2)+pow(F12,2))+11*pow(F21,2))*F22+(2*pow(F11,2)+9*pow(F12,2))*F21*pow(F22,2)-F11*F12*pow(F22,3))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho*rho10);

  // 5 row.
  ay(4,  0) = -(pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*(F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu*rho1)/(2.*rho*rho10);
  ay(4,  1) =  0.0;
  ay(4,  2) = -(alpha1*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*(F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu)/(2.*rho*rho10);
  ay(4,  3) =  (alpha1*rho1*(rho-alpha1*rho1)*(u21-u22))/pow(rho,2);
  ay(4,  4) =  (pow(alpha1,2)*pow(rho1,2)*(u21-u22)+pow(rho,2)*u22)/pow(rho,2);
  ay(4,  5) =  0.0;
  ay(4,  6) =  0.0;
  ay(4,  7) = -(alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(-(pow(F11,3)*F21*F22)+pow(F11,2)*F12*(9*pow(F21,2)+2*pow(F22,2))+F12*(pow(F12,2)+pow(F21,2)+pow(F22,2))*(3*pow(F21,2)+8*pow(F22,2))+F11*F21*F22*(11*pow(F12,2)+5*(pow(F21,2)+pow(F22,2))))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(4,  8) =  (alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(2*F11*pow(F21,2)*(4*pow(F11,2)+pow(F12,2)+4*pow(F21,2))+F12*F21*(11*pow(F11,2)-pow(F12,2)+5*pow(F21,2))*F22+F11*(3*pow(F11,2)+9*pow(F12,2)+11*pow(F21,2))*pow(F22,2)+5*F12*F21*pow(F22,3)+3*F11*pow(F22,4))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(4,  9) =  (alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(5*pow(F11,3)*F12*F21+3*pow(F11,4)*F22+pow(F11,2)*F22*(11*pow(F12,2)+9*pow(F21,2)+3*pow(F22,2))+2*pow(F12,2)*F22*(4*pow(F12,2)+pow(F21,2)+4*pow(F22,2))+F11*F12*F21*(5*pow(F12,2)-pow(F21,2)+11*pow(F22,2)))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(4, 10) = -(alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*((8*pow(F11,2)+3*pow(F12,2))*F21*(pow(F11,2)+pow(F12,2)+pow(F21,2))+F11*F12*(5*(pow(F11,2)+pow(F12,2))+11*pow(F21,2))*F22+(2*pow(F11,2)+9*pow(F12,2))*F21*pow(F22,2)-F11*F12*pow(F22,3))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho*rho10);

  // 6 row.
  ay(5,  0) =  (-(pow(F12,4)*(-2+pow(F21,4))*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*gamma1*gamma2*mu*rho1)+4*F11*pow(F12,3)*pow(F21,3)*F22*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*gamma1*gamma2*mu*rho1-pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*pow(pow(F21,2)+pow(F22,2),2)*gamma1*gamma2*mu*rho1-pow(F11,4)*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(-2+pow(F22,4))*gamma1*gamma2*mu*rho1+pow(F11,2)*(pow(F21,2)*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*gamma1*gamma2*mu*rho1+6*pow(F22,2)*rho10*(pow(C1,2)*gamma2*(-1+pow(rho1/rho10,gamma1))*rho10+gamma1*(gamma2*(p10-p20)-pow(C2,2)*rho20*(-1+pow((rho-alpha1*rho1)/(rho20-alpha1*rho20),gamma2)))))+2*F11*F12*F21*F22*(pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(1+2*pow(F11,2)*pow(F22,2))*gamma1*gamma2*mu*rho1+6*rho10*(-(pow(C1,2)*gamma2*(-1+pow(rho1/rho10,gamma1))*rho10)+gamma1*(gamma2*(-p10+p20)+pow(C2,2)*rho20*(-1+pow((rho-alpha1*rho1)/(rho20-alpha1*rho20),gamma2)))))+pow(F12,2)*(pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(4*pow(F11,2)+pow(F22,2))*gamma1*gamma2*mu*rho1-6*pow(F21,2)*(pow(F11,2)*pow(F22,2)*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*gamma1*gamma2*mu*rho1+rho10*(-(pow(C1,2)*gamma2*(-1+pow(rho1/rho10,gamma1))*rho10)+gamma1*(gamma2*(-p10+p20)+pow(C2,2)*rho20*(-1+pow((rho-alpha1*rho1)/(rho20-alpha1*rho20),gamma2)))))))/(6.*pow(F12*F21-F11*F22,2)*gamma1*gamma2*rho*rho10);
  ay(5,  1) =  0.0;
  ay(5,  2) =  (-(alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(pow(F12,4)*(-2+pow(F21,4))-2*F11*F12*F21*(1+2*pow(F12,2)*pow(F21,2))*F22-pow(F12,2)*pow(F22,2)-4*pow(F11,3)*F12*F21*pow(F22,3)+pow(pow(F21,2)+pow(F22,2),2)+pow(F11,4)*(-2+pow(F22,4))+pow(F11,2)*(-pow(F21,2)+pow(F12,2)*(-4+6*pow(F21,2)*pow(F22,2))))*mu*pow(rho1,2))+6*pow(C1,2)*pow(F12*F21-F11*F22,2)*rho*pow(rho1/rho10,gamma1)*pow(rho10,2))/(6.*pow(F12*F21-F11*F22,2)*rho*pow(rho1,2)*rho10);
  ay(5,  3) =  (pow(rho-alpha1*rho1,2)*(u11-u12))/pow(rho,2);
  ay(5,  4) =  (alpha1*rho1*(rho-alpha1*rho1)*(u11-u12))/pow(rho,2);
  ay(5,  5) =  u21;
  ay(5,  6) =  0.0;
  ay(5,  7) =  (pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*mu*(pow(F11,3)*F12*F21*(3*(3+4*pow(F22,4))*rho+alpha1*(15-28*pow(F22,4))*rho1)+pow(F11,4)*(-3*(F22+pow(F22,5))*rho+alpha1*F22*(-5+7*pow(F22,4))*rho1)+pow(F11,2)*F22*(3*(pow(F21,2)+pow(F12,2)*(1-6*pow(F21,2)*pow(F22,2)))*rho+alpha1*(-pow(F21,2)+pow(F12,2)*(5+42*pow(F21,2)*pow(F22,2)))*rho1)+F22*(2*pow(pow(F21,2)+pow(F22,2),2)*(3*rho-7*alpha1*rho1)+pow(F12,2)*(3*pow(F21,2)+4*pow(F22,2))*(3*rho-alpha1*rho1)+pow(F12,4)*(-3*(-2+pow(F21,4))*rho+alpha1*(10+7*pow(F21,4))*rho1))+F11*F12*F21*((3*pow(F21,2)+5*pow(F22,2))*(3*rho-alpha1*rho1)+pow(F12,2)*(3*(3+4*pow(F21,2)*pow(F22,2))*rho+alpha1*(15-28*pow(F21,2)*pow(F22,2))*rho1))))/(18.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(5,  8) = -(pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*mu*(pow(F11,3)*F12*F22*(3*(3+4*pow(F21,2)*pow(F22,2))*rho+alpha1*(15-28*pow(F21,2)*pow(F22,2))*rho1)+pow(F11,4)*F21*(-3*(-2+pow(F22,4))*rho+alpha1*(10+7*pow(F22,4))*rho1)+F11*F12*F22*((5*pow(F21,2)+3*pow(F22,2))*(3*rho-alpha1*rho1)+pow(F12,2)*(3*(3+4*pow(F21,4))*rho+alpha1*(15-28*pow(F21,4))*rho1))+F21*(2*pow(pow(F21,2)+pow(F22,2),2)*(3*rho-7*alpha1*rho1)+pow(F12,2)*pow(F22,2)*(3*rho-alpha1*rho1)+pow(F12,4)*(-3*(1+pow(F21,4))*rho+alpha1*(-5+7*pow(F21,4))*rho1))+pow(F11,2)*F21*((4*pow(F21,2)+3*pow(F22,2))*(3*rho-alpha1*rho1)+pow(F12,2)*(3*(1-6*pow(F21,2)*pow(F22,2))*rho+alpha1*(5+42*pow(F21,2)*pow(F22,2))*rho1))))/(18.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(5,  9) =  (pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*mu*(pow(F11,3)*F21*F22*(-3*(3+4*pow(F12,2)*pow(F22,2))*rho+alpha1*(3+28*pow(F12,2)*pow(F22,2))*rho1)+pow(F11,4)*F12*(3*(-2+pow(F22,4))*rho-alpha1*(10+7*pow(F22,4))*rho1)+F11*F21*F22*(-3*(pow(F21,2)+pow(F22,2))*(3*rho-7*alpha1*rho1)+5*pow(F12,2)*(-3*rho+alpha1*rho1)+4*pow(F12,4)*pow(F21,2)*(-3*rho+7*alpha1*rho1))+F12*((pow(F21,2)-2*pow(F22,2))*(pow(F21,2)+pow(F22,2))*(3*rho-7*alpha1*rho1)+4*pow(F12,2)*pow(F22,2)*(-3*rho+alpha1*rho1)+pow(F12,4)*(3*(-2+pow(F21,4))*rho-alpha1*(10+7*pow(F21,4))*rho1))+pow(F11,2)*F12*((-pow(F21,2)-3*pow(F22,2))*(3*rho-alpha1*rho1)+2*pow(F12,2)*((-6+9*pow(F21,2)*pow(F22,2))*rho-alpha1*(10+21*pow(F21,2)*pow(F22,2))*rho1))))/(18.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(5, 10) =  (pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*mu*(4*pow(F11,4)*F12*F21*pow(F22,3)*(3*rho-7*alpha1*rho1)+pow(F11,2)*F12*F21*F22*(3*(5+4*pow(F12,2)*pow(F21,2))*rho-alpha1*(5+28*pow(F12,2)*pow(F21,2))*rho1)+pow(F11,5)*(-3*(-2+pow(F22,4))*rho+alpha1*(10+7*pow(F22,4))*rho1)+2*pow(F11,3)*(3*(2*pow(F21,2)+pow(F12,2)*(2-3*pow(F21,2)*pow(F22,2)))*rho+alpha1*(-2*pow(F21,2)+pow(F12,2)*(10+21*pow(F21,2)*pow(F22,2)))*rho1)+3*F12*F21*F22*((pow(F21,2)+pow(F22,2))*(3*rho-7*alpha1*rho1)+pow(F12,2)*(3*rho-alpha1*rho1))+F11*((2*pow(F21,2)-pow(F22,2))*(pow(F21,2)+pow(F22,2))*(3*rho-7*alpha1*rho1)+pow(F12,2)*(3*pow(F21,2)+pow(F22,2))*(3*rho-alpha1*rho1)+pow(F12,4)*(-3*(-2+pow(F21,4))*rho+alpha1*(10+7*pow(F21,4))*rho1))))/(18.*pow(F12*F21-F11*F22,3)*rho*rho10);

  // 7 row.
  ay(6,  0) =  (-((pow(F21,4)*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*mu*rho1)/rho10)-(pow(F12,4)*(-2+pow(F21,4))*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*mu*rho1)/rho10+(2*F11*F12*F21*(1+2*pow(F12,2)*pow(F21,2))*F22*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*mu*rho1)/rho10+(pow(F12,2)*pow(F22,2)*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*mu*rho1)/rho10-(2*pow(F21,2)*pow(F22,2)*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*mu*rho1)/rho10+(4*pow(F11,3)*F12*F21*pow(F22,3)*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*mu*rho1)/rho10-(pow(F22,4)*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*mu*rho1)/rho10-(pow(F11,4)*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*(-2+pow(F22,4))*mu*rho1)/rho10+(pow(F11,2)*pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*(pow(F21,2)+pow(F12,2)*(4-6*pow(F21,2)*pow(F22,2)))*mu*rho1)/rho10+(6*pow(C1,2)*pow(rho1/rho10,gamma1)*rho10)/gamma1+(6*pow(C2,2)*((-1+gamma2)*pow(rho,2)-(-2*alpha1+gamma2)*rho*rho1-pow(alpha1,2)*pow(rho1,2))*rho20*pow((rho-alpha1*rho1)/(rho20-alpha1*rho20),gamma2))/(gamma2*pow(rho-alpha1*rho1,2))+6*(p10-p20-(pow(C1,2)*rho10)/gamma1+(pow(C2,2)*rho20)/gamma2))/(6.*rho);
  ay(6,  1) = -(((-1+alpha1)*pow(C2,2)*rho20*pow((rho-alpha1*rho1)/(rho20-alpha1*rho20),gamma2))/pow(rho-alpha1*rho1,2));
  ay(6,  2) =  (alpha1*(-((pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*(pow(F12,4)*(-2+pow(F21,4))-2*F11*F12*F21*(1+2*pow(F12,2)*pow(F21,2))*F22-pow(F12,2)*pow(F22,2)-4*pow(F11,3)*F12*F21*pow(F22,3)+pow(pow(F21,2)+pow(F22,2),2)+pow(F11,4)*(-2+pow(F22,4))+pow(F11,2)*(-pow(F21,2)+pow(F12,2)*(-4+6*pow(F21,2)*pow(F22,2))))*mu)/rho10)+(6*(-1+alpha1)*pow(C2,2)*rho*rho20*pow((rho-alpha1*rho1)/(rho20-alpha1*rho20),gamma2))/pow(rho-alpha1*rho1,2)))/(6.*rho);
  ay(6,  3) =  (alpha1*rho1*(-rho+alpha1*rho1)*(u11-u12))/pow(rho,2);
  ay(6,  4) =  (pow(alpha1,2)*pow(rho1,2)*(-u11+u12))/pow(rho,2);
  ay(6,  5) =  0.0;
  ay(6,  6) =  u22;
  ay(6,  7) =  (alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(pow(F11,3)*F12*F21*(15-28*pow(F22,4))+pow(F11,4)*F22*(-5+7*pow(F22,4))+F22*(pow(F12,4)*(10+7*pow(F21,4))-14*pow(pow(F21,2)+pow(F22,2),2)-pow(F12,2)*(3*pow(F21,2)+4*pow(F22,2)))-F11*F12*F21*(3*pow(F21,2)+5*pow(F22,2)+pow(F12,2)*(-15+28*pow(F21,2)*pow(F22,2)))+pow(F11,2)*F22*(-pow(F21,2)+pow(F12,2)*(5+42*pow(F21,2)*pow(F22,2))))*mu*rho1)/(18.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(6,  8) =  (alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(F11*F12*F22*(5*pow(F21,2)+pow(F12,2)*(-15+28*pow(F21,4))+3*pow(F22,2))+pow(F11,3)*F12*F22*(-15+28*pow(F21,2)*pow(F22,2))-pow(F11,4)*F21*(10+7*pow(F22,4))+F21*(pow(F12,4)*(5-7*pow(F21,4))+pow(F12,2)*pow(F22,2)+14*pow(pow(F21,2)+pow(F22,2),2))+pow(F11,2)*F21*(4*pow(F21,2)+3*pow(F22,2)-pow(F12,2)*(5+42*pow(F21,2)*pow(F22,2))))*mu*rho1)/(18.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(6,  9) =  (alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(F12*(-10*pow(pow(F11,2)+pow(F12,2),2)+pow(F11,2)*pow(F21,2)-7*(1+pow(F12,4))*pow(F21,4))+F11*F21*(3*pow(F11,2)+5*pow(F12,2)+7*(3+4*pow(F12,4))*pow(F21,2))*F22+F12*(4*pow(F12,2)+7*pow(F21,2)+pow(F11,2)*(3-42*pow(F12,2)*pow(F21,2)))*pow(F22,2)+7*F11*(3+4*pow(F11,2)*pow(F12,2))*F21*pow(F22,3)-7*(-2+pow(F11,4))*F12*pow(F22,4))*mu*rho1)/(18.*pow(F12*F21-F11*F22,3)*rho*rho10);
  ay(6, 10) =  (alpha1*pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(F11*(10*pow(pow(F11,2)+pow(F12,2),2)-(4*pow(F11,2)+3*pow(F12,2))*pow(F21,2)+7*(-2+pow(F12,4))*pow(F21,4))-F12*F21*(5*pow(F11,2)+3*pow(F12,2)+7*(3+4*pow(F11,2)*pow(F12,2))*pow(F21,2))*F22+F11*(-7*pow(F21,2)+pow(F12,2)*(-1+42*pow(F11,2)*pow(F21,2)))*pow(F22,2)-7*(3+4*pow(F11,4))*F12*F21*pow(F22,3)+7*F11*(1+pow(F11,4))*pow(F22,4))*mu*rho1)/(18.*pow(F12*F21-F11*F22,3)*rho*rho10);

  // 8 row.
  ay(7,  0) =  (F21*rho1*(-u11+u12))/rho;
  ay(7,  1) =  (alpha1*F21*rho1*(u11-u12))/pow(rho,2);
  ay(7,  2) =  (alpha1*F21*(-u11+u12))/rho;
  ay(7,  3) = -((alpha1*F21*rho1)/rho);
  ay(7,  4) =  F21*(-1.0 + (alpha1*rho1)/rho);
  ay(7,  5) =  0.0;
  ay(7,  6) =  0.0;
  ay(7,  7) =  (alpha1*rho1*(u21-u22)+rho*u22)/rho;
  ay(7,  8) =  0.0;
  ay(7,  9) =  0.0;
  ay(7, 10) =  0.0;

  // 9 row.
  ay(8,  0) =  (F22*rho1*(-u11+u12))/rho;
  ay(8,  1) =  (alpha1*F22*rho1*(u11-u12))/pow(rho,2);
  ay(8,  2) =  (alpha1*F22*(-u11+u12))/rho;
  ay(8,  3) = -((alpha1*F22*rho1)/rho);
  ay(8,  4) =  F22*(-1.0+(alpha1*rho1)/rho);
  ay(8,  5) =  0.0;
  ay(8,  6) =  0.0;
  ay(8,  7) =  0.0;
  ay(8,  8) =  (alpha1*rho1*(u21-u22)+rho*u22)/rho;
  ay(8,  9) =  0.0;
  ay(8, 10) =  0.0;

  // 10 row.
  ay(9,  0) =  (F21*rho1*(-u21+u22))/rho;
  ay(9,  1) =  (alpha1*F21*rho1*(u21-u22))/pow(rho,2);
  ay(9,  2) =  (alpha1*F21*(-u21+u22))/rho;
  ay(9,  3) =  0.0;
  ay(9,  4) =  0.0;
  ay(9,  5) = -((alpha1*F21*rho1)/rho);
  ay(9,  6) =  F21*(-1.0+(alpha1*rho1)/rho);
  ay(9,  7) =  0.0;
  ay(9,  8) =  0.0;
  ay(9,  9) =  (alpha1*rho1*(u21-u22)+rho*u22)/rho;
  ay(9, 10) =  0.0;

  // 11 row.
  ay(10,  0) =  (F22*rho1*(-u21+u22))/rho;
  ay(10,  1) =  (alpha1*F22*rho1*(u21-u22))/pow(rho,2);
  ay(10,  2) =  (alpha1*F22*(-u21+u22))/rho;
  ay(10,  3) =  0.0;
  ay(10,  4) =  0.0;
  ay(10,  5) = -((alpha1*F22*rho1)/rho);
  ay(10,  6) =  F22*(-1.0+(alpha1*rho1)/rho);
  ay(10,  7) =  0.0;
  ay(10,  8) =  0.0;
  ay(10,  9) =  0.0;
  ay(10, 10) =  (alpha1*rho1*(u21 - u22) + rho*u22)/rho;

  return ay;
}

} // namespace LinearizedFluxMatrices

