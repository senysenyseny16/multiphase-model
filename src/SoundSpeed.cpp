//: SoundSpeed.cpp

#include "SoundSpeed.hpp"
#include "Definitions.hpp"
#include "Energy.hpp"

#include <cmath>
#include <algorithm>
#include <cassert>

namespace SoundSpeed {

double C1(const double rho1)
{
  return std::sqrt(Energy::dp1(rho1));
}

double C2(const double rho2)
{
  return std::sqrt(Energy::dp2(rho2));
}

double Cv(const double rho1)
{
  return std::sqrt(Physics::mu / rho1);
}

double maxC( const double rho1
           , const double rho2
           , const double c1
           , const double c2
           , const double u)
{
  const double C1Squared = Energy::dp1(rho1) + (4. / 3.) * (Physics::mu / rho1);
  const double C2Squared = Energy::dp2(rho2);

  const double C = std::sqrt(c1 * C1Squared + c2 * C2Squared);

  const double maxC = std::max(std::abs(u + C), std::abs(u - C));

  return maxC;
}

double longitudinalVelocity2Elastic(const double rho1)
{
  assert(rho1 > 0.);

  return (Energy::dp1(rho1) + (4. / 3.) * (Physics::mu / rho1));
}

double longitudinalVelocity2Liquid(const double rho2)
{
  assert(rho2 > 0.);

  return Energy::dp2(rho2);
}

double longitudinalVelocityMixture( const double rho1
                                  , const double rho2
                                  , const double c1)
{
  assert(rho1 > 0.);
  assert(rho2 > 0.);
  assert(c1 > 0. and c1 < 1.);

  return std::sqrt(c1 * longitudinalVelocity2Elastic(rho1) + (1. - c1) * longitudinalVelocity2Liquid(rho2));
}

} // namespace SoundSpeed
