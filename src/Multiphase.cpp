//: Multiphase.cpp

#include "SchemeO1.hpp"
#include "SchemeO2.hpp"
#include "SchemeO3.hpp"
#include "InitialData.hpp"
#include "Mesh.hpp"

#include "Utility.hpp"

#include <iostream>

//#include <fenv.h>

int main() {

  //feenableexcept(FE_INVALID | FE_OVERFLOW);

  std::cout << "Multiphase" << std::endl;

  // Check compatibility of boundary conditions.
  bool isCompatibleBC = Utility::checkCompatibilityBC();
  if (not isCompatibleBC) return 0;

  Mesh iData = InitialData::squareTwoStates();

  SchemeO2 scheme(iData);

  scheme.info();
  scheme.setStatusInterval(500);
  scheme.run();

  return 0;
}

