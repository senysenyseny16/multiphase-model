//: THINC.cpp

#include "THINC.hpp"
#include "Definitions.hpp"
#include "Utility.hpp"
#include "Mesh.hpp"
#include "MeshRec.hpp"
#include "MeshEdge.hpp"
#include "VariablesAdditional.hpp"
#include "InterfaceNormal.hpp"
#include "WENO.hpp"

#include <cmath>
#include <vector>

#include <iostream>

namespace THINC {

std::pair<double, double> thinc( const double qB
                               , const double qC
                               , const double qF
                               , const double beta)
{
  // Find interface location.
  const double theta = Utility::sgn(qF - qB);
  const double qMin = std::min(qB, qF);
  const double qMax = std::max(qB, qF) - qMin;

  const double C = (qC - qMin + 1e-14) / (qMax + 1e-14);
  const double B = std::exp(theta * beta * (2. * C - 1.));
  const double A = (B / std::cosh(beta) - 1.) / (std::tanh(beta));

  // Reconstruct q.
  const double qRecB = qMin + (qMax / 2.) * (1. + theta * A);
  const double qRecF = qMin + (qMax / 2.) * (1. + theta * (std::tanh(beta) + A) / (1. + A * std::tanh(beta)));

  return std::make_pair(qRecB, qRecF);
}

std::pair<double, double> thincCentered( const double qB
                                       , const double qC
                                       , const double qF
                                       , const double beta)
{
  // Find interface location.
  const double theta = Utility::sgn(qF - qB);
  const double qMin = std::min(qB, qF);
  const double qMax = std::max(qB, qF) - qMin;

  const double C = (qC - qMin + 1e-12) / (qMax + 1e-12);
  const double B = std::exp(theta * beta * (2. * C - 1.));
  const double A = (1. - B) / ((1. + B) * std::tanh(beta / 2.));

  // Reconstruct q.
  const double qRecB = qMin + (qMax / 2.) * (1. + theta * (-std::tanh(beta / 2.) - A) / (1. + A * std::tanh(beta / 2.)));
  const double qRecF = qMin + (qMax / 2.) * (1. + theta * ( std::tanh(beta / 2.) - A) / (1. - A * std::tanh(beta / 2.)));

  return std::make_pair(qRecB, qRecF);
}

std::pair<VarVector, VarVector> thinc( const Mesh& v
                                     , const size_t i
                                     , const size_t j
                                     , const Geometry::Direction direction
                                     , const double beta)
{
  VarVector recB;
  VarVector recF;

  switch (direction) {

  case (Geometry::Direction::X):

    // Reconstruct variables.
    for (size_t var = 0; var < Physics::varCount; ++var) {

      auto qRec = thinc( v(var, i - 1, j)
                       , v(var, i    , j)
                       , v(var, i + 1, j)
                       , beta);

      recB[var] = qRec.first;
      recF[var] = qRec.second;

    }

    break;

  case (Geometry::Direction::Y):

    // Reconstruct variables.
    for (size_t var = 0; var < Physics::varCount; ++var) {

      auto qRec = thinc( v(var, i, j - 1)
                       , v(var, i, j    )
                       , v(var, i, j + 1)
                       , beta);

      recB[var] = qRec.first;
      recF[var] = qRec.second;

    }

    break;

  }

  return make_pair(recB, recF);
}

std::pair<VarVector, VarVector> weno(  const Mesh& v
                                     , const size_t i
                                     , const size_t j
                                     , const Geometry::Direction direction
                                     , const THINC::RecType recType)
{
  VarVector recB;
  VarVector recF;

  switch (direction) {

  case (Geometry::Direction::X):

    switch (recType) {

    case (THINC::RecType::Constant):

      for (size_t var = 0; var < Physics::varCount; ++var) {
        recB[var] = v(var, i, j);
        recF[var] = v(var, i, j);
      }

    break;

    case (THINC::RecType::WenoThird):

      for (size_t var = 0; var < Physics::varCount; ++var) {
        WENO::weno3( v(var, i - 1, j)
                   , v(var, i    , j)
                   , v(var, i + 1, j)
                   , recB[var]
                   , recF[var]
                   );
      }

    break;

    case (THINC::RecType::WenoFifth):

      for (size_t var = 0; var < Physics::varCount; ++var) {
        WENO::weno5( v(var, i - 2, j)
                   , v(var, i - 1, j)
                   , v(var, i    , j)
                   , v(var, i + 1, j)
                   , v(var, i + 2, j)
                   , recB[var]
                   , recF[var]
                   );
      }

     break;

    }

  case (Geometry::Direction::Y):

    switch (recType) {

    case (THINC::RecType::Constant):

      for (size_t var = 0; var < Physics::varCount; ++var) {
        recB[var] = v(var, i, j);
        recF[var] = v(var, i, j);
      }

    break;

    case (THINC::RecType::WenoThird):

      for (size_t var = 0; var < Physics::varCount; ++var) {
        WENO::weno3( v(var, i, j - 1)
                   , v(var, i, j    )
                   , v(var, i, j + 1)
                   , recB[var]
                   , recF[var]
                   );
      }

    break;

    case (THINC::RecType::WenoFifth):

      for (size_t var = 0; var < Physics::varCount; ++var) {
        WENO::weno5( v(var, i, j - 2)
                   , v(var, i, j - 1)
                   , v(var, i, j    )
                   , v(var, i, j + 1)
                   , v(var, i, j + 2)
                   , recB[var]
                   , recF[var]
                   );
      }

     break;

    }

  }

  return std::make_pair(recB, recF);
}

double tbv( const double qwBF
          , const double qwFB

          , const double qtBF
          , const double qtFB

          , const double qCB
          , const double qCF)
{
  const double min1 = std::min(std::abs(qwBF - qCB) + std::abs(qCF - qwFB), std::abs(qtBF - qCB) + std::abs(qCF - qtFB));
  const double min2 = std::min(std::abs(qwBF - qCB) + std::abs(qCF - qtFB), std::abs(qtBF - qCB) + std::abs(qCF - qwFB));

  return (std::min(min1, min2));
}

void bvd( const Mesh& v
        , MeshEdge& rec
        , THINC::RecType recType)
{

  #pragma omp parallel for
  for (size_t j = Geometry::firstCell + 10; j <= Geometry::lastCellY - 10; ++j) {
    for (size_t i = Geometry::firstCell + 10; i <= Geometry::lastCellX - 10; ++i) {

      // Find interface in x-direction.
      // Check interface-cell conditions.
      const bool isInterfaceX =     v(0, i, j) > THINC::eps
                                and v(0, i, j) < (1. - THINC::eps)
                                and (v(0, i + 1, j) - v(0, i, j)) * (v(0, i, j) - v(0, i - 1, j)) > 0.;
      const bool isInterfaceY =     v(0, i, j) > THINC::eps
                                and v(0, i, j) < (1. - THINC::eps)
                                and (v(0, i, j + 1) - v(0, i, j)) * (v(0, i, j) - v(0, i, j - 1)) > 0.;

      if (isInterfaceX or isInterfaceY) {

        // Find weight of THINC-reconstruction.
        double nx = 0.;
        double ny = 0.;
        InterfaceNormal::interfaceNormalPY( v(0, i - 1, j - 1)
                                          , v(0, i    , j - 1)
                                          , v(0, i + 1, j - 1)
                                          , v(0, i - 1, j    )
                                          , v(0, i + 1, j    )
                                          , v(0, i - 1, j + 1)
                                          , v(0, i    , j + 1)
                                          , v(0, i + 1, j + 1)
                                          , nx
                                          , ny);

        // Correct beta for THINC-reconstruction.
        const double betaX = THINC::beta * std::abs(nx) + THINC::minBeta;
        const double betaY = THINC::beta * std::abs(ny) + THINC::minBeta;

        {
          // Calc reconstructions in x-direction.
          const auto thincC = thinc(v, i, j, Geometry::Direction::X, betaX);
          /* const auto wenoC  = weno (v, i, j, Geometry::Direction::X, order); */

          /* const auto thincB = thinc(v, i - 1, j, Geometry::Direction::X, betaX); */
          /* const auto wenoB  = weno (v, i - 1, j, Geometry::Direction::X, order); */

          /* const auto thincF = thinc(v, i + 1, j, Geometry::Direction::X, betaX); */
          /* const auto wenoF  = weno (v, i + 1, j, Geometry::Direction::X, order); */

          // Calc TBV.
          /* for (size_t var = 0; var < Physics::varCount; ++var) { */
          /*   /1* if (var > 2 or var < 7) continue; *1/ */

          /*   if ((v(var, i + 1, j) - v(var, i, j)) * (v(var, i, j) - v(var, i - 1, j)) > 0.) { */

          /*     const double TBVT = tbv( wenoB.second [var] */
          /*                            , wenoF.first  [var] */

          /*                            , thincB.second[var] */
          /*                            , thincF.first [var] */

          /*                            , thincC.first [var] */
          /*                            , thincC.second[var]); */

          /*     const double TBVW = tbv( wenoB.second [var] */
          /*                            , wenoF.first  [var] */

          /*                            , thincB.second[var] */
          /*                            , thincF.first [var] */

          /*                            , wenoC.first  [var] */
          /*                            , wenoC.second [var]); */


          /*     //if (TBVT < TBVW) { */
          /*       rec.dataXB()(var, i, j) = thincC.first [var]; */
          /*       rec.dataXF()(var, i, j) = thincC.second[var]; */
          /*     //} */
          /*   } */
          /* } */
          if ((v(0, i + 1, j) - v(0, i, j)) * (v(0, i, j) - v(0, i - 1, j)) > 0.) {
            rec.dataXB()(0, i, j) = thincC.first [0];
            rec.dataXF()(0, i, j) = thincC.second[0];
          }

            rec.dataXB()(2, i, j) = v(2, i, j);
            rec.dataXF()(2, i, j) = v(2, i, j);

            rec.dataXB()(1, i, j) = v(1, i, j);
            rec.dataXF()(1, i, j) = v(1, i, j);

            /* rec.dataXB()(3, i, j) = v(3, i, j); */
            /* rec.dataXB()(4, i, j) = v(4, i, j); */
            /* rec.dataXB()(5, i, j) = v(5, i, j); */
            /* rec.dataXB()(6, i, j) = v(6, i, j); */

            /* rec.dataXF()(3, i, j) = v(3, i, j); */
            /* rec.dataXF()(4, i, j) = v(4, i, j); */
            /* rec.dataXF()(5, i, j) = v(5, i, j); */
            /* rec.dataXF()(6, i, j) = v(6, i, j); */

            /* rec.dataXB()(7, i, j) = v(7, i, j); */
            /* rec.dataXB()(8, i, j) = v(8, i, j); */
            /* rec.dataXB()(9, i, j) = v(9, i, j); */
            /* rec.dataXB()(10, i, j) = v(10, i, j); */

            /* rec.dataXF()(7, i, j) = v(7, i, j); */
            /* rec.dataXF()(8, i, j) = v(8, i, j); */
            /* rec.dataXF()(9, i, j) = v(9, i, j); */
            /* rec.dataXF()(10, i, j) = v(10, i, j); */

        }

        {
          // Calc reconstructions in y-direction.
          const auto thincC = thinc(v, i, j, Geometry::Direction::Y, betaY);
          /* const auto wenoC  = weno (v, i, j, Geometry::Direction::Y, order); */

          /* const auto thincB = thinc(v, i, j - 1, Geometry::Direction::Y, betaY); */
          /* const auto wenoB  = weno (v, i, j - 1, Geometry::Direction::Y, order); */

          /* const auto thincF = thinc(v, i, j + 1, Geometry::Direction::Y, betaY); */
          /* const auto wenoF  = weno (v, i, j + 1, Geometry::Direction::Y, order); */

          // Calc TBV.
          /* for (size_t var = 0; var < Physics::varCount; ++var) { */
          /*   /1* if (var > 2 or var < 7) continue; *1/ */

          /*   if ((v(var, i, j + 1) - v(var, i, j)) * (v(var, i, j) - v(var, i, j - 1)) > 0.) { */

          /*     const double TBVT = tbv(wenoB.second [var] */
          /*                           , wenoF.first  [var] */

          /*                           , thincB.second[var] */
          /*                           , thincF.first [var] */

          /*                           , thincC.first [var] */
          /*                           , thincC.second[var]); */

          /*     const double TBVW = tbv(wenoB.second [var] */
          /*                           , wenoF.first  [var] */

          /*                           , thincB.second[var] */
          /*                           , thincF.first [var] */

          /*                           , wenoC.first  [var] */
          /*                           , wenoC.second [var]); */


          /*    //if (TBVT < TBVW) { */
          /*      rec.dataYB()(var, i, j) = thincC.first [var]; */
          /*      rec.dataYF()(var, i, j) = thincC.second[var]; */
          /*    //} */

          /*   } */
          /* } */
          if ((v(0, i, j + 1) - v(0, i, j)) * (v(0, i, j) - v(0, i, j - 1)) > 0.) {
            rec.dataYB()(0, i, j) = thincC.first [0];
            rec.dataYF()(0, i, j) = thincC.second[0];
          }

            rec.dataYB()(2, i, j) = v(2, i, j);
            rec.dataYF()(2, i, j) = v(2, i, j);

            rec.dataYB()(1, i, j) = v(1, i, j);
            rec.dataYF()(1, i, j) = v(1, i, j);

            /* rec.dataYB()(3, i, j) = v(3, i, j); */
            /* rec.dataYB()(4, i, j) = v(4, i, j); */
            /* rec.dataYB()(5, i, j) = v(5, i, j); */
            /* rec.dataYB()(6, i, j) = v(6, i, j); */

            /* rec.dataYF()(3, i, j) = v(3, i, j); */
            /* rec.dataYF()(4, i, j) = v(4, i, j); */
            /* rec.dataYF()(5, i, j) = v(5, i, j); */
            /* rec.dataYF()(6, i, j) = v(6, i, j); */

            /* rec.dataYB()(7, i, j) = v(7, i, j); */
            /* rec.dataYB()(8, i, j) = v(8, i, j); */
            /* rec.dataYB()(9, i, j) = v(9, i, j); */
            /* rec.dataYB()(10, i, j) = v(10, i, j); */

            /* rec.dataYF()(7, i, j) = v(7, i, j); */
            /* rec.dataYF()(8, i, j) = v(8, i, j); */
            /* rec.dataYF()(9, i, j) = v(9, i, j); */
            /* rec.dataYF()(10, i, j) = v(10, i, j); */

        }

      }
    }
  }
}

} // namespace THINC

