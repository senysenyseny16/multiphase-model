//: BoundaryConditionYFront.cpp

#include "BoundaryConditionYFront.hpp"

#include "Definitions.hpp"
#include "DefinitionsEigen.hpp"
#include "LinearizedFluxMatrices.hpp"
#include "UtilityEigenvectors.hpp"
#include "FluxDifferential.hpp"
#include "VariablesAdditional.hpp"
#include "Energy.hpp"
#include "ShearStress.hpp"

#include <cassert>

namespace BC {

void slipHybridYFront( const double alpha1
                     , const double rho
                     , const double rho1
                     , const double u11
                     , const double u12
                     , const double u21
                     , const double u22
                     , const double F11
                     , const double F12
                     , const double F21
                     , const double F22

                     , double& yF0
                     , double& yF1
                     , double& yF2
                     , double& yF3
                     , double& yF4
                     , double& yF5
                     , double& yF6
                     , double& yF7
                     , double& yF8
                     , double& yF9
                     , double& yF10)
{
  using namespace Physics;

  /*  y-front hybrid boundary condition implementation
   *  (based on characteristic decomposition):
   *
   *  1. Linearize system of equation on previous time-step.
   *  2. Find Riemann invariants for linearized system.
   *  3. Find 3 Riemann invariants corresponding to positive eigenvalues
   *     and form 3 equations.
   *  4. Find 5 Riemann invariants corresponding to zero eigenvalue
   *     and form 5 equations.
   *  5. Set u11, u21, u22 to zero.
   *  6. Solve linear system.
   */

  // 1. Calc linearized flux matrix Ax for characteristic decomposition.
  /* const Eigen::Matrix11 Ay(LinearizedFluxMatrices::Ay( alpha1 */
  /*                                                    , rho */
  /*                                                    , rho1 */
  /*                                                    , u11 */
  /*                                                    , u12 */
  /*                                                    , u21 */
  /*                                                    , u22 */
  /*                                                    , F11 */
  /*                                                    , F12 */
  /*                                                    , F21 */
  /*                                                    , F22)); */

  /* // 2. We need eigenvalues and eigenvectors. */
  /* const Eigen::EigenSolver11 eigenSolver11(Ay); */

  /* // @TODO: Check complex part. */
  /* const Eigen::VectorRow11 eigenValues (eigenSolver11.eigenvalues ().real()); */
  /* Eigen::Matrix11          eigenVectors(eigenSolver11.eigenvectors().real()); */

  /* const double c1 = VariablesAdditional::c1(alpha1, rho, rho1); */
  /* const double c2 = 1. - c1; */
  /* const double u2 = VariablesAdditional::u2(c1, c2, u21, u22); */


  /* if (std::abs(u2) < BC::zeroEpsSlipHybrid) { */
  /*   eigenVectors = Utility::eigenVectorsDecomp(Ay, eigenValues, eigenVectors); */
  /* } */

  /* // Calc Riemann invariants for linearized system. */
  /* const Eigen::Matrix11 riemainnInvariants(eigenVectors.colPivHouseholderQr().solve(Eigen::Matrix11::Identity())); */

  /* // We want to solve linear system: leftSide * boundaryValues = rightSide. */
  /* // Form leftSide and rightSide. */
  /* Eigen::Matrix11 leftSide = Eigen::Matrix11::Zero(11, 11); */

  /* size_t rowCounter = 0; */

  /* // 3. Get 3 Riemann invariants corresponds to positive eigenvalues. */
  /* for (size_t evalueNum = 0; evalueNum < Physics::varCount; ++evalueNum) { */
  /*   if (eigenValues(evalueNum) > BC::zeroEpsSlipHybrid) { */

  /*     leftSide.row(rowCounter++) = riemainnInvariants.row(evalueNum); */

  /*   } */
  /* } */

  /* // We must find only 3 positive eigen values. */
  /* assert(rowCounter == 3); */

  /* // 4. Get 5 Riemann invariants corresponds to zero eigen values. */
  /* for (size_t evalueNum = 0; evalueNum < Physics::varCount; ++evalueNum) { */
  /*   if (std::abs(eigenValues(evalueNum)) < BC::zeroEpsSlipHybrid) { */

  /*     leftSide.row(rowCounter++) = riemainnInvariants.row(evalueNum); */

  /*   } */
  /* } */

  /* // We must find only 5 zero eigen values */
  /* // 3 positive + 5 zero -> 8. */
  /* assert(rowCounter == 8); */

  /* // 5. Form equations for u11, u21, u22. */
  /* leftSide(8,  3) = 1.; */
  /* leftSide(9,  5) = 1.; */
  /* leftSide(10, 6) = 1.; */

  /* // Form rightside. */

  /* Eigen::VectorCol11 cell; */
  /* cell << alpha1, rho, rho1, u11, u12, u21, u22, F11, F12, F21, F22; */

  /* Eigen::VectorCol11 rightSide; */
  /* rightSide(0)  = leftSide.row(0) * cell; */
  /* rightSide(1)  = leftSide.row(1) * cell; */
  /* rightSide(2)  = leftSide.row(2) * cell; */
  /* rightSide(3)  = leftSide.row(3) * cell; */
  /* rightSide(4)  = leftSide.row(4) * cell; */
  /* rightSide(5)  = leftSide.row(5) * cell; */
  /* rightSide(6)  = leftSide.row(6) * cell; */
  /* rightSide(7)  = leftSide.row(7) * cell; */
  /* rightSide(8)  = 0.; // u11 */
  /* rightSide(9)  = 0.; // u21 */
  /* rightSide(10) = 0.; // u22 */

  /* // Rightside of equation is filled. */

  /* // 6. Solve linear system. */
  /* const Eigen::VectorCol11 bc;//(leftSide.colPivHouseholderQr().solve(rightSide)); */

  /* const double bAlpha1 = alpha1;//bc(0); */
  /* const double bRho    = rho;//bc(1); */
  /* const double bRho1   = rho1;//bc(2); */
  /* const double bU11    = 0.;//bc(3); */
  /* const double bU12    = 0.;//u12;//bc(4); */
  /* const double bU21    = 0.;//bc(5); */
  /* const double bU22    = 0.;//bc(6); */
  /* const double bF11    = F11;//bc(7); */
  /* const double bF12    = F12;//bc(8); */
  /* const double bF21    = F21;//bc(9); */
  /* const double bF22    = F22;//bc(10); */

  /* /1* const double bAlpha1 = bc(0); *1/ */
  /* /1* const double bRho    = bc(1); *1/ */
  /* /1* const double bRho1   = bc(2); *1/ */
  /* /1* const double bU11    = bc(3); *1/ */
  /* /1* const double bU12    = bc(4); *1/ */
  /* /1* const double bU21    = bc(5); *1/ */
  /* /1* const double bU22    = bc(6); *1/ */
  /* /1* const double bF11    = bc(7); *1/ */
  /* /1* const double bF12    = bc(8); *1/ */
  /* /1* const double bF21    = bc(9); *1/ */
  /* /1* const double bF22    = bc(10); *1/ */

  /* // Check solution. */
  /* assert(bAlpha1 > 0. and bAlpha1 < 1.); */
  /* assert(bRho  > 0.); */
  /* assert(bRho1 > 0.); */

  /* // Calc additional variables. */
  /* const double bAlpha2 = VariablesAdditional::alpha2(bAlpha1); */
  /* const double bRho2   = VariablesAdditional::rho2(bAlpha1, bRho, bRho1); */
  /* const double bC1     = VariablesAdditional::c1(bAlpha1, bRho, bRho1); */
  /* const double bC2     = VariablesAdditional::c2(bAlpha1, bRho, bRho1); */
  /* const double bU1     = VariablesAdditional::u1(bC1, bC2, bU11, bU12); */
  /* const double bU2     = VariablesAdditional::u2(bC1, bC2, bU21, bU22); */
  /* // Calc additional average variables. */
  /* /1* const double c1 = VariablesAdditional::c1(bAlpha1, bRho, bRho1); *1/ */
  /* /1* const double c2 = VariablesAdditional::c2(bAlpha1, bRho, bRho1); *1/ */
  /* /1* const double u2 = VariablesAdditional::u2(c1, c2, bU21, bU22); *1/ */
  /* // Calc energy and pressure. */
  /* const double bE1 = Energy::e1(bRho1, bF11, bF12, bF21, bF22); */
  /* const double bE2 = Energy::e2(bRho2); */
  /* const double bP1 = Energy::p1(bRho1); */
  /* const double bP2 = Energy::p2(bRho2); */
  /* // Calc shear stresses. */
  /* const double bSigma12 = ShearStress::sigma12(bRho1, bF11, bF12, bF21, bF22); */
  /* const double bSigma22 = ShearStress::sigma22(bRho1, bF11, bF12, bF21, bF22); */

  /* yF0  = Flux::yF0(bAlpha1, bU2); */
  /* yF1  = Flux::yF1(bRho, bU2); */
  /* yF2  = Flux::yF2(bAlpha1, bRho1, bU21); */
  /* yF3  = 0.; */
  /* yF4  = Flux::xF3(bRho1, bRho2, bU11, bU12, bU21, bU22, bE1, bE2, bP1, bP2); */
  /* yF5  = Flux::yF5(bAlpha1, bAlpha2, bRho1, bRho2, bU11, bU12, bU21, bU22, bSigma12); */
  /* yF6  = Flux::yF6(bAlpha1, bAlpha2, bRho1, bRho2, bU21, bU22, bP1,  bP2,  bSigma22); */
  /* yF7  = Flux::yF7(bRho, bF11, bF21, bU1, bU2); */
  /* yF8  = Flux::yF8(bRho, bF12, bF22, bU1, bU2); */
  /* yF9  = 0.; */
  /* yF10 = 0.; */
}

} // namespace BC

