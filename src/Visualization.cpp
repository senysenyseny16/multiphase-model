//: Visualization.cpp

#include "Visualization.hpp"
#include "Mesh.hpp"
#include "MeshRec.hpp"
#include "MeshEdgeMutual1D.hpp"
#include "MeshFluxNumerical.hpp"
#include "Definitions.hpp"

#include "VariablesAdditional.hpp"
#include "Energy.hpp"
#include "ShearStress.hpp"
#include "SoundSpeed.hpp"

#include "Utility.hpp"

namespace Visualization {

void plotMeshPhysical(const Mesh& mesh)
{
  using namespace SchemePars;

  // Make GNU Plot header.
  plotter("set terminal qt noraise size 3000, 1800");
  plotter("set autoscale fix");
  plotter("set size 1, 1");
  plotter("set size ratio 1");
  plotter("set origin 0, 0");
  plotter("set multiplot layout 2, 3");
  plotter("set title \"Physical variables\" font \",8\"");
  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");
  plotter("set palette rgbformulae 22, 13, -31");

  const bool enableTics = false;

  if (enableTics) {
    plotter("set xtics " + std::to_string(hx));
    plotter("set ytics " + std::to_string(hy));
    plotter("set grid front xtics ytics lw 1.5 lt -1 lc rgb 'white'");
  }

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      // First two columns are x and y coordinates.
      std::string dataRow( Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx / 2.)
                         + separator
                         + Utility::toStringWithPrecision((j - Geometry::cellCountGhost) * hy + hy / 2.)
                         + separator
                         );

      // Add physical variables.
      for (size_t var = 0; var < Physics::varCount; ++var) {
        dataRow.append(Utility::toStringWithPrecision(mesh(var, i, j)) + separator);
      }

      // p1 - p2.
      dataRow.append(Utility::toStringWithPrecision(   Energy::p1(mesh(2, i, j))
                                                     - Energy::p2(mesh(1, i, j))
                                                   ) + separator);

      // Push row to GNU Plot.
      plotter(dataRow);
    }
  }

  // End of dataset.
  plotter("EOD");

  // Draw graphics.
  plotter("set cbrange [0:1]");
  plotter("plot $dataset using 1:2:3 with image pixels title \"alpha1\"");
  plotter("unset cbrange");

  plotter("plot $dataset using 1:2:5 with image title \"rho1\"");
  plotter("plot $dataset using 1:2:4 with image title \"rho2\"");

  plotter("plot $dataset using 1:2:6 with image title \"u11\"");
  /* plotter("plot $dataset using 1:2:7 with image title \"u12\""); */
  plotter("plot $dataset using 1:2:8 with image title \"u21\"");
  /* plotter("plot $dataset using 1:2:9 with image title \"u22\""); */

  /* plotter("plot $dataset using 1:2:10 with image title \"F11\""); */
  /* plotter("plot $dataset using 1:2:11 with image title \"F12\""); */
  /* plotter("plot $dataset using 1:2:12 with image title \"F21\""); */
  /* plotter("plot $dataset using 1:2:13 with image title \"F22\""); */

  plotter("plot $dataset using 1:2:14 with image title \"p1-p2\"");

  plotter("unset multiplot");
}

void plotMeshPhysicalToFile( const Mesh& mesh
                           , const std::string filename)
{
  using namespace SchemePars;

  // Make GNU Plot header.
  plotter("set terminal png size 3000, 3000");
  plotter("set output '" + filename + ".png'");
  plotter("set autoscale fix");
  plotter("set size ratio 1");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set multiplot layout 3, 3");
  plotter("set title \"Physical variables\" font \",8\"");
  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");
  plotter("set palette rgbformulae 22, 13, -31");

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");
  std::string dataRow;
  dataRow.reserve(1000);

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      dataRow.clear();

      // First two columns are x and y coordinates.
      dataRow.append( Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx / 2.)
                    + separator
                    + Utility::toStringWithPrecision((j - Geometry::cellCountGhost) * hy + hy / 2.)
                    + separator
                    );

      // Add physical variables.
      for (size_t var = 0; var < Physics::varCount; ++var) {
        dataRow.append(Utility::toStringWithPrecision(mesh(var, i, j)) + separator);
        /* dataRow.append(((mesh(0, i, j) < 1e-7) ? Utility::toStringWithPrecision(mesh(var, i, j)) : "NaN") + separator); */
      }

      // p1 - p2.
      dataRow.append(Utility::toStringWithPrecision(   Energy::p1(mesh(2, i, j))
                                                     - Energy::p2(mesh(1, i, j))
                                                   ) + separator);

      const double partialDensity1 = mesh(0, i, j) * mesh(2, i, j);
      const double partialDensity2 = (1. - mesh(0, i, j)) * mesh(1, i, j);
      const double rho = partialDensity1 + partialDensity2;

      dataRow.append(Utility::toStringWithPrecision(partialDensity1) + separator);
      dataRow.append(Utility::toStringWithPrecision(partialDensity2) + separator);
      dataRow.append(Utility::toStringWithPrecision(rho) + separator);

      const double sigma11 = ShearStress::sigma11( mesh(2, i, j)
                                                 , mesh(7, i, j)
                                                 , mesh(8, i, j)
                                                 , mesh(9, i, j)
                                                 , mesh(10, i, j));

      const double Sigma11 = - ( mesh(0, i, j) * Energy::p1(mesh(2, i, j))
                               + (1. - mesh(0, i, j)) * Energy::p2(mesh(1, i, j))
                               ) + mesh(0, i, j) * sigma11;

      dataRow.append(Utility::toStringWithPrecision(Sigma11) + separator);

      const double sigma12 = ShearStress::sigma12( mesh(2, i, j)
                                                 , mesh(7, i, j)
                                                 , mesh(8, i, j)
                                                 , mesh(9, i, j)
                                                 , mesh(10, i, j));

      const double Sigma12 = mesh(0, i, j) * sigma12;

      dataRow.append(Utility::toStringWithPrecision(Sigma12) + separator);

      const double sigma22 = ShearStress::sigma22( mesh(2, i, j)
                                                 , mesh(7, i, j)
                                                 , mesh(8, i, j)
                                                 , mesh(9, i, j)
                                                 , mesh(10, i, j));

      const double Sigma22 = - ( mesh(0, i, j) * Energy::p1(mesh(2, i, j))
                               + (1. - mesh(0, i, j)) * Energy::p2(mesh(1, i, j))
                               ) + mesh(0, i, j) * sigma22;

      dataRow.append(Utility::toStringWithPrecision(Sigma22) + separator);

      dataRow.append(Utility::toStringWithPrecision(rho) + separator);

      dataRow.append(Utility::toStringWithPrecision(rho * mesh(7, i, j)) + separator);


      const double c1 = VariablesAdditional::c1(mesh(0, i, j), mesh(2, i, j), mesh(1, i, j));
      const double longVelocityMixture = SoundSpeed::longitudinalVelocityMixture(mesh(2, i, j), mesh(1, i, j), c1);

      dataRow.append(Utility::toStringWithPrecision(longVelocityMixture) + separator);

      // Push row to GNU Plot.
      plotter(dataRow);
    }
  }

  // End of dataset.
  plotter("EOD");

  // Draw graphics.
  plotter("set cbrange [0:1]");
  plotter("plot $dataset using 1:2:3 with image pixels title \"alpha1\"");
  plotter("unset cbrange");

  plotter("plot $dataset using 1:2:5 with image title \"rho1\"");
  plotter("plot $dataset using 1:2:4 with image title \"rho2\"");

  plotter("plot $dataset using 1:2:6 with image title \"u11\"");
  /* plotter("plot $dataset using 1:2:7 with image title \"u12\""); */
  plotter("plot $dataset using 1:2:8 with image title \"u21\"");
  /* plotter("plot $dataset using 1:2:9 with image title \"u22\""); */

  /* plotter("plot $dataset using 1:2:10 with image title \"F11\""); */
  /* plotter("plot $dataset using 1:2:11 with image title \"F12\""); */
  /* plotter("plot $dataset using 1:2:12 with image title \"F21\""); */
  /* plotter("plot $dataset using 1:2:13 with image title \"F22\""); */

  plotter("plot $dataset using 1:2:14 with image title \"p1-p2\"");

  /* plotter("plot $dataset using 1:2:15 with image title \"alpha1 * rho1\""); */
  /* plotter("plot $dataset using 1:2:16 with image title \"alpha2 * rho2\""); */
  /* plotter("plot $dataset using 1:2:17 with image title \"rho\""); */

  plotter("plot $dataset using 1:2:18 with image title \"Sigma11\"");
  plotter("plot $dataset using 1:2:19 with image title \"Sigma12\"");
  plotter("plot $dataset using 1:2:20 with image title \"Sigma22\"");

  /* plotter("plot $dataset using 1:2:21 with image title \"rho\""); */
  /* plotter("plot $dataset using 1:2:22 with image title \"rho * F11\""); */

  /* plotter("plot $dataset using 1:2:23 with image title \"C mixture\""); */

  plotter("unset multiplot");
}

void plotMeshEdgeMutual1D(const MeshEdgeMutual1D& m)
{
  using namespace SchemePars;

  // Make GNU Plot header.
  plotter("set terminal qt noraise size 3000, 1800");
  plotter("set autoscale fix");
  plotter("set size ratio 1");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set multiplot layout 2, 1");
  plotter("set title \"MeshEdgeMutual1D\" font \",8\"");
  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");
  plotter("set palette rgbformulae 22, 13, -31");

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY + 1; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX + 1; ++i) {

      // First two columns are x and y coordinates.
      std::string dataRow( Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx / 2.)
                         + separator
                         + Utility::toStringWithPrecision((j - Geometry::cellCountGhost) * hy + hy / 2.)
                         + separator
                         );

      // Add physical variables.
      dataRow.append(Utility::toStringWithPrecision(m.xB(i, j)) + separator);
      dataRow.append(Utility::toStringWithPrecision(m.yB(i, j)) + separator);

      // Push row to GNU Plot.
      plotter(dataRow);
    }
  }

  // End of dataset.
  plotter("EOD");

  // Draw graphics.
  plotter("plot $dataset using 1:2:3 with image pixels title \"edgesX\"");
  plotter("plot $dataset using 1:2:4 with image pixels title \"edgesY\"");

  plotter("unset multiplot");

}

void plotMeshPhysical1DX( const Mesh& mesh
                        , const size_t j)
{
  using namespace SchemePars;

  const double y = (j - Geometry::cellCountGhost) * hy + hy / 2.;

  // Make GNU Plot header.
  plotter("set terminal qt noraise size 3000, 2000");
  plotter("set autoscale fix");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set multiplot layout 10, 1");

  plotter(   "set title \"Physical variables (1D, Y = "
           + std::to_string(y)
           + ")\" font \",8\"");

  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");
  plotter("set style line 1 lt rgb \"black\" lw 2 pt 6");
  plotter("set style line 2 lt rgb \"#87CEFA\" lw 2 pt 6");
  plotter("set grid ytics lc rgb \"#bbbbbb\" lw 1 lt 0");
  plotter("set grid xtics lc rgb \"#bbbbbb\" lw 1 lt 0");

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

    // First column is x coordinate.
    std::string dataRow( Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx / 2.)
                       + separator
                       );

    // Add physical variables.
    for (size_t var = 0; var < Physics::varCount; ++var) {
      dataRow.append(Utility::toStringWithPrecision(mesh(var, i, j), 16) + separator);
    }

    const double pDiff = Energy::p1(mesh(2, i, j)) - Energy::p2(mesh(1, i, j));
    dataRow.append(Utility::toStringWithPrecision(pDiff) + separator);

    const double fSigma11 = - (mesh(0, i, j) * Energy::p1(mesh(2, i, j)) + (1. - mesh(0, i, j)) * Energy::p2(mesh(1, i, j))) + mesh(0, i, j) * ShearStress::sigma11(mesh(2, i, j), mesh(7, i, j), mesh(8, i, j), mesh(9, i, j), mesh(10, i, j));
    dataRow.append(Utility::toStringWithPrecision(fSigma11) + separator);

    const double fSigma21 =  mesh(0, i, j) * ShearStress::sigma21(mesh(2, i, j), mesh(7, i, j), mesh(8, i, j), mesh(9, i, j), mesh(10, i, j));
    dataRow.append(Utility::toStringWithPrecision(fSigma21) + separator);

    const double partialDensity1 = mesh(0, i, j) * mesh(2, i, j);
    const double partialDensity2 = (1. - mesh(0, i, j)) * mesh(1, i, j);
    const double rho = partialDensity1 + partialDensity2;

    dataRow.append(Utility::toStringWithPrecision(rho) + separator);

    dataRow.append(Utility::toStringWithPrecision(rho * mesh(7, i, j)) + separator);
    dataRow.append(Utility::toStringWithPrecision(rho * mesh(8, i, j)) + separator);
    dataRow.append(Utility::toStringWithPrecision(partialDensity1) + separator);
    dataRow.append(Utility::toStringWithPrecision(partialDensity2) + separator);
    dataRow.append(Utility::toStringWithPrecision(rho * mesh(9, i, j)) + separator);
    dataRow.append(Utility::toStringWithPrecision(rho * mesh(10, i, j)) + separator);

    const double detF = mesh(7, i, j) * mesh(10, i, j) - mesh(8, i, j) * mesh(9, i, j);

    const size_t xbBorder1 = 20 - 10;
    const size_t xfBorder1 = 20 + 10;
    const size_t ybBorder1 = Geometry::firstCell - 1;
    const size_t yfBorder1 = Geometry::meshSizeY + 1;
    const double pressure = 1e-4;
    const double eps = 1e-6;

    double alpha1 = eps;
    if (i > xbBorder1 and i < xfBorder1 and j > ybBorder1 and j < yfBorder1) {
      alpha1 = 1. - eps;
    } else {
      alpha1 = eps;
    }
    const double rho20 = Energy::rho2(pressure);
    const double rho10 = Energy::rho1(pressure);
    const double rho0  = alpha1 * rho10 + (1. - alpha1) * rho20;

    dataRow.append(Utility::toStringWithPrecision(rho - rho0 / detF) + separator);

    // Push row to GNUPlot.
    plotter(dataRow);
  }

  // End of dataset.
  plotter("EOD");

  plotter("set lmargin at screen 0.1");

  plotter("set xlabel \"cm\""); // For all graphs.

  // alpha1
  plotter("set ylabel \"-\"");
  plotter("plot $dataset using 1:($2) title \"alpha1\" with line ls 1");

  /* plotter("plot $dataset using 1:19 title \"rho1 * alpha1\" with line ls 1"); */
  /* plotter("plot $dataset using 1:20 title \"rho2 * alpha2\" with line ls 1"); */

  // rho2
  plotter("plot $dataset using 1:3 title \"density of liquid\" with line ls 1");

  // rho1
  /* plotter("set ylabel \"gram / cm3\""); */
  plotter("plot $dataset using 1:4 title \"density of porous medium\" with line ls 1");

  // u11, u12
  plotter("set ylabel \"cm / s^-5\"");
  plotter("plot $dataset using 1:5 title \"velocity of porous medium (x)\" with line ls 1,\
                $dataset using 1:6 title \"velocity of liquid (x)\"        with line ls 2");

  // u21, u22
  /* plotter("set ylabel \"cm / s^-5\""); */
  /* plotter("plot $dataset using 1:7 title \"velocity of porous medium (y)\" with line ls 1,\ */
  /*               $dataset using 1:8 title \"velocity of liquid (y)\"        with line ls 2"); */

  // F11, F22
  plotter("set ylabel \"-\"");
  plotter("plot $dataset using 1:($9)  title \"F11\" with line ls 1,\
                $dataset using 1:($12) title \"F22\" with line ls 2");

  // F12, F21
  plotter("set ylabel \"-\"");
  plotter("plot $dataset using 1:10 title \"F12\" with line ls 1,\
                $dataset using 1:11 title \"F21\" with line ls 2");

  plotter("plot $dataset using 1:13 title \"p1 - p2\" with line ls 1");

  plotter("plot $dataset using 1:14 title \"Total Stress 11\" with line ls 1");

  /* plotter("plot $dataset using 1:15 title \"Total Stress 21\" with line ls 1"); */

  plotter("plot $dataset using 1:16 title \"rho\" with line ls 1");
  plotter("plot $dataset using 1:17 title \"rho * F11\" with line ls 1");
  /* plotter("plot $dataset using 1:18 title \"rho * F12\" with line ls 1"); */

  /* plotter("plot $dataset using 1:21 title \"rho * F21\" with line ls 1"); */
  /* plotter("plot $dataset using 1:22 title \"rho * F22\" with line ls 1"); */

  /* plotter("plot $dataset using 1:23 title \"rho - rho0 / det F\" with line ls 1"); */

  plotter("unset multiplot");
}

void plotMeshPhysical1DY( const Mesh& mesh
                        , const size_t i)
{
  using namespace SchemePars;

  const double x = (i - Geometry::cellCountGhost) * hx + hx / 2.;

  // Make GNU Plot header.
  plotter("set terminal qt noraise size 3000, 2000");
  plotter("set autoscale fix");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set multiplot layout 7, 1");

  plotter(   "set title \"Physical variables (1D, X = "
           + std::to_string(x)
           + ")\" font \",8\"");


  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");
  plotter("set style line 1 lt rgb \"black\" lw 2 pt 6");
  plotter("set style line 2 lt rgb \"#87CEFA\" lw 2 pt 6");

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      //
    // First column is x coordinate.
    std::string dataRow( Utility::toStringWithPrecision((j - Geometry::cellCountGhost) * hy + hy / 2.)
                       + separator
                       );

    // Add physical variables.
    for (size_t var = 0; var < Physics::varCount; ++var) {
      dataRow.append(Utility::toStringWithPrecision(mesh(var, i, j)) + separator);
    }

    // Push row to GNUPlot.
    plotter(dataRow);
  }

  // End of dataset.
  plotter("EOD");

  plotter("set lmargin at screen 0.1");

  plotter("set xlabel \"cm\""); // For all graphs.

  // alpha1
  plotter("set ylabel \"-\"");
  plotter("plot $dataset using 1:2 title \"alpha1\" with line ls 1");

  // rho2
  plotter("set ylabel \"gram / cm3\"");
  plotter("plot $dataset using 1:3 title \"density of liquid\" with line ls 1");

  // rho1
  plotter("set ylabel \"gram / cm3\"");
  plotter("plot $dataset using 1:4 title \"density of porous medium\" with line ls 1");

  // u11, u12
  plotter("set ylabel \"cm / s^-5\"");
  plotter("plot $dataset using 1:5 title \"velocity of porous medium (x)\" with line ls 1,\
                $dataset using 1:6 title \"velocity of liquid (x)\"        with line ls 2");

  // u21, u22
  plotter("set ylabel \"cm / s^-5\"");
  plotter("plot $dataset using 1:7 title \"velocity of porous medium (y)\" with line ls 1,\
                $dataset using 1:8 title \"velocity of liquid (y)\"        with line ls 2");

  // F11 - 1, F22 - 1
  plotter("set ylabel \"-\"");
  plotter("plot $dataset using 1:($9-1)  title \"F11 - 1\" with line ls 1,\
                $dataset using 1:($12-1) title \"F22 - 1\" with line ls 2");

  // F12, F21
  plotter("set ylabel \"-\"");
  plotter("plot $dataset using 1:10 title \"F12\" with line ls 1,\
                $dataset using 1:11 title \"F21\" with line ls 2");

  plotter("unset multiplot");
}

void plotRecX( const MeshRec& rec
             , const Mesh& mesh
             , const size_t var
             , const size_t j)
{
  using namespace SchemePars;

  // Make GNU Plot header.
  plotter("set terminal qt size 2800, 1400");
  plotter("set autoscale fix");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set title \"Reconstruction\" font \",8\"");
  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");

  plotter("set style line 1 lt rgb \"#FF82AB\" lw 2 pt 6");
  plotter("set style line 2 lt rgb \"#AB82FF\" lw 2 pt 6");
  plotter("set style line 3 lt rgb \"#6495ED\" lw 2 pt 6");

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset1 << EOD");

  const std::string separator(" ");

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

    // First two columns is xB and xF coordinates of cell.
    std::string dataRow( Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx / 2.)
                       + separator
                       + Utility::toStringWithPrecision(mesh(var, i, j))
                       );

    // Push row to GNU Plot.
    plotter(dataRow);
  }

  // End of dataset.
  plotter("EOD");

  plotter("$dataset2 << EOD");
  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

    std::string recB(   Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx)
                      + separator
                      + Utility::toStringWithPrecision(rec.xB(var, i, j)) + separator);

    std::string recF(   Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx)
                      + separator
                      + Utility::toStringWithPrecision(rec.xF(var, i, j)));

    plotter(recB);
    plotter(recF);
    plotter("\n");
  }

  // End of dataset2.
  plotter("EOD");

  plotter("set lmargin at screen 0.1");

  plotter("set xlabel \"x\"");
  plotter("set ylabel \"y\"");

  plotter("plot $dataset1 using 1:2 title \"Source variable\" with line ls 2,\
                $dataset2 using 1:2 title \"Reconstruction\"  with line ls 3");
}

void plotRecY( const MeshRec& rec
             , const Mesh& mesh
             , const size_t var
             , const size_t i)
{
  using namespace SchemePars;

  // Make GNU Plot header.
  plotter("set terminal qt size 2800, 1400");
  plotter("set autoscale fix");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set title \"Reconstruction\" font \",8\"");
  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");

  plotter("set style line 1 lt rgb \"#FF82AB\" lw 2 pt 6");
  plotter("set style line 2 lt rgb \"#AB82FF\" lw 2 pt 6");
  plotter("set style line 3 lt rgb \"#6495ED\" lw 2 pt 6");

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {

    // First two columns is xB and xF coordinates of cell.
    std::string dataRow( Utility::toStringWithPrecision((j - Geometry::cellCountGhost) * hy)
                       + separator
                       + Utility::toStringWithPrecision((j - Geometry::cellCountGhost) * hy + hy / 2.)
                       + separator
                       + Utility::toStringWithPrecision((j - Geometry::cellCountGhost) * hy + hy)
                       + separator
                       );

    dataRow.append(Utility::toStringWithPrecision(rec.yB(var, i, j)) + separator);
    dataRow.append(Utility::toStringWithPrecision(mesh  (var, i, j)) + separator);
    dataRow.append(Utility::toStringWithPrecision(rec.yF(var, i, j)));

    // Push row to GNU Plot.
    plotter(dataRow);
  }

  // End of dataset.
  plotter("EOD");

  plotter("set lmargin at screen 0.1");

  plotter("set xlabel \"x\""); // For all graphs.
  plotter("set ylabel \"y\"");
  plotter("plot $dataset using 1:4 title \"Reconstruction yB\" with line ls 1,\
                $dataset using 2:5 title \"Source variable\"   with line ls 2,\
                $dataset using 3:6 title \"Reconstruction yF\" with line ls 3");
}

void plotFlux1DX( const Mesh& mB
                , const Mesh& mF
                , const size_t j)
{
  using namespace SchemePars;

  // Make GNU Plot header.
  plotter("set terminal qt noraise size 2800, 1400");
  plotter("set autoscale fix");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set multiplot layout 11, 1");
  plotter("set title \"Flux(1D, Y = const)\" font \",8\"");
  plotter("set key box opaque");

  plotter("set style line 1 lt rgb \"black\" lw 2 pt 6");
  plotter("set style line 2 lt rgb \"#87CEFA\" lw 2 pt 6");

  plotter("set xtics 0.05");
  plotter("set mxtics 2");

  plotter("set grid ytics lc rgb \"#bbbbbb\" lw 1 lt 0");
  plotter("set grid xtics lc rgb \"#bbbbbb\" lw 1 lt 0");
  plotter("set grid mxtics lc rgb \"#bbbbbb\" lw 1 lt 0");


  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    // First column is x coordinate.
    std::string dataRow( Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx)
                       + separator
                       + Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx)
                       + separator
                       );

    // Add physical variables.
    for (size_t var = 0; var < Physics::varCount; ++var) {
      dataRow.append(Utility::toStringWithPrecision(mB(var, i, j)) + separator);
      dataRow.append(Utility::toStringWithPrecision(mF(var, i, j)) + separator);
    }

    // Push row.
    plotter(dataRow);
  }

  // End of dataset.
  plotter("EOD");

  plotter("set lmargin at screen 0.1");

  plotter("set xlabel \"cm\""); // For all graphs.

  plotter("plot $dataset using 1:3 title \"B\" with line ls 1,\
                $dataset using 2:4 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:5 title \"B\" with line ls 1,\
                $dataset using 2:6 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:7 title \"B\" with line ls 1,\
                $dataset using 2:8 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:9 title \"B\" with line ls 1,\
                $dataset using 2:10 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:11 title \"B\" with line ls 1,\
                $dataset using 2:12 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:13 title \"B\" with line ls 1,\
                $dataset using 2:14 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:15 title \"B\" with line ls 1,\
                $dataset using 2:16 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:17 title \"B\" with line ls 1,\
                $dataset using 2:18 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:19 title \"B\" with line ls 1,\
                $dataset using 2:20 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:21 title \"B\" with line ls 1,\
                $dataset using 2:22 title \"F\" with line ls 2");

  plotter("plot $dataset using 1:23 title \"B\" with line ls 1,\
                $dataset using 2:24 title \"F\" with line ls 2");

  plotter("unset multiplot");
}

void plotMeshFluxNumerical(const FluxNumerical::MeshFluxNumerical& fN)
{
  using namespace SchemePars;

  // Make GNU Plot header.
  plotter("set terminal qt noraise size 3000, 1800");
  plotter("set autoscale fix");
  plotter("set size ratio 1");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set multiplot layout 2, 5");
  plotter("set title \"MeshFluxNumerical\" font \",8\"");
  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");
  plotter("set palette rgbformulae 22, 13, -31");

  plotter("set xtics " + std::to_string(hx));
  plotter("set ytics " + std::to_string(hy));
  plotter("set grid front xtics ytics lw 1.5 lt -1 lc rgb 'white'");

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      // First two columns are x and y coordinates.
      std::string dataRow( Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx / 2.)
                         + separator
                         + Utility::toStringWithPrecision((j - Geometry::cellCountGhost) * hy + hy / 2.)
                         + separator
                         );

      // Add physical variables.
      for (size_t var = 1; var < Physics::varCount; ++var) {
        const double flux = (   (fN.c.xF(var, i, j) - fN.c.xB(var, i, j))
                              + (fN.c.yF(var, i, j) - fN.c.yB(var, i, j))
                            )
                            +
                            (   (fN.nC.xF(var, i, j) - fN.nC.xB(var, i, j))
                              + (fN.nC.yF(var, i, j) - fN.nC.yB(var, i, j)));

        dataRow.append(Utility::toStringWithPrecision(flux) + separator);
      }

      // Push row to GNU Plot.
      plotter(dataRow);
    }
  }

  // End of dataset.
  plotter("EOD");

  // Draw graphics.
  plotter("plot $dataset using 1:2:3 with image title \"F1\"");
  plotter("plot $dataset using 1:2:4 with image title \"F2\"");
  plotter("plot $dataset using 1:2:5 with image title \"F3\"");
  plotter("plot $dataset using 1:2:6 with image title \"F4\"");
  plotter("plot $dataset using 1:2:7 with image title \"F5\"");
  plotter("plot $dataset using 1:2:8 with image title \"F6\"");
  plotter("plot $dataset using 1:2:9 with image title \"F7\"");
  plotter("plot $dataset using 1:2:10 with image title \"F8\"");
  plotter("plot $dataset using 1:2:11 with image title \"F9\"");
  plotter("plot $dataset using 1:2:12 with image title \"F10\"");

  plotter("unset multiplot");
}

void plotFlux1DX2( const FluxNumerical::MeshFluxNumerical& fN
                 , const size_t j)
{
  using namespace SchemePars;

  const double y = (j - Geometry::cellCountGhost) * hy + hy / 2.;

  // Make GNU Plot header.
  plotter("set terminal qt noraise size 3000, 2000");
  plotter("set autoscale fix");
  plotter("set size 1, 1");
  plotter("set origin 0, 0");
  plotter("set multiplot layout 11, 1");

  plotter(   "set title \"Flux (1D, Y = "
           + std::to_string(y)
           + ")\" font \",8\"");

  plotter("set key box opaque");
  plotter("set tics scale 0 font \",8\"");
  plotter("set style line 1 lt rgb \"#F53141\" lw 2 pt 6");
  plotter("set style line 2 lt rgb \"#DFB3E3\" lw 2 pt 6");
  plotter("set style line 3 lt rgb \"#DED1FA\" lw 2 pt 6");
  plotter("set style line 4 lt rgb \"#124AE6\" lw 2 pt 6");
  plotter("set style line 5 lt rgb \"#33DCFD\" lw 2 pt 6");
  plotter("set grid ytics lc rgb \"#bbbbbb\" lw 1 lt 0");
  plotter("set grid xtics lc rgb \"#bbbbbb\" lw 1 lt 0");

  // Prepare data.
  // "$dataset" implemented in GNU Plot version >= 5.0.
  plotter("$dataset << EOD");

  const std::string separator(" ");

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

    // First column is x coordinate.
    std::string dataRow( Utility::toStringWithPrecision((i - Geometry::cellCountGhost) * hx + hx / 2.)
                       + separator
                       );

    // Add physical variables.
    for (size_t var = 0; var < Physics::varCount; ++var) {

      const double flux = (fN.c.xF(var, i, j) - fN.c.xB(var, i, j)) + (fN.nC.xF(var, i, j) - fN.nC.xB(var, i, j));

      dataRow.append(Utility::toStringWithPrecision(flux) + separator);
    }

    // Push row to GNUPlot.
    plotter(dataRow);
  }

  // End of dataset.
  plotter("EOD");

  // Draw graphics.
  plotter("plot $dataset using 1:2 title \"F0\" with line ls 1");
  plotter("plot $dataset using 1:3 title \"F1\" with line ls 2");
  plotter("plot $dataset using 1:4 title \"F2\" with line ls 2");
  plotter("plot $dataset using 1:5 title \"F3\" with line ls 3");
  plotter("plot $dataset using 1:6 title \"F4\" with line ls 3");
  plotter("plot $dataset using 1:7 title \"F5\" with line ls 4");
  plotter("plot $dataset using 1:8 title \"F6\" with line ls 4");
  plotter("plot $dataset using 1:9 title  \"F7\"  with line ls 5");
  plotter("plot $dataset using 1:10 title \"F8\"  with line ls 5");
  plotter("plot $dataset using 1:11 title \"F9\"  with line ls 5");
  plotter("plot $dataset using 1:12 title \"F10\" with line ls 5");

  plotter("unset multiplot");
}

} // namespace Visualization

