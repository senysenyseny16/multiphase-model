//: InitialData.cpp

#include "InitialData.hpp"
#include "Energy.hpp"

#include <cmath>
#include <iostream>

namespace InitialData {

Mesh eqState()
{
  Mesh id; // initial data.

  const double pressure = 1e-4;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      id(0,  i, j) = 0.5; // alpha1
      id(1,  i, j) = Energy::rho2(pressure);
      id(2,  i, j) = Energy::rho1(pressure);
      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh squareTwoStates()
{
  Mesh id; // initial data.

  const double pressure = 1e-4;
  const double eps = 1e-3;

  /* const size_t xbBorder1 = Geometry::meshSizeX / 2 - 3 - 2; */
  /* const size_t xfBorder1 = Geometry::meshSizeX / 2 + 3 - 2; */
  /* const size_t ybBorder1 = Geometry::meshSizeY / 2 - 3; */
  /* const size_t yfBorder1 = Geometry::meshSizeY / 2 + 3; */
  const double hsize = Geometry::meshSizeX / 12;
  const size_t xbBorder1 = Geometry::meshSizeX / 2 - hsize;
  const size_t xfBorder1 = Geometry::meshSizeX / 2 + hsize;
  const size_t yhsize    = Geometry::meshSizeY / 3;
  const size_t ybBorder1 = Geometry::meshSizeY / 2 - yhsize;
  const size_t yfBorder1 = Geometry::meshSizeY / 2 + yhsize;

  const size_t xfBorder2 = xbBorder1;
  const size_t xbBorder2 = xfBorder2 - hsize * 2.;
  const size_t ybBorder2 = Geometry::meshSizeY / 2 - hsize;
  const size_t yfBorder2 = Geometry::meshSizeY / 2 + hsize;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      double alpha1 = eps;
      double u = 0.0;
      if (i > xbBorder1 and i < xfBorder1 and j > ybBorder1 and j < yfBorder1) {
        alpha1 = 1. - eps;
        /* u = 0.5; */
      } else {
        alpha1 = eps;
      }


      if (i > xbBorder2 and i <= xfBorder2 and j > ybBorder2 and j < yfBorder2) {
        alpha1 = 1. - eps;
        u = 0.5;
      }
      /* } else { */
      /*   alpha1 = eps; */
      /*   u = 0.; */
      /* } */

      id(0,  i, j) = alpha1;
      id(1,  i, j) = Energy::rho2(pressure);
      id(2,  i, j) = Energy::rho1(pressure);
      id(3,  i, j) = u;
      id(4,  i, j) = u;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;


    }
  }

  /* id(0, 4, 4) = 1. - eps; */
  /* id(3, 4, 4) = 1.; */
  /* id(4, 4, 4) = 1.; */

  return id;
}

Mesh twoEqStatesX()
{
  Mesh id; // initial data.

  const double pressure1 = 1e-4;
  const double pressure2 = 3e+0;

  const size_t bBorder = Geometry::meshSizeX / 2 - Geometry::meshSizeX / 5;
  const size_t fBorder = Geometry::meshSizeX / 2 + Geometry::meshSizeX / 5;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      id(0, i, j) = 0.5;

      const double pressure = (i < bBorder or i > fBorder) ? pressure1 : pressure2;
      const double u = (i < bBorder or i > fBorder) ? 0.0 : 0.0;

      id(1,  i, j) = Energy::rho2(pressure);
      id(2,  i, j) = Energy::rho1(pressure);

      id(3,  i, j) = u;
      id(4,  i, j) = u;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh twoEqStatesY()
{
  Mesh id; // initial data.

  const double pressure1 = 1e-4;
  const double pressure2 = 3e+0;

  const size_t bBorder = Geometry::meshSizeY / 2 - Geometry::meshSizeY / 5;
  const size_t fBorder = Geometry::meshSizeY / 2 + Geometry::meshSizeY / 5;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      id(0, i, j) = 1e-8;

      const double pressure = (j < bBorder or j > fBorder) ? pressure1 : pressure2;

      id(1,  i, j) = Energy::rho2(pressure);
      id(2,  i, j) = Energy::rho1(pressure);

      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh twoAlpha1X()
{
  Mesh id; // initial data.

  const double pressure = 1e-4;

  const double eps = 1e-6;

  const double width = Geometry::meshSizeX / 8;

  const size_t bBorder = Geometry::meshSizeX / 2 - width;
  const size_t fBorder = Geometry::meshSizeX / 2 + width;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      id(0, i, j) = (i < bBorder or i >= fBorder) ? eps : (1. - eps);
      double u = 0.0;
      /* if (i >= bBorder and i < fBorder) u = 0.8; */

      id(1,  i, j) = Energy::rho2(pressure);
      id(2,  i, j) = Energy::rho1(pressure);
      id(3,  i, j) = u;
      id(4,  i, j) = u;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh twoAlpha1Y()
{
  Mesh id; // initial data.

  const double pressure = 1e-4;

  const double eps = 1e-6;

  const double width = Geometry::meshSizeY / 8;

  const size_t bBorder = Geometry::meshSizeY / 2 - width;
  const size_t fBorder = Geometry::meshSizeY / 2 + width;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      id(0, i, j) = (j < bBorder or j >= fBorder) ? eps : (1. - eps);
      double u = 0.0;
      /* if (j >= bBorder and j < fBorder) u = 0.8; */

      id(1, i, j) = Energy::rho2(pressure);
      id(2, i, j) = Energy::rho1(pressure);

      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = u;
      id(6,  i, j) = u;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh periodicalSmoothX()
{
  Mesh id; // initial data.

  const double pressure = 1e-4;
  const double pi = 3.1415926535897932385;
  const double amp = 1e-5;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const double deviation = std::sin(2. * pi * (i - Geometry::firstCell) / Geometry::cellCountX) * amp;

      id(0, i, j) = 0.5; // alpha1

      id(1, i, j) = Energy::rho2(pressure) + deviation;
      id(2, i, j) = Energy::rho1(pressure) + deviation;

      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh periodicalSmoothXY()
{
  Mesh id; // initial data.

  const double pressure = 1e-4;
  const double pi = 3.1415926535897932385;
  const double amp = 1e-1;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const double deviation =   std::sin(2. * pi * (i - Geometry::firstCell)  / Geometry::cellCountX) * amp
                               + std::cos(2. * pi * (j - Geometry::firstCell)  / Geometry::cellCountY) * amp;

      id(0, i, j) = 0.5 + deviation; // alpha1

      id(1, i, j) = Energy::rho2(pressure) + deviation;
      id(2, i, j) = Energy::rho1(pressure) + deviation;

      id(3,  i, j) = 0. + deviation;
      id(4,  i, j) = 0. + deviation;
      id(5,  i, j) = 0. + deviation;
      id(6,  i, j) = 0. + deviation;
      id(7,  i, j) = 1. + deviation;
      id(8,  i, j) = 0. + deviation;
      id(9,  i, j) = 0. + deviation;
      id(10, i, j) = 1. + deviation;

    }
  }
  return id;
}

double rhombus( const double x
              , const double y
              , const double a
              , const double b
              , const double x0
              , const double y0)
{
  return a * (x - x0) + b * (y - y0);
}

Mesh rhombusAlpha1()
{
  Mesh id; // initial data.

  const double pressure = 1e-4;

  const double eps = 1e-4;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const bool inRhombus =   rhombus(i, j,  0.5, 0.5, Geometry::meshSizeX / 2,     Geometry::meshSizeY / 4)
                             * rhombus(i, j,  0.5, 0.5, Geometry::meshSizeX / 2, 3 * Geometry::meshSizeY / 4) < 0.
                             and
                               rhombus(i, j, -0.5, 0.5, Geometry::meshSizeX / 2, 3 * Geometry::meshSizeY / 4)
                             * rhombus(i, j, -0.5, 0.5, Geometry::meshSizeX / 2,     Geometry::meshSizeY / 4) < 0.;


      id(0,  i, j) = inRhombus ? (1. - eps) : eps;
      id(1,  i, j) = Energy::rho2(pressure);
      id(2,  i, j) = Energy::rho1(pressure);
      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh circleAlpha1()
{
  Mesh id; // initial data.

  const double centerX = Geometry::meshSizeX / 2.;
  const double centerY = Geometry::meshSizeY / 2.;

  const size_t smallerSide = std::min(Geometry::cellCountX, Geometry::cellCountY);

  const double r0 = smallerSide / 4.;

  const double pressure = 1e-4;

  const double eps = 1e-4;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const double r = std::sqrt( std::pow(std::abs(centerX - i), 2)
                                + std::pow(std::abs(centerY - j), 2)
                                );
      //if (r >= r0) {         id(0, i, j) = (1. - eps) + (-1. + 2. * eps) * std::exp(- std::pow((r-r0) / 1.2, 2) / 2);       } else {         id(0, i, j) = (1. - eps) + (-1. + 2. * eps);       }
      if (r <= r0) {         id(0, i, j) = (1. - eps) + (-1. + 2. * eps) * std::exp(- std::pow((r-r0) / 1.2, 2) / 2);       } else {         id(0, i, j) = (1. - eps) + (-1. + 2. * eps);       }
      //id(0,  i, j) = (r <= r0) ? eps : (1. - eps);
      id(1,  i, j) = Energy::rho2(pressure);
      id(2,  i, j) = Energy::rho1(pressure);
      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}


double circle( const double x
             , const double y
             , const double ccx
             , const double ccy
             , const double r0)
{
  return std::pow(std::abs(x - ccx), 2) + std::pow(std::abs(y - ccy), 2) - r0 * r0;
}

double line0( const double x
            , const double y
            , const double ccx
            , const double ccy)
{
  return (x - y) + (ccy - ccx);
}

double line1( const double x
            , const double y
            , const double ccx
            , const double ccy)
{
  return -(x + y) + (ccy + ccx);
}

Mesh circleSmoothAlpha1()
{
  Mesh id; // initial data.

  // Circle center.
  const double ccx = SchemePars::hx * Geometry::meshSizeX / 2.;
  const double ccy = SchemePars::hy * Geometry::meshSizeY / 2.;

  const size_t smallerSide = std::min(Geometry::cellCountX, Geometry::cellCountY);
  const double h = std::min(SchemePars::hx, SchemePars::hy);
  const double r0 = h * smallerSide / 4 + SchemePars::hx / 4. + SchemePars::hy / 6.;

  const double pressure = 1e-4;
  const double eps = 1e-5;

  const double hhx = SchemePars::hx / 2.;
  const double hhy = SchemePars::hy / 2.;
  const double cellS = SchemePars::hx * SchemePars::hy;

  for (size_t j = 0; j < Geometry::meshSizeY; ++j) {
    for (size_t i = 0; i < Geometry::meshSizeY; ++i) {

      const double cellX = i * SchemePars::hx;
      const double cellY = j * SchemePars::hy;

      if (     circle(cellX - hhx, cellY - hhy, ccx, ccy, r0) < 0.
           and circle(cellX + hhx, cellY - hhy, ccx, ccy, r0) < 0.
           and circle(cellX - hhx, cellY + hhy, ccx, ccy, r0) < 0.
           and circle(cellX + hhy, cellY + hhy, ccx, ccy, r0) < 0.) {

        id(0, i, j) = 1. - eps;

      } else if (     circle(cellX - hhx, cellY - hhy, ccx, ccy, r0) > 0.
                  and circle(cellX + hhx, cellY - hhy, ccx, ccy, r0) > 0.
                  and circle(cellX - hhx, cellY + hhy, ccx, ccy, r0) > 0.
                  and circle(cellX + hhy, cellY + hhy, ccx, ccy, r0) > 0.) {

        id(0, i, j) = eps;

      } else {
        if (     line0(cellX, cellY, ccx, ccy) <= 0.
             and line1(cellX, cellY, ccx, ccy) <= 0.) {

          const double xB = std::max(cellX - hhx, ccx - sqrt(std::abs(r0 * r0 - std::pow(cellY - hhy - ccy, 2))));
          const double xF = std::min(cellX + hhx, ccx + sqrt(std::abs(r0 * r0 - std::pow(cellY - hhy - ccy, 2))));

          const double s1 =   (ccy - (cellY - hhy)) * (xF - xB)
                            + 0.5 * (xF - ccx) * sqrt(std::abs(r0 * r0 - std::pow(xF - ccx, 2))) - 0.5 * r0 * r0 * std::atan((ccx - xF) / sqrt(std::abs(r0 * r0 - std::pow(xF - ccx, 2))))
                            - 0.5 * (xB - ccx) * sqrt(std::abs(r0 * r0 - std::pow(xB - ccx, 2))) + 0.5 * r0 * r0 * std::atan((ccx - xB) / sqrt(std::abs(r0 * r0 - std::pow(xB - ccx, 2))));

          const double s2 = cellS - s1;

          id(0, i, j) = (s1 * (1. - eps) + s2 * eps) / cellS;

        } else if (     line0(cellX, cellY, ccx, ccy) >= 0.
                    and line1(cellX, cellY, ccx, ccy) >= 0.) {

          const double xB = std::max(cellX - hhx, ccx - sqrt(std::abs(r0 * r0 - std::pow(cellY + hhy - ccy, 2))));
          const double xF = std::min(cellX + hhx, ccx + sqrt(std::abs(r0 * r0 - std::pow(cellY + hhy - ccy, 2))));

          const double s1 =   (cellY + hhy - ccy) * (xF - xB)
                            + 0.5 * (xF - ccx) * sqrt(std::abs(r0 * r0 - std::pow(xF - ccx, 2))) - 0.5 * r0 * r0 * std::atan((ccx - xF) / sqrt(std::abs(r0 * r0 - std::pow(xF - ccx, 2))))
                            - 0.5 * (xB - ccx) * sqrt(std::abs(r0 * r0 - std::pow(xB - ccx, 2))) + 0.5 * r0 * r0 * std::atan((ccx - xB) / sqrt(std::abs(r0 * r0 - std::pow(xB - ccx, 2))));

          const double s2 = cellS - s1;

          id(0, i, j) = (s1 * (1. - eps) + s2 * eps) / cellS;

        } else if (     line0(cellX, cellY, ccx, ccy) >= 0.
                    and line1(cellX, cellY, ccx, ccy) <= 0.) {

          const double yB = std::max(cellY - hhy, ccy - sqrt(std::abs(r0 * r0 - std::pow(cellX - hhx - ccx, 2))));
          const double yF = std::min(cellY + hhy, ccy + sqrt(std::abs(r0 * r0 - std::pow(cellX - hhx - ccx, 2))));

          const double s1 =   (ccx - (cellX - hhx)) * (yF - yB)
                            + 0.5 * (yF - ccy) * sqrt(std::abs(r0 * r0 - std::pow(yF - ccy, 2))) - 0.5 * r0 * r0 * std::atan((ccy - yF) / sqrt(std::abs(r0 * r0 - std::pow(yF - ccy, 2))))
                            - 0.5 * (yB - ccy) * sqrt(std::abs(r0 * r0 - std::pow(yB - ccy, 2))) + 0.5 * r0 * r0 * std::atan((ccy - yB) / sqrt(std::abs(r0 * r0 - std::pow(yB - ccy, 2))));

          const double s2 = cellS - s1;

          id(0, i, j) = (s1 * (1. - eps) + s2 * eps) / cellS;

        } else {

          const double yB = std::max(cellY - hhy, ccy - sqrt(std::abs(r0 * r0 - std::pow(cellX + hhx - ccx, 2))));
          const double yF = std::min(cellY + hhy, ccy + sqrt(std::abs(r0 * r0 - std::pow(cellX + hhx - ccx, 2))));

          const double s1 =   (cellX + hhx - ccx) * (yF - yB)
                            + 0.5 * (yF - ccy) * sqrt(std::abs(r0 * r0 - std::pow(yF - ccy, 2))) - 0.5 * r0 * r0 * std::atan((ccy - yF) / sqrt(std::abs(r0 * r0 - std::pow(yF - ccy, 2))))
                            - 0.5 * (yB - ccy) * sqrt(std::abs(r0 * r0 - std::pow(yB - ccy, 2))) + 0.5 * r0 * r0 * std::atan((ccy - yB) / sqrt(std::abs(r0 * r0 - std::pow(yB - ccy, 2))));

          const double s2 = cellS - s1;

          id(0, i, j) = (s1 * (1. - eps) + s2 * eps) / cellS;

        }
      }

      if (id(0, i, j) < eps     ) id(0, i, j) = eps;
      if (id(0, i, j) > 1. - eps) id(0, i, j) = 1. - eps;

      id(1,  i, j) = Energy::rho2(pressure);
      id(2,  i, j) = Energy::rho1(pressure);
      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh circlePressure()
{
  Mesh id; // initial data.

  const double centerX = Geometry::meshSizeX / 2.;
  const double centerY = Geometry::meshSizeY / 2.;

  const size_t smallerSide = std::min(Geometry::cellCountX, Geometry::cellCountY);

  const double r0 = smallerSide / 4.;

  const double pressure1 = 1e-4;
  const double pressure2 = 3e+0;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const double r = std::sqrt( std::pow(std::abs(centerX - i), 2)
                                + std::pow(std::abs(centerY - j), 2)
                                );

      id(0, i, j) = 0.5;

      const double pressure = (r >= r0) ? pressure1 : pressure2;

      id(1, i, j) = Energy::rho2(pressure);
      id(2, i, j) = Energy::rho1(pressure);

      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh ellipseSmoothAlpha1()
{
  Mesh id; // initial data.

  const double centerX = Geometry::meshSizeX / 2.;
  const double centerY = Geometry::meshSizeY / 2.;

  const double a = 3.;
  const double b = 1.;

  const double pressure = 1e-4;

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const double x2 = std::pow(std::abs(centerX - i), 2);
      const double y2 = std::pow(std::abs(centerY - j), 2);

      id(0, i, j) = 0.5 + 0.001 * std::exp(-0.005 * (x2 / a + y2 / b));

      id(1, i, j) = Energy::rho2(pressure);
      id(2, i, j) = Energy::rho1(pressure);

      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }
  return id;
}

Mesh vessel0()
{
  using namespace Geometry::Vessel0;
  Mesh id;

  const double eps = 1e-3;
  const double pressure = 1e-4;

  for (size_t j = 0 ; j < vesselCenter - vesselWidth / 2 - vesselWallWidth; ++j) {
    for (size_t i = 0; i < Geometry::meshSizeX; ++i) {
      id(0, i, j) = eps;
    }
  }

  for (size_t j = vesselCenter - vesselWidth / 2 - vesselWallWidth; j < vesselCenter - vesselWidth / 2; ++j) {
    for (size_t i = 0; i < Geometry::meshSizeX; ++i) {
      id(0, i, j) = 1. - eps;
    }
  }

  for (size_t j = vesselCenter - vesselWidth / 2; j <= vesselCenter + vesselWidth / 2; ++j) {
    for (size_t i = 0; i < Geometry::meshSizeX; ++i) {
      id(0, i, j) = eps;
    }
  }

  for (size_t j = vesselCenter + vesselWidth / 2 + 1; j <= vesselCenter + vesselWidth / 2 + vesselWallWidth; ++j) {
    for (size_t i = 0; i < Geometry::meshSizeX; ++i) {
      id(0, i, j) = 1. - eps;
    }
  }

  for (size_t j = vesselCenter + vesselWidth / 2 + vesselWallWidth + 1; j < Geometry::meshSizeY; ++j) {
    for (size_t i = 0; i < Geometry::meshSizeX; ++i) {
      id(0, i, j) = eps;
    }
  }

  for (size_t j = 0; j < Geometry::meshSizeY; ++j) {
    for (size_t i = 0; i < Geometry::meshSizeX; ++i) {

      id(1, i, j) = Energy::rho2(pressure);
      id(2, i, j) = Energy::rho1(pressure);
      id(3,  i, j) = 0.;
      id(4,  i, j) = 0.;
      id(5,  i, j) = 0.;
      id(6,  i, j) = 0.;
      id(7,  i, j) = 1.;
      id(8,  i, j) = 0.;
      id(9,  i, j) = 0.;
      id(10, i, j) = 1.;

    }
  }

  /* for (size_t j = vesselCenter - vesselWidth / 2; j <= vesselCenter + vesselWidth / 2; ++j) { */
  /*   for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) { */
  /*     id(4, i, j) = 7e-4; */
  /*   } */
  /* } */

  return id;
}

} // namespace InitialData

