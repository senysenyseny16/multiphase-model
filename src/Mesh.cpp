//: Mesh.cpp

#include "Mesh.hpp"
#include "Definitions.hpp"

#include <ostream>

Mesh::Mesh()
  : data(Physics::varCount * Geometry::meshSizeVar, 0.)
{}

Mesh::Mesh(const Mesh& other)
  : data(other.data)
{}

Mesh& Mesh::operator=(const Mesh& right)
{
  if (this == &right) {
    return *this;
  }

  data = right.data;

  return *this;
}

Mesh::~Mesh()
{}

std::ostream& operator<<(std::ostream& os, const Mesh& mesh)
{
  for (size_t var = 0; var < Physics::varCount; ++var) {
    // Print (with ghost cells) in other direction: (i, j) -> (j, i).
    for (size_t j = 0; j < Geometry::meshSizeY; ++j) {
      for (size_t i = 0; i < Geometry::meshSizeX; ++i) {
        os << mesh(var, i, j) << " ";
      }
      os << std::endl;
    }
    os << std::endl;
  }
  return os;
}

