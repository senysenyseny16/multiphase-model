//: MeshRec.cpp

#include "MeshRec.hpp"
#include "Definitions.hpp"
#include "WENO.hpp"
#include "MUSCL.hpp"

#include "Energy.hpp"
#include "VariablesAdditional.hpp"
#include <cmath>
#include <iostream>

MeshRec::MeshRec()
  : MeshEdge()
{}

void MeshRec::reconstruct( const Mesh& source
                         , const RecType recType)
{
  switch (recType) {

  case (RecType::Constant):

    // No reconstruction, only copy.
    #pragma omp parallel for
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
        for (size_t v = 0; v < Physics::varCount; ++v) {

          meshXB(v, i, j) = source(v, i, j);
          meshXF(v, i, j) = source(v, i, j);
          meshYB(v, i, j) = source(v, i, j);
          meshYF(v, i, j) = source(v, i, j);

        }
      }
    }
    break;

  case (RecType::Weno3):

    assert(Geometry::cellCountGhost >= 1);

    #pragma omp parallel for
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
        for (size_t v = 0; v < Physics::varCount; ++v) {

          // Reconstruct in x-direction.
          WENO::weno3( source(v, i - 1, j)
                     , source(v, i    , j)
                     , source(v, i + 1, j)
                     , meshXB(v, i    , j)
                     , meshXF(v, i    , j)
                     );

          // Reconstruct in y-direction.
          WENO::weno3( source(v, i, j - 1)
                     , source(v, i, j    )
                     , source(v, i, j + 1)
                     , meshYB(v, i, j    )
                     , meshYF(v, i, j    )
                     );
        }
      }
    }
    break;

  case (RecType::Weno5):

    assert(Geometry::cellCountGhost >= 2);

    #pragma omp parallel for
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
        for (size_t v = 0; v < Physics::varCount; ++v) {

          // Reconstruct in x-direction.
          WENO::weno5( source(v, i - 2, j)
                     , source(v, i - 1, j)
                     , source(v, i    , j)
                     , source(v, i + 1, j)
                     , source(v, i + 2, j)
                     , meshXB(v, i    , j)
                     , meshXF(v, i    , j)
                     );

          // Reconstruct in y-direction.
          WENO::weno5( source(v, i, j - 2)
                     , source(v, i, j - 1)
                     , source(v, i, j    )
                     , source(v, i, j + 1)
                     , source(v, i, j + 2)
                     , meshYB(v, i, j    )
                     , meshYF(v, i, j    )
                     );
        }
      }
    }
    break;

  case (RecType::Muscl2):

    assert(Geometry::cellCountGhost >= 1);

    #pragma omp parallel for
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
        for (size_t v = 0; v < Physics::varCount; ++v) {

          // Reconstruct in x-direction.
          MUSCL::MUSCL2( source(v, i - 1, j)
                       , source(v, i    , j)
                       , source(v, i + 1, j)
                       , meshXB(v, i    , j)
                       , meshXF(v, i    , j)
                       );

          // Reconstruct in y-direction.
          MUSCL::MUSCL2( source(v, i, j - 1)
                       , source(v, i, j    )
                       , source(v, i, j + 1)
                       , meshYB(v, i, j    )
                       , meshYF(v, i, j    )
                       );
        }
      }
    }
    break;
  }
}

