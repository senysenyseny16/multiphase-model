// MeshRightSide.cpp

#include "MeshRightSide.hpp"
#include "Definitions.hpp"
#include "VariablesAdditional.hpp"
#include "Derivatives.hpp"

MeshRightSide::MeshRightSide()
  : Mesh()
{ }

void MeshRightSide::calcRightSide(const Mesh& physicalVars)
{
  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      const double c1 = VariablesAdditional::c1( physicalVars(0, i, j)
                                               , physicalVars(2, i, j)
                                               , physicalVars(1, i ,j));
      const double c2 = 1. - c1;

      const double w1 = VariablesAdditional::w1( physicalVars(3, i, j)
                                               , physicalVars(4, i, j));

      const double w2 = VariablesAdditional::w2( physicalVars(5, i, j)
                                               , physicalVars(6, i, j));

      const double u1 = VariablesAdditional::u1( c1
                                               , c2
                                               , physicalVars(3, i, j)
                                               , physicalVars(4, i, j));

      const double u2 = VariablesAdditional::u2( c1
                                               , c2
                                               , physicalVars(5, i, j)
                                               , physicalVars(6, i, j));

      const double w1YB = VariablesAdditional::w1( physicalVars(3, i, j - 1)
                                                 , physicalVars(4, i, j - 1));

      const double w1YF = VariablesAdditional::w1( physicalVars(3, i, j + 1)
                                                 , physicalVars(4, i, j + 1));

      const double w2XB = VariablesAdditional::w2( physicalVars(5, i - 1, j)
                                                 , physicalVars(6, i - 1, j));

      const double w2XF = VariablesAdditional::w2( physicalVars(5, i + 1, j)
                                                 , physicalVars(6, i + 1, j));

      // General case.
      double dw1dy = Derivatives::dYS(w1YB, w1YF);

      // Boundary.
      if (j == Geometry::firstCell) dw1dy = Derivatives::dYF(w1  , w1YF);
      if (j == Geometry::lastCellY) dw1dy = Derivatives::dYB(w1YB, w1  );

      // General case.
      double dw2dx = Derivatives::dXS(w2XB, w2XF);

      // Boundary.
      if (i == Geometry::firstCell) dw2dx = Derivatives::dXF(w2  , w2XF);
      if (i == Geometry::lastCellX) dw2dx = Derivatives::dXB(w2XB, w2  );

      operator()(3, i, j) = -u2 * (dw1dy - dw2dx);
      operator()(4, i, j) = -u1 * (dw2dx - dw1dy);
    }
  }
}


