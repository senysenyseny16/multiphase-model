//: BoundaryConditions.cpp

#include "BoundaryConditions.hpp"
#include "Definitions.hpp"
#include "Mesh.hpp"
#include "MeshRec.hpp"
#include "MeshFluxNumerical.hpp"
#include "MeshNCFluxAlpha1.hpp"

#include "BoundaryConditionXBack.hpp"
#include "BoundaryConditionXFront.hpp"
#include "BoundaryConditionYBack.hpp"
#include "BoundaryConditionYFront.hpp"

#include "VariablesAdditional.hpp"
#include "FluxDifferential.hpp"
#include "Energy.hpp"
#include "ShearStress.hpp"

#include <cmath>

namespace BC {

void applyBoundaryConditions1( const Mesh& p
                             , MeshRec&    rec)
{
  // Implement x-back (left) boundary condition.
  switch (BC::boundaryCondXBack) {

  case (BC::BoundaryCondXBack::Periodic): break;
  case (BC::BoundaryCondXBack::NonReflect): break;

  case (BC::BoundaryCondXBack::Inflow):

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {

    BC::inflowXBack( p
                   , j
                   , BC::inflowPorousVelocity
                   , BC::inflowLiquidVelocity
                   , rec
                   );
  }
  break;

  case (BC::BoundaryCondXBack::VesselInflow):

  /* { */
  /* using namespace Geometry::Vessel0; */

  /* for (size_t j = Geometry::firstCell; j < vesselCenter - vesselWidth / 2; ++j) { */
  /*   for (size_t var = 0; var < Physics::varCount; ++var) { */
  /*     fluxNumerical.dataXB()(var, Geometry::firstCell, j) = fluxNumerical.dataXF()(var, Geometry::firstCell, j); */
  /*   } */
  /* } */

  /* for (size_t j = vesselCenter - vesselWidth / 2; j <= vesselCenter + vesselWidth / 2; ++j) { */

  /*   // Generate parabolic profile. */
  /*   const double r = j - static_cast<double>(vesselCenter); */
  /*   const double parabolicProfile = -pow(fabs((r) / (vesselWidth / 2.)), 2) + 1.; */
  /*   const double porousVelocity = BC::inflowPorousVelocity;//(parabolicProfile > 0.) ? (parabolicProfile * BC::inflowPorousVelocity) : 0.; */
  /*   const double liquidVelocity = BC::inflowLiquidVelocity;//(parabolicProfile > 0.) ? (parabolicProfile * BC::inflowLiquidVelocity) : 0.; */

  /*   BC::inflowXBack( physicalVars(0,  Geometry::firstCell, j) // alpha1 */
  /*                  , physicalVars(1,  Geometry::firstCell, j) // rho */
  /*                  , physicalVars(2,  Geometry::firstCell, j) // rho1 */
  /*                  , physicalVars(3,  Geometry::firstCell, j) // u11 */
  /*                  , physicalVars(4,  Geometry::firstCell, j) // u12 */
  /*                  , physicalVars(5,  Geometry::firstCell, j) // u21 */
  /*                  , physicalVars(6,  Geometry::firstCell, j) // u22 */
  /*                  , physicalVars(7,  Geometry::firstCell, j) // F11 */
  /*                  , physicalVars(8,  Geometry::firstCell, j) // F12 */
  /*                  , physicalVars(9,  Geometry::firstCell, j) // F21 */
  /*                  , physicalVars(10, Geometry::firstCell, j) // F22 */

  /*                  , porousVelocity */
  /*                  , liquidVelocity */

  /*                  , fluxNumerical.dataXB()(0,  Geometry::firstCell, j) // alpha1 */
  /*                  , fluxNumerical.dataXB()(1,  Geometry::firstCell, j) // rho */
  /*                  , fluxNumerical.dataXB()(2,  Geometry::firstCell, j) // rho1 */
  /*                  , fluxNumerical.dataXB()(3,  Geometry::firstCell, j) // u11 */
  /*                  , fluxNumerical.dataXB()(4,  Geometry::firstCell, j) // u12 */
  /*                  , fluxNumerical.dataXB()(5,  Geometry::firstCell, j) // u21 */
  /*                  , fluxNumerical.dataXB()(6,  Geometry::firstCell, j) // u22 */
  /*                  , fluxNumerical.dataXB()(7,  Geometry::firstCell, j) // F11 */
  /*                  , fluxNumerical.dataXB()(8,  Geometry::firstCell, j) // F12 */
  /*                  , fluxNumerical.dataXB()(9,  Geometry::firstCell, j) // F21 */
  /*                  , fluxNumerical.dataXB()(10, Geometry::firstCell, j) // F22 */
  /*                  ); */
  /* } */

  /* for (size_t j = vesselCenter + vesselWidth / 2 + 1; j <= Geometry::lastCellY; ++j) { */
  /*   for (size_t var = 0; var < Physics::varCount; ++var) { */
  /*     fluxNumerical.dataXB()(var, Geometry::firstCell, j) = fluxNumerical.dataXF()(var, Geometry::firstCell, j); */
  /*   } */
  /* } */
  /* } */
  break;

  case (BC::BoundaryCondXBack::VesselFixed):

  /* { */
  /* using namespace Geometry::Vessel0; */

  /* for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) { */
  /*   for (size_t var = 0; var < Physics::varCount; ++var) { */
  /*     fluxNumerical.dataXB()(var, Geometry::firstCell, j) = fluxNumerical.dataXF()(var, Geometry::firstCell, j); */
  /*   } */
  /* } */

  /* // Fix first vessel wall. */
  /* for ( size_t j =  vesselCenter - vesselWidth / 2 - vesselWallWidth - transitionLayer */
  /*     ;        j <= vesselCenter - vesselWidth / 2 + transitionLayer */
  /*     ;      ++j) { */

  /*   BC::inflowXBack( physicalVars(0,  Geometry::firstCell, j) // alpha1 */
  /*                  , physicalVars(1,  Geometry::firstCell, j) // rho */
  /*                  , physicalVars(2,  Geometry::firstCell, j) // rho1 */
  /*                  , physicalVars(3,  Geometry::firstCell, j) // u11 */
  /*                  , physicalVars(4,  Geometry::firstCell, j) // u12 */
  /*                  , physicalVars(5,  Geometry::firstCell, j) // u21 */
  /*                  , physicalVars(6,  Geometry::firstCell, j) // u22 */
  /*                  , physicalVars(7,  Geometry::firstCell, j) // F11 */
  /*                  , physicalVars(8,  Geometry::firstCell, j) // F12 */
  /*                  , physicalVars(9,  Geometry::firstCell, j) // F21 */
  /*                  , physicalVars(10, Geometry::firstCell, j) // F22 */

  /*                  , 0. // porous velocity */
  /*                  , 0. // liquid velocity */

  /*                  , fluxNumerical.dataXB()(0,  Geometry::firstCell, j) // alpha1 */
  /*                  , fluxNumerical.dataXB()(1,  Geometry::firstCell, j) // rho */
  /*                  , fluxNumerical.dataXB()(2,  Geometry::firstCell, j) // rho1 */
  /*                  , fluxNumerical.dataXB()(3,  Geometry::firstCell, j) // u11 */
  /*                  , fluxNumerical.dataXB()(4,  Geometry::firstCell, j) // u12 */
  /*                  , fluxNumerical.dataXB()(5,  Geometry::firstCell, j) // u21 */
  /*                  , fluxNumerical.dataXB()(6,  Geometry::firstCell, j) // u22 */
  /*                  , fluxNumerical.dataXB()(7,  Geometry::firstCell, j) // F11 */
  /*                  , fluxNumerical.dataXB()(8,  Geometry::firstCell, j) // F12 */
  /*                  , fluxNumerical.dataXB()(9,  Geometry::firstCell, j) // F21 */
  /*                  , fluxNumerical.dataXB()(10, Geometry::firstCell, j) // F22 */
  /*                  ); */
  /* } */

  /* // Fix second vessel wall. */
  /* for ( size_t j  = vesselCenter + vesselWidth / 2 - transitionLayer */
  /*     ;        j <= vesselCenter + vesselWidth / 2 + vesselWallWidth + transitionLayer */
  /*     ;      ++j) { */

  /*   BC::inflowXBack( physicalVars(0,  Geometry::firstCell, j) // alpha1 */
  /*                  , physicalVars(1,  Geometry::firstCell, j) // rho */
  /*                  , physicalVars(2,  Geometry::firstCell, j) // rho1 */
  /*                  , physicalVars(3,  Geometry::firstCell, j) // u11 */
  /*                  , physicalVars(4,  Geometry::firstCell, j) // u12 */
  /*                  , physicalVars(5,  Geometry::firstCell, j) // u21 */
  /*                  , physicalVars(6,  Geometry::firstCell, j) // u22 */
  /*                  , physicalVars(7,  Geometry::firstCell, j) // F11 */
  /*                  , physicalVars(8,  Geometry::firstCell, j) // F12 */
  /*                  , physicalVars(9,  Geometry::firstCell, j) // F21 */
  /*                  , physicalVars(10, Geometry::firstCell, j) // F22 */

  /*                  , 0. // porous velocity */
  /*                  , 0. // liquid velocity */

  /*                  , fluxNumerical.dataXB()(0,  Geometry::firstCell, j) // alpha1 */
  /*                  , fluxNumerical.dataXB()(1,  Geometry::firstCell, j) // rho */
  /*                  , fluxNumerical.dataXB()(2,  Geometry::firstCell, j) // rho1 */
  /*                  , fluxNumerical.dataXB()(3,  Geometry::firstCell, j) // u11 */
  /*                  , fluxNumerical.dataXB()(4,  Geometry::firstCell, j) // u12 */
  /*                  , fluxNumerical.dataXB()(5,  Geometry::firstCell, j) // u21 */
  /*                  , fluxNumerical.dataXB()(6,  Geometry::firstCell, j) // u22 */
  /*                  , fluxNumerical.dataXB()(7,  Geometry::firstCell, j) // F11 */
  /*                  , fluxNumerical.dataXB()(8,  Geometry::firstCell, j) // F12 */
  /*                  , fluxNumerical.dataXB()(9,  Geometry::firstCell, j) // F21 */
  /*                  , fluxNumerical.dataXB()(10, Geometry::firstCell, j) // F22 */
  /*                  ); */
  /* } */

  /* // Inflow. */
  /* for ( size_t j  = vesselCenter - vesselWidth / 2 + transitionLayer + 1 */
  /*     ;        j <= vesselCenter + vesselWidth / 2 - transitionLayer - 1 */
  /*     ;      ++j) { */

  /*   // Generate parabolic profile. */
  /*   const double r = j - static_cast<double>(vesselCenter); */
  /*   const double parabolicProfile = -pow(fabs((r) / ((vesselWidth - transitionLayer * 2) / 2.)), 2) + 1.; */
  /*   const double porousVelocity = (parabolicProfile > 0.) ? (parabolicProfile * BC::inflowPorousVelocity) : 0.; */
  /*   const double liquidVelocity = (parabolicProfile > 0.) ? (parabolicProfile * BC::inflowLiquidVelocity) : 0.; */

  /*   BC::inflowXBack( physicalVars(0,  Geometry::firstCell, j) // alpha1 */
  /*                  , physicalVars(1,  Geometry::firstCell, j) // rho */
  /*                  , physicalVars(2,  Geometry::firstCell, j) // rho1 */
  /*                  , physicalVars(3,  Geometry::firstCell, j) // u11 */
  /*                  , physicalVars(4,  Geometry::firstCell, j) // u12 */
  /*                  , physicalVars(5,  Geometry::firstCell, j) // u21 */
  /*                  , physicalVars(6,  Geometry::firstCell, j) // u22 */
  /*                  , physicalVars(7,  Geometry::firstCell, j) // F11 */
  /*                  , physicalVars(8,  Geometry::firstCell, j) // F12 */
  /*                  , physicalVars(9,  Geometry::firstCell, j) // F21 */
  /*                  , physicalVars(10, Geometry::firstCell, j) // F22 */

  /*                  , porousVelocity */
  /*                  , liquidVelocity */

  /*                  , fluxNumerical.dataXB()(0,  Geometry::firstCell, j) // alpha1 */
  /*                  , fluxNumerical.dataXB()(1,  Geometry::firstCell, j) // rho */
  /*                  , fluxNumerical.dataXB()(2,  Geometry::firstCell, j) // rho1 */
  /*                  , fluxNumerical.dataXB()(3,  Geometry::firstCell, j) // u11 */
  /*                  , fluxNumerical.dataXB()(4,  Geometry::firstCell, j) // u12 */
  /*                  , fluxNumerical.dataXB()(5,  Geometry::firstCell, j) // u21 */
  /*                  , fluxNumerical.dataXB()(6,  Geometry::firstCell, j) // u22 */
  /*                  , fluxNumerical.dataXB()(7,  Geometry::firstCell, j) // F11 */
  /*                  , fluxNumerical.dataXB()(8,  Geometry::firstCell, j) // F12 */
  /*                  , fluxNumerical.dataXB()(9,  Geometry::firstCell, j) // F21 */
  /*                  , fluxNumerical.dataXB()(10, Geometry::firstCell, j) // F22 */
  /*                  ); */
  /* } */
  /* } */
  break;

  }

  // Implement x-front (right) boundary condition.
  switch (BC::boundaryCondXFront) {

  case (BC::BoundaryCondXFront::Periodic): break;

  case (BC::BoundaryCondXFront::NonReflect): break;

  case (BC::BoundaryCondXFront::VesselFixed):

  /* { */
  /* using namespace Geometry::Vessel0; */

  /* for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) { */
  /*   for (size_t var = 0; var < Physics::varCount; ++var) { */
  /*     fluxNumerical.dataXF()(var, Geometry::lastCellX, j) = fluxNumerical.dataXB()(var, Geometry::lastCellX, j); */
  /*   } */
  /* } */

  /* // Fix first vessel wall. */
  /* for ( size_t j  = vesselCenter - vesselWidth / 2 - vesselWallWidth - transitionLayer */
  /*     ;        j <= vesselCenter - vesselWidth / 2 + transitionLayer */
  /*     ;      ++j) { */

  /*   BC::inflowXFront( physicalVars(0,  Geometry::lastCellY, j) // alpha1 */
  /*                   , physicalVars(1,  Geometry::lastCellY, j) // rho */
  /*                   , physicalVars(2,  Geometry::lastCellY, j) // rho1 */
  /*                   , physicalVars(3,  Geometry::lastCellY, j) // u11 */
  /*                   , physicalVars(4,  Geometry::lastCellY, j) // u12 */
  /*                   , physicalVars(5,  Geometry::lastCellY, j) // u21 */
  /*                   , physicalVars(6,  Geometry::lastCellY, j) // u22 */
  /*                   , physicalVars(7,  Geometry::lastCellY, j) // F11 */
  /*                   , physicalVars(8,  Geometry::lastCellY, j) // F12 */
  /*                   , physicalVars(9,  Geometry::lastCellY, j) // F21 */
  /*                   , physicalVars(10, Geometry::lastCellY, j) // F22 */

  /*                   , 0. // porous velocity */
  /*                   , 0. // liquid velocity */

  /*                   , fluxNumerical.dataXF()(0,  Geometry::lastCellY, j) // alpha1 */
  /*                   , fluxNumerical.dataXF()(1,  Geometry::lastCellY, j) // rho */
  /*                   , fluxNumerical.dataXF()(2,  Geometry::lastCellY, j) // rho1 */
  /*                   , fluxNumerical.dataXF()(3,  Geometry::lastCellY, j) // u11 */
  /*                   , fluxNumerical.dataXF()(4,  Geometry::lastCellY, j) // u12 */
  /*                   , fluxNumerical.dataXF()(5,  Geometry::lastCellY, j) // u21 */
  /*                   , fluxNumerical.dataXF()(6,  Geometry::lastCellY, j) // u22 */
  /*                   , fluxNumerical.dataXF()(7,  Geometry::lastCellY, j) // F11 */
  /*                   , fluxNumerical.dataXF()(8,  Geometry::lastCellY, j) // F12 */
  /*                   , fluxNumerical.dataXF()(9,  Geometry::lastCellY, j) // F21 */
  /*                   , fluxNumerical.dataXF()(10, Geometry::lastCellY, j) // F22 */
  /*                   ); */
  /* } */

  /* // Fix second vessel wall. */
  /* for ( size_t j  = vesselCenter + vesselWidth / 2 - transitionLayer */
  /*     ;        j <= vesselCenter + vesselWidth / 2 + vesselWallWidth + transitionLayer */
  /*     ;      ++j) { */

  /*   BC::inflowXFront( physicalVars(0,  Geometry::lastCellY, j) // alpha1 */
  /*                   , physicalVars(1,  Geometry::lastCellY, j) // rho */
  /*                   , physicalVars(2,  Geometry::lastCellY, j) // rho1 */
  /*                   , physicalVars(3,  Geometry::lastCellY, j) // u11 */
  /*                   , physicalVars(4,  Geometry::lastCellY, j) // u12 */
  /*                   , physicalVars(5,  Geometry::lastCellY, j) // u21 */
  /*                   , physicalVars(6,  Geometry::lastCellY, j) // u22 */
  /*                   , physicalVars(7,  Geometry::lastCellY, j) // F11 */
  /*                   , physicalVars(8,  Geometry::lastCellY, j) // F12 */
  /*                   , physicalVars(9,  Geometry::lastCellY, j) // F21 */
  /*                   , physicalVars(10, Geometry::lastCellY, j) // F22 */

  /*                   , 0. // porous velocity */
  /*                   , 0. // liquid velocity */

  /*                   , fluxNumerical.dataXF()(0,  Geometry::lastCellY, j) // alpha1 */
  /*                   , fluxNumerical.dataXF()(1,  Geometry::lastCellY, j) // rho */
  /*                   , fluxNumerical.dataXF()(2,  Geometry::lastCellY, j) // rho1 */
  /*                   , fluxNumerical.dataXF()(3,  Geometry::lastCellY, j) // u11 */
  /*                   , fluxNumerical.dataXF()(4,  Geometry::lastCellY, j) // u12 */
  /*                   , fluxNumerical.dataXF()(5,  Geometry::lastCellY, j) // u21 */
  /*                   , fluxNumerical.dataXF()(6,  Geometry::lastCellY, j) // u22 */
  /*                   , fluxNumerical.dataXF()(7,  Geometry::lastCellY, j) // F11 */
  /*                   , fluxNumerical.dataXF()(8,  Geometry::lastCellY, j) // F12 */
  /*                   , fluxNumerical.dataXF()(9,  Geometry::lastCellY, j) // F21 */
  /*                   , fluxNumerical.dataXF()(10, Geometry::lastCellY, j) // F22 */
  /*                   ); */
  /* } */
  /* } */
  break;

  }

  // Implement y-back boundary condition.
  switch (BC::boundaryCondYBack) {

  case (BC::BoundaryCondYBack::Periodic): break;

  case (BC::BoundaryCondYBack::NonReflect): break;

  case (BC::BoundaryCondYBack::Hybrid):
  /* for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) { */

  /*   BC::slipHybridYBack( physicalVars(0,  i, Geometry::firstCell) // alpha1 */
  /*                      , physicalVars(1,  i, Geometry::firstCell) // rho */
  /*                      , physicalVars(2,  i, Geometry::firstCell) // rho1 */
  /*                      , physicalVars(3,  i, Geometry::firstCell) // u11 */
  /*                      , physicalVars(4,  i, Geometry::firstCell) // u12 */
  /*                      , physicalVars(5,  i, Geometry::firstCell) // u21 */
  /*                      , physicalVars(6,  i, Geometry::firstCell) // u22 */
  /*                      , physicalVars(7,  i, Geometry::firstCell) // F11 */
  /*                      , physicalVars(8,  i, Geometry::firstCell) // F12 */
  /*                      , physicalVars(9,  i, Geometry::firstCell) // F21 */
  /*                      , physicalVars(10, i, Geometry::firstCell) // F22 */

  /*                      , fluxNumerical.dataYB()(0,  i, Geometry::firstCell) // alpha1 */
  /*                      , fluxNumerical.dataYB()(1,  i, Geometry::firstCell) // rho */
  /*                      , fluxNumerical.dataYB()(2,  i, Geometry::firstCell) // rho1 */
  /*                      , fluxNumerical.dataYB()(3,  i, Geometry::firstCell) // u11 */
  /*                      , fluxNumerical.dataYB()(4,  i, Geometry::firstCell) // u12 */
  /*                      , fluxNumerical.dataYB()(5,  i, Geometry::firstCell) // u21 */
  /*                      , fluxNumerical.dataYB()(6,  i, Geometry::firstCell) // u22 */
  /*                      , fluxNumerical.dataYB()(7,  i, Geometry::firstCell) // F11 */
  /*                      , fluxNumerical.dataYB()(8,  i, Geometry::firstCell) // F12 */
  /*                      , fluxNumerical.dataYB()(9,  i, Geometry::firstCell) // F21 */
  /*                      , fluxNumerical.dataYB()(10, i, Geometry::firstCell) // F22 */
  /*                      ); */
  /* } */
  break;

  }

  // Implement y-front boundary condition.
  switch (BC::boundaryCondYFront) {

  case (BC::BoundaryCondYFront::Periodic): break;

  case (BC::BoundaryCondYFront::NonReflect): break;

  case (BC::BoundaryCondYFront::Hybrid):
  /* for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) { */

  /*   BC::slipHybridYFront( physicalVars(0,  i, Geometry::lastCellY) // alpha1 */
  /*                       , physicalVars(1,  i, Geometry::lastCellY) // rho */
  /*                       , physicalVars(2,  i, Geometry::lastCellY) // rho1 */
  /*                       , physicalVars(3,  i, Geometry::lastCellY) // u11 */
  /*                       , physicalVars(4,  i, Geometry::lastCellY) // u12 */
  /*                       , physicalVars(5,  i, Geometry::lastCellY) // u21 */
  /*                       , physicalVars(6,  i, Geometry::lastCellY) // u22 */
  /*                       , physicalVars(7,  i, Geometry::lastCellY) // F11 */
  /*                       , physicalVars(8,  i, Geometry::lastCellY) // F12 */
  /*                       , physicalVars(9,  i, Geometry::lastCellY) // F21 */
  /*                       , physicalVars(10, i, Geometry::lastCellY) // F22 */

  /*                       , fluxNumerical.dataYF()(0,  i, Geometry::lastCellY) // alpha1 */
  /*                       , fluxNumerical.dataYF()(1,  i, Geometry::lastCellY) // rho */
  /*                       , fluxNumerical.dataYF()(2,  i, Geometry::lastCellY) // rho1 */
  /*                       , fluxNumerical.dataYF()(3,  i, Geometry::lastCellY) // u11 */
  /*                       , fluxNumerical.dataYF()(4,  i, Geometry::lastCellY) // u12 */
  /*                       , fluxNumerical.dataYF()(5,  i, Geometry::lastCellY) // u21 */
  /*                       , fluxNumerical.dataYF()(6,  i, Geometry::lastCellY) // u22 */
  /*                       , fluxNumerical.dataYF()(7,  i, Geometry::lastCellY) // F11 */
  /*                       , fluxNumerical.dataYF()(8,  i, Geometry::lastCellY) // F12 */
  /*                       , fluxNumerical.dataYF()(9,  i, Geometry::lastCellY) // F21 */
  /*                       , fluxNumerical.dataYF()(10, i, Geometry::lastCellY) // F22 */
  /*                       ); */
  /* } */
  break;

  }
}

void calcFluxByRecX( const Mesh& p
                   , const Mesh& rec
                   , FluxNumerical::MeshFluxNumerical& fN
                   , FluxNumerical::MeshNCFluxAlpha1&  fNAlpha1
                   , const size_t i
                   , const size_t j)
{
  /* const double bAlpha1 = rec(0,  i, j); */
  /* const double bRho2   = rec(1,  i, j); */
  /* const double bRho1   = rec(2,  i, j); */
  /* const double bU11    = rec(3,  i, j); */
  /* const double bU12    = rec(4,  i, j); */
  /* const double bU21    = rec(5,  i, j); */
  /* const double bU22    = rec(6,  i, j); */
  /* const double bF11    = rec(7,  i, j); */
  /* const double bF12    = rec(8,  i, j); */
  /* const double bF21    = rec(9,  i, j); */
  /* const double bF22    = rec(10, i, j); */

  /* // Calc additional variables. */
  /* const double bAlpha2 = VariablesAdditional::alpha2(bAlpha1); */
  /* const double bRho    = VariablesAdditional::rho(bAlpha1, bRho1, bRho2); */
  /* const double bC1     = VariablesAdditional::c1(bAlpha1, bRho1, bRho2); */
  /* const double bC2     = VariablesAdditional::c2(bAlpha1, bRho1, bRho2); */
  /* const double bU1     = VariablesAdditional::u1(bC1, bC2, bU11, bU12); */
  /* const double bU2     = VariablesAdditional::u2(bC1, bC2, bU21, bU22); */

  /* const double c1      = VariablesAdditional::c1( p(0, i, j) */
  /*                                               , p(2, i, j) */
  /*                                               , p(1, i, j)); */
  /* const double c2      = 1. - c1; */
  /* const double u1      = VariablesAdditional::u1( c1, c2 */
  /*                                               , p(3, i, j) */
  /*                                               , p(4, i, j)); */

  /* // Calc energy and pressure. */
  /* const double bE1 = Energy::e1(bRho1, bF11, bF12, bF21, bF22); */
  /* const double bE2 = Energy::e2(bRho2); */
  /* const double bP1 = Energy::p1(bRho1); */
  /* const double bP2 = Energy::p2(bRho2); */
  /* // Calc shear stresses. */
  /* const double bSigma11 = ShearStress::sigma11(bRho1, bF11, bF12, bF21, bF22); */
  /* const double bSigma21 = ShearStress::sigma12(bRho1, bF11, bF12, bF21, bF22); */

  /* // Calc "nonconservative flux". */
  /* fNAlpha1.xB(i, j)  = u1 * bAlpha1; */

  /* // Calc flux. */
  /* fN.edgeX(1,  i, j) = Flux::xF1(bAlpha1, bRho2, bU12); */
  /* fN.edgeX(2,  i, j) = Flux::xF2(bAlpha1, bRho1, bU11); */
  /* fN.edgeX(3,  i, j) = Flux::xF3(bRho1, bRho2, bU11, bU12, bU21, bU22, bE1, bE2, bP1, bP2); */
  /* fN.edgeX(4,  i, j) = 0.; */
  /* fN.edgeX(5,  i, j) = Flux::xF5(bAlpha1, bAlpha2, bRho1, bRho2, bU11, bU12, bP1,  bP2,  bSigma11); */
  /* fN.edgeX(6,  i, j) = Flux::xF6(bAlpha1, bAlpha2, bRho1, bRho2, bU11, bU12, bU21, bU22, bSigma21); */
  /* fN.edgeX(7,  i, j) = 0.; */
  /* fN.edgeX(8,  i, j) = 0.; */
  /* fN.edgeX(9,  i, j) = Flux::xF9 (bRho, bF11, bF21, bU1, bU2); */
  /* fN.edgeX(10, i, j) = Flux::xF10(bRho, bF12, bF22, bU1, bU2); */
}

void applyBoundaryConditions2( const Mesh& p
                             , const MeshRec& rec
                             , FluxNumerical::MeshFluxNumerical& fluxNum)
{

  // Implement x-back (left) boundary condition.
  switch (BC::boundaryCondXBack) {

  case (BC::BoundaryCondXBack::Periodic): break;

  case (BC::BoundaryCondXBack::NonReflect):

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t var = 0; var < Physics::varCount; ++var) {
      fluxNum.c.xB(var, Geometry::firstCell, j) = fluxNum.c.xF(var, Geometry::firstCell, j);
    }
  }

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    fluxNum.nC.xB(0, Geometry::firstCell, j) = fluxNum.nC.xF(0, Geometry::firstCell, j);
  }
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t var = 7; var < Physics::varCount; ++var) {
      fluxNum.nC.xB(var, Geometry::firstCell, j) = fluxNum.nC.xF(var, Geometry::firstCell, j);
    }
  }
  break;

/*   case (BC::BoundaryCondXBack::Inflow): */
/*   { */
/*     for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) { */
/*       calcFluxByRecX(p, rec.dataXB(), fluxNum, fluxNumAlpha1, Geometry::firstCell, j); */
/*     } */
/*   } */
/*   break; */
/*   case (BC::BoundaryCondXBack::VesselInflow): break; */
/*   case (BC::BoundaryCondXBack::VesselFixed): break; */
  }

  // Implement x-front (right) boundary condition.
  switch (BC::boundaryCondXFront) {

  case (BC::BoundaryCondXFront::Periodic): break;

  case (BC::BoundaryCondXFront::NonReflect):

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t var = 0; var < Physics::varCount; ++var) {
      fluxNum.c.xF(var, Geometry::lastCellX, j) = fluxNum.c.xB(var, Geometry::lastCellX, j);
    }
  }

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    fluxNum.nC.xF(0, Geometry::lastCellX, j) = fluxNum.nC.xB(0, Geometry::lastCellX, j);
  }
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t var = 7; var < Physics::varCount; ++var) {
      fluxNum.nC.xF(var, Geometry::lastCellX, j) = fluxNum.nC.xB(var, Geometry::lastCellX, j);
    }
  }
  break;

/*   case (BC::BoundaryCondXFront::VesselFixed): break; */
  }

  // Implement y-back boundary condition.
  switch (BC::boundaryCondYBack) {

  case (BC::BoundaryCondYBack::Periodic): break;

  case (BC::BoundaryCondYBack::NonReflect):

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    for (size_t var = 0; var < Physics::varCount; ++var) {
      fluxNum.c.yB(var, i, Geometry::firstCell) = fluxNum.c.yF(var, i, Geometry::firstCell);
    }
  }

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    fluxNum.nC.yB(0, i, Geometry::firstCell) = fluxNum.nC.yF(0, i, Geometry::firstCell);
  }
  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    for (size_t var = 7; var < Physics::varCount; ++var) {
      fluxNum.nC.yB(var, i, Geometry::firstCell) = fluxNum.nC.yF(var, i, Geometry::firstCell);
    }
  }
  break;

/*   case (BC::BoundaryCondYBack::Hybrid): break; */
  }

  // Implement y-front boundary condition.
  switch (BC::boundaryCondYFront) {

  case (BC::BoundaryCondYFront::Periodic): break;

  case (BC::BoundaryCondYFront::NonReflect):

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    for (size_t var = 0; var < Physics::varCount; ++var) {
      fluxNum.c.yF(var, i, Geometry::lastCellY) = fluxNum.c.yB(var, i, Geometry::lastCellY);
    }
  }

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    fluxNum.nC.yF(0, i, Geometry::lastCellY) = fluxNum.nC.yB(0, i, Geometry::lastCellY);
  }
  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    for (size_t var = 7; var < Physics::varCount; ++var) {
      fluxNum.nC.yF(var, i, Geometry::lastCellY) = fluxNum.nC.yB(var, i, Geometry::lastCellY);
    }
  }
  break;

/*   case (BC::BoundaryCondYFront::Hybrid): break; */
  }
}

} // namespace BC

