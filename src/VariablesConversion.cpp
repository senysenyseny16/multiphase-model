//: VariablesConversion.cpp

#include "VariablesConversion.hpp"

#include <cassert>

namespace VariablesConversion {

double alpha1(const double U0)
{
  assert(U0 > 0. and U0 < 1.);

  return U0;
}

double rho2( const double U0
           , const double U1)
{
  assert(U0 > 0. and U0 < 1.);
  assert(U1 > 0.);

  const double alpha2 = 1. - U0;
  const double rho2 = U1 / alpha2;

  assert(rho2 > 0.);

  return rho2;
}

double rho1( const double U0
           , const double U2)
{
  assert(U0 > 0. and U0 < 1.);
  assert(U2 > 0.);

  const double rho1 = U2 / U0;

  assert (rho1 > 0.);

  return rho1;
}

double u11( const double U1
          , const double U2
          , const double U3
          , const double U5)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return (U1 * U3 + U5) / (U1 + U2);
}

double u12( const double U1
          , const double U2
          , const double U3
          , const double U5)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return (U5 - U2 * U3) / (U1 + U2);

}

double u21( const double U1
          , const double U2
          , const double U4
          , const double U6)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return (U1 * U4 + U6) / (U1 + U2);
}

double u22( const double U1
          , const double U2
          , const double U4
          , const double U6)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return (U6 - U2 * U4) / (U1 + U2);
}

double F11( const double U1
          , const double U2
          , const double U7)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return U7 / (U1 + U2);
}

double F12( const double U1
          , const double U2
          , const double U8)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return U8 / (U1 + U2);
}

double F21( const double U1
          , const double U2
          , const double U9)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return U9 / (U1 + U2);
}

double F22( const double U1
          , const double U2
          , const double U10)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return U10 / (U1 + U2);
}

double alpha2(const double U0)
{
  assert(U0 > 0. and U0 < 1.);

  const double alpha2 = 1. - U0;

  assert(alpha2 > 0. and alpha2 < 1.);

  return alpha2;
}

double rho( const double U1
          , const double U2)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  const double rho = U1 + U2;

  assert(rho > 0.);

  return rho;
}

double w1(const double U3)
{
  return U3;
}

double w2(const double U4)
{
  return U4;
}

double c1( const double U1
         , const double U2)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  const double c1 = U2 / (U1 + U2);

  assert(c1 > 0. and c1 < 1.);

  return c1;
}

double c2( const double U1
         , const double U2)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  const double c2 = U1 / (U1 + U2);

  assert(c2 > 0. and c2 < 1.);

  return c2;
}

double u1( const double U1
         , const double U2
         , const double U5)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return U5 / (U1 + U2);
}

double u2( const double U1
         , const double U2
         , const double U6)
{
  assert(U1 > 0.);
  assert(U2 > 0.);

  return U6 / (U1 + U2);
}

double U0(const double alpha1)
{
  assert(alpha1 > 0. and alpha1 < 1.);

  return alpha1;
}

double U1( const double alpha1
         , const double rho2)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho2 > 0.);

  const double U1 = rho2 * (1. - alpha1);

  assert(U1 > 0.);

  return U1;
}

double U2( const double alpha1
         , const double rho1)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);

  const double U2 = rho1 * alpha1;

  assert(U2 > 0.);

  return U2;
}

double U3( const double u11
         , const double u12)
{
  return (u11 - u12);
}

double U4( const double u21
         , const double u22)
{
  return (u21 - u22);
}

double U5( const double alpha1
         , const double rho1
         , const double rho2
         , const double u11
         , const double u12)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  return ((alpha1 *  u11) * rho1 + ((1. - alpha1) * u12) * rho2);
  /* return (alpha1 *  (u11 * rho1) + (1. - alpha1) * (u12 * rho2)); */
}

double U6( const double alpha1
         , const double rho1
         , const double rho2
         , const double u21
         , const double u22)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  return ((alpha1 * u21) * rho1 + ((1. - alpha1) * u22) * rho2);
  /* return ((alpha1 * u21) * rho1 + ((1. - alpha1) * u22) * rho2); */
}

double U7( const double alpha1
         , const double rho1
         , const double rho2
         , const double F11)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  const double rho = alpha1 * rho1 + (1. - alpha1) * rho2;

  assert(rho > 0.);

  return (rho * F11);
}

double U8( const double alpha1
         , const double rho1
         , const double rho2
         , const double F12)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  const double rho = alpha1 * rho1 + (1. - alpha1) * rho2;

  assert(rho > 0.);

  return (rho * F12);
}

double U9( const double alpha1
         , const double rho1
         , const double rho2
         , const double F21)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  const double rho = alpha1 * rho1 + (1. - alpha1) * rho2;

  assert(rho > 0.);

  return (rho * F21);
}


double U10( const double alpha1
          , const double rho1
          , const double rho2
          , const double F22)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  const double rho = alpha1 * rho1 + (1. - alpha1) * rho2;

  assert(rho > 0.);

  return (rho * F22);
}

} // namespace VariablesConversion

