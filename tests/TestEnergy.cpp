//: TestEnergy.cpp

#include "catch.hpp"

#include "Energy.hpp"

SCENARIO("Energy of liquid must satisfy certain properties", "[Energy]")
{
  GIVEN("A state parameters") {

    const double C     = 3.2782233;
    const double rho   = 2.1000001;
    const double rho0  = 1.9888812;
    const double p0    = 0.0009922;

    WHEN("gamma = 2, C = 1") {
      THEN("Energy of liquid equals to (rho / rho0) / 2 + (rho0 - 2 * p0) / (2 * rho)") {
        REQUIRE(    Energy::liquid(1., 2., rho, rho0, p0)
                 == Approx((rho / rho0) / 2. + (rho0 - 2. * p0) / (2. * rho)).epsilon(1e-16)
               );
      }
    }

    WHEN("gamma = 2, rho = 1, rho0 = 1") {
      THEN("Energy of liquid equals to (2 * C ^ 2 - 2 * p0) / 2") {
        REQUIRE(Energy::liquid(C, 2., 1., 1., p0) == Approx((2. * C * C - 2. * p0) / 2.).epsilon(1e-16));
      }
    }
  }
}

SCENARIO("Energy of solid must satisfy certain properties", "[Energy]")
{
  GIVEN("A state parameters") {

    const double mu   =  4.32;
    const double rho0 =  9.98234661;
    const double F11  =  0.19999999;
    const double F12  = -101.000891;
    const double F21  =  11.2;
    const double F22  =  3.6;

    WHEN("Tensor F is identical") {
      THEN("Energy of solid equals to 0") {
        REQUIRE(Energy::solid(mu, rho0, 1., 0., 0., 1.) == Approx(0.).epsilon(1e-16));
      }
    }

    WHEN("mu = 0") {
      THEN("Energy of solid equals to 0") {
        REQUIRE(Energy::solid(0., rho0, F11, F12, F21, F22) == Approx(0.).epsilon(1e-16));
      }
    }
  }
}

SCENARIO("Pressure must satisfy certain properties", "[Energy]")
{
  GIVEN("A state parameters") {

    const double rho   = 2.1000001;
    const double p0    = 0.0009922;

    WHEN("C = 1, gamma = 2, rho0 = 1") {
      THEN("Pressure equals to (rho ^ 2 - 1 + 2 * p0) / 2") {
        REQUIRE(Energy::pressure(1., 2., rho, 1., p0) == Approx((rho * rho - 1. + 2. * p0) / 2.).epsilon(1e-16));
      }
    }
  }
}

SCENARIO("Pressure derivative must satisfy certain properties", "[Energy]")
{
  GIVEN("A state parameters") {

    const double C     = 3.2782233;
    const double gamma = 1.4993873;
    const double rho   = 2.1000001;
    const double rho0  = 1.9888812;

    WHEN("C = 0") {
      THEN("dp = 0") {
        REQUIRE(Energy::dp(0., gamma, rho, rho0) == Approx(0.).epsilon(1e-16));
      }
    }

    WHEN("gamma = 1") {
      THEN("dp = C ^ 2") {
        REQUIRE(Energy::dp(C, 1., rho, rho0) == Approx(C * C).epsilon(1e-16));
      }
    }

    WHEN("rho = 0") {
      THEN("dp = 0") {
        REQUIRE(Energy::dp(C, gamma, 0., rho0) == Approx(0.).epsilon(1e-16));
      }
    }

    WHEN("We use given parameters") {
      THEN("dp = 11.042510876847030089470219492524") {
        REQUIRE(Energy::dp(C, gamma, rho, rho0) == Approx(11.042510876847030089470219492524).epsilon(1e-16));
      }
    }
  }
}

SCENARIO("Rho must satisfy certain properties", "[Energy]")
{
  GIVEN("A state parameters") {

    const double C     = 3.2782233;
    const double rho0  = 12.333333;
    const double p0    = 2.1000001;
    const double p     = 1.9888812;

    WHEN("gamma = 1, C = 1") {
      THEN("rho = p - p0 + rho0") {
        REQUIRE(Energy::rho(1., 1., rho0, p0, p) == Approx(p - p0 + rho0).epsilon(1e-16));
      }
    }

    WHEN("gamma = 1") {
      THEN("rho = (p - p0 + C ^ 2 * rho0) / C ^ 2") {
        REQUIRE(Energy::rho(C, 1., rho0, p0, p) == Approx((p - p0 + C * C * rho0) / (C * C)).epsilon(1e-16));
      }
    }
  }
}

