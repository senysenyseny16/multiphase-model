//: TestVariablesAdditional.cpp

#include "catch.hpp"

#include "VariablesAdditional.hpp"

TEST_CASE("Additional variables test", "[VariablesAdditional]")
{
  const double alpha1 =  0.488277273;
  const double rho    =  2.128849444;
  const double rho1   =  1.773295382;
  const double u11    =  9.288339927;
  const double u12    =  0.882828282;
  const double u21    =  1.466373772;
  const double u22    = -1.884673621;

  const double alpha2 = VariablesAdditional::alpha2(alpha1);
  const double rho2   = VariablesAdditional::rho2  (alpha1, rho, rho1);
  const double c1     = VariablesAdditional::c1    (alpha1, rho, rho1);
  const double c2     = VariablesAdditional::c2    (alpha1, rho, rho1);
  const double u1     = VariablesAdditional::u1    (c1, c2, u11, u12);
  const double u2     = VariablesAdditional::u2    (c1, c2, u21, u22);

  REQUIRE(alpha2 == Approx(0.511722727).epsilon(1e-10));
  REQUIRE(rho2   == Approx(2.46811318711).epsilon(1e-10));
  REQUIRE(c1     == Approx(0.40672666438991882321199976788965).epsilon(1e-16));
  REQUIRE(c2     == Approx(0.59327333561008117678800023211035).epsilon(1e-16));
  REQUIRE(u1     == Approx(4.30157399586).epsilon(1e-10));
  REQUIRE(u2     == Approx(-0.52171329263).epsilon(1e-10));
}

