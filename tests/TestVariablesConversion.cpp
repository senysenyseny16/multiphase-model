//: TestVariablesConversion.cpp

#include "catch.hpp"

#include "VariablesConversion.hpp"

TEST_CASE("Primitive -> Conservative -> Primitive = identity mapping", "[VariablesConversion]")
{
  const double alpha1 = 0.2 + 1e-14;
  const double rho    = 1.2 + 3e-14;
  const double rho1   = 0.3 + 1e-14;
  const double u11    = 1e-3 - 7e-14;
  const double u12    = 3e-3 + 4e-14;
  const double u21    = 2e-2 + 9e-14;
  const double u22    = 1e-3 - 3e-14;
  const double F11    = 1e-7 - 2e-14;
  const double F12    = 1e-2 + 6e-14;
  const double F21    = 1e-1 + 1e-14;
  const double F22    = 1e-4 - 7e-14;

  const double U0  = VariablesConversion::U0 (alpha1);
  const double U1  = VariablesConversion::U1 (rho);
  const double U2  = VariablesConversion::U2 (rho1, alpha1);
  const double U3  = VariablesConversion::U3 (u11, u12);
  const double U4  = VariablesConversion::U4 (u21, u22);
  const double U5  = VariablesConversion::U5 (alpha1, rho, rho1, u11, u12);
  const double U6  = VariablesConversion::U6 (alpha1, rho, rho1, u21, u22);
  const double U7  = VariablesConversion::U7 (rho, F11);
  const double U8  = VariablesConversion::U8 (rho, F12);
  const double U9  = VariablesConversion::U9 (rho, F21);
  const double U10 = VariablesConversion::U10(rho, F22);

  const double alpha1R = VariablesConversion::alpha1(U0);
  const double rhoR    = VariablesConversion::rho(U1);
  const double rho1R   = VariablesConversion::rho1(U0, U2);
  const double u11R    = VariablesConversion::u11(U1, U2, U3, U5);
  const double u12R    = VariablesConversion::u12(U1, U2, U3, U5);
  const double u21R    = VariablesConversion::u21(U1, U2, U4, U6);
  const double u22R    = VariablesConversion::u22(U1, U2, U4, U6);
  const double F11R    = VariablesConversion::F11(U1, U7);
  const double F12R    = VariablesConversion::F12(U1, U8);
  const double F21R    = VariablesConversion::F21(U1, U9);
  const double F22R    = VariablesConversion::F22(U1, U10);

  REQUIRE(alpha1 == Approx(alpha1R).epsilon(1e-16));
  REQUIRE(rho    == Approx(rhoR).epsilon(1e-16));
  REQUIRE(rho1   == Approx(rho1R).epsilon(1e-16));
  REQUIRE(u11    == Approx(u11R).epsilon(1e-16));
  REQUIRE(u12    == Approx(u12R).epsilon(1e-16));
  REQUIRE(u21    == Approx(u21R).epsilon(1e-16));
  REQUIRE(u22    == Approx(u22R).epsilon(1e-15));
  REQUIRE(F11    == Approx(F11R).epsilon(1e-16));
  REQUIRE(F12    == Approx(F12R).epsilon(1e-16));
  REQUIRE(F21    == Approx(F21R).epsilon(1e-16));
  REQUIRE(F22    == Approx(F22R).epsilon(1e-16));
}

TEST_CASE("alpha2 is eq (1 - alpha1)", "[VariablesConversion]")
{
  const double alpha1 = 0.7651299;

  const double U0 = VariablesConversion::U0(alpha1);

  const double alpha2 = VariablesConversion::alpha2(U0);

  REQUIRE(alpha2 == Approx(1. - alpha1).epsilon(1e-16));
}

TEST_CASE("rho2 conversion", "[VariablesConversion]")
{
  const double alpha1 = 0.86625211;
  const double rho    = 2.37484837;
  const double rho1   = 1.27738399;

  const double U0 = VariablesConversion::U0(alpha1);
  const double U1 = VariablesConversion::U1(rho);
  const double U2 = VariablesConversion::U2(rho1, alpha1);

  const double rho2 = VariablesConversion::rho2(U0, U1, U2);

  REQUIRE(rho2 == Approx((rho - alpha1 * rho1) / (1. - alpha1)).epsilon(1e-15));
}

TEST_CASE("mass fraction (c1, c2) conversion", "[VariablesConversion]")
{
  const double alpha1 = 0.86625211;
  const double rho    = 2.37484837;
  const double rho1   = 1.27738399;

  const double U1 = VariablesConversion::U1(rho);
  const double U2 = VariablesConversion::U2(rho1, alpha1);

  const double c1 = VariablesConversion::c1(U1, U2);
  const double c2 = VariablesConversion::c2(U1, U2);

  REQUIRE(c1 == Approx(alpha1 * rho1 / rho));
  REQUIRE(c2 == Approx((rho - alpha1 * rho1) / rho));

  REQUIRE(c1 + c2 == Approx(1.).epsilon(1e-16));
}

TEST_CASE("relative velocities (w1, w2) conversion", "[VariablesConversion]")
{
  const double alpha1 =  0.86625211;
  const double rho    =  2.37484837;
  const double rho1   =  1.27738399;
  const double u11    = -1.66755556;
  const double u12    =  2.00018766;
  const double u21    =  0.33333333;
  const double u22    = -70.9383838;

  const double U1 = VariablesConversion::U1(rho);
  const double U2 = VariablesConversion::U2(rho1, alpha1);
  const double U3 = VariablesConversion::U3(u11, u12);
  const double U4 = VariablesConversion::U4(u21, u22);
  const double U5 = VariablesConversion::U5(alpha1, rho, rho1, u11, u12);
  const double U6 = VariablesConversion::U6(alpha1, rho, rho1, u21, u22);

  const double w1 = VariablesConversion::w1(U1, U2, U3, U5);
  const double w2 = VariablesConversion::w2(U1, U2, U4, U6);

  REQUIRE(w1 == Approx(u11 - u12).epsilon(1e-16));
  REQUIRE(w2 == Approx(u21 - u22).epsilon(1e-16));
}

TEST_CASE("mixture velocities (u1, u2) conversion", "[VariablesConversion]")
{
  const double alpha1 =  0.86625211;
  const double rho    =  2.37484837;
  const double rho1   =  1.27738399;
  const double u11    = -1.66755556;
  const double u12    =  2.00018766;
  const double u21    =  0.33333333;
  const double u22    = -1.9383838;

  const double U1 = VariablesConversion::U1(rho);
  const double U2 = VariablesConversion::U2(rho1, alpha1);
  const double U3 = VariablesConversion::U3(u11, u12);
  const double U4 = VariablesConversion::U4(u21, u22);
  const double U5 = VariablesConversion::U5(alpha1, rho, rho1, u11, u12);
  const double U6 = VariablesConversion::U6(alpha1, rho, rho1, u21, u22);

  const double u1 = VariablesConversion::u1(U1, U2, U3, U5);
  const double u2 = VariablesConversion::u2(U1, U2, U4, U6);

  const double c1 = alpha1 * rho1 / rho;
  const double c2 = 1. - c1;

  REQUIRE(u1 == Approx(c1 * u11 + c2 * u12).epsilon(1e-14));
  REQUIRE(u2 == Approx(c1 * u21 + c2 * u22).epsilon(1e-14));
}

